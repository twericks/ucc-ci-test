package ucc.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * This class contains all the common timing operations. Functions in this class
 * are static so they can be called directly without instantiating an object of
 * this class.
 * 
 * @author Integrity Applications Incorporated
 * 
 */
public class TimeUtils
{
   /** Instantiate the Log4j logger for this class */
   private static final Logger logger = LogManager.getLogger(TimeUtils.class);

   /** A constant to convert from Nanoseconds to seconds */
   public static final double NANOSEC_PER_SEC = 1000000000.0;

   public static long GetTime()
   {
      return System.nanoTime();
   }

   /**
    * Function for printing the time elapsed from start to end
    *
    * @param start
    *           Start time
    * @param end
    *           End time
    * @param function
    *           Name of function from the start and end times
    *
    */
   public static void PrintElapsedTime(long start, long end, String function)
   {
      String duration = CalcElapsedTime(start, end);
      String output = String.format("%s execution time: %s", function, duration);
      // Need to avoid writing to console on checksum and duplicate to maintain
      // output
      System.out.println(output);
      logger.info(output);
   }

   /**
    * Function for returning a current timestamp
    *
    * @return timestamp Final timestamp of given time
    *
    */
   public static String PrintTimestamp()
   {
      String timestamp = "";
      SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ssSS");
      timestamp = dateFormat.format(new Date());
      return timestamp;
   }

   /**
    * Calculate the elapsed time between start to end
    *
    * @param start
    *           Start time
    * @param end
    *           End time
    * @return String of the elapsed time
    *
    */
   public static String CalcElapsedTime(long start, long end)
   {
      String elapsedTime = "";
      long duration = (end - start);

      long days = TimeUnit.NANOSECONDS.toDays(duration);
      duration -= TimeUnit.DAYS.toNanos(days);
      long hours = TimeUnit.NANOSECONDS.toHours(duration);
      duration -= TimeUnit.HOURS.toNanos(hours);
      long minutes = TimeUnit.NANOSECONDS.toMinutes(duration);
      duration -= TimeUnit.MINUTES.toNanos(minutes);
      long seconds = TimeUnit.NANOSECONDS.toSeconds(duration);
      duration -= TimeUnit.SECONDS.toNanos(seconds);
      long milliseconds = TimeUnit.NANOSECONDS.toMillis(duration);
      duration -= TimeUnit.MILLISECONDS.toNanos(milliseconds);

      // At minimum have the elapsed time 1 millisecond
      if (days == 0 && hours == 0 && minutes == 0 && seconds == 0 && milliseconds == 0)
      {
         elapsedTime = "1 millisecond";
      }

      else
      {
         if (days != 0)
         {
            elapsedTime += days;
            if (days == 1)
            {
               elapsedTime += " day ";
            }
            else
            {
               elapsedTime += " days ";
            }
         }
         if (hours != 0)
         {
            elapsedTime += hours;
            if (hours == 1)
            {
               elapsedTime += " hour ";
            }
            else
            {
               elapsedTime += " hours ";
            }
         }
         if (minutes != 0)
         {
            elapsedTime += minutes;
            if (minutes == 1)
            {
               elapsedTime += " minute ";
            }
            else
            {
               elapsedTime += " minutes ";
            }
         }
         if (seconds != 0)
         {
            elapsedTime += seconds;
            if (seconds == 1)
            {
               elapsedTime += " second ";
            }
            else
            {
               elapsedTime += " seconds ";
            }
         }
         if (milliseconds != 0)
         {
            elapsedTime += milliseconds;
            if (milliseconds == 1)
            {
               elapsedTime += " millisecond";
            }
            else
            {
               elapsedTime += " milliseconds";
            }
         }
      }

      return elapsedTime;
   }
}
