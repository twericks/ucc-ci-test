package ucc.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class is constructed under one of the most intuitive, not fastest, means
 * of implementing a text-line progress bar.
 */
public class ProgressVisualizer
{
   // Instantiate the Log4j logger for this class
   private static final Logger logger = LogManager.getLogger(ProgressVisualizer.class);

   // Class constants
   public static final double DONE = 1.0;
   private static final String RESET_CHARACTER = "\r";

   // Setup progress visualizer state
   private boolean done = false;
   private double previousProgress = 0.0;
   private double percentThreshold = 0.0;

   private String doneMessage = "DONE";
   private String actionMessage = "";
   private int progressBarLength = 10;
   // private String progressBar = "";

   /**
    * @param progressBarLength
    *           (int) - the discrete finite length of the progress bar itself
    * @param percentThreshold
    *           (double) - the percentage (out of 100.0%) threshold for
    *           determining when to print the progress bar
    * @param doneMessage
    *           Message to indicate the processing is complete
    * @param actionMessage
    *           Message to indicate the processing being completed
    */
   public ProgressVisualizer(int progressBarLength, double percentThreshold, String doneMessage, String actionMessage)
   {
      this.progressBarLength = progressBarLength;
      this.percentThreshold = percentThreshold;
      this.doneMessage = doneMessage;
      this.actionMessage = actionMessage;
   }

   /**
    * This is a default value constructor for the progress visualizer.
    *
    * @param actionMessage
    *           Message to indicate the processing being completed
    */
   public ProgressVisualizer(String actionMessage)
   {
      this(50, 1.0, "DONE", actionMessage);
   }

   /**
    * This method prints the progress bar based on the progress normalized
    * between 0.0 and 1.0.
    *
    * The method can also be used to test whether the progress bar has
    * completed.
    *
    * @param message
    *           (String) - message to display
    * @param progress
    *           (double) - the current progress normalized between 0.0 and 1.0
    * @return true if the action succeeded and false if it failed
    */
   public boolean printProgressBar(String message, double progress)
   {
      if (done == false)
      {
         // We have to round because due issues using double.
         // Out precision is to five additional places
         progress = Math.round(progress * 100000.0) / 100000.0;
         String resetCharacter = RESET_CHARACTER;
         if (0.0 <= progress && progress <= 1.0)
         {
            // Create the buffered space
            int length = progressBarLength - message.length();
            String progressBar = message;
            for (int i = 0; i < length; i++)
            {
               progressBar += ".";
            }

            // Add the completion status
            if (progress < 1.0)
            {
               // Determine how far the progress bar should extend
               length = (int) Math.round(progress * progressBarLength);
               progressBar += (int) Math.round(progress * 100) + "%";
            }
            else
            {
               progressBar += doneMessage;
               // We are done; go to the next line
               resetCharacter = "\n";
               done = true;
            }
            progressBar += resetCharacter;
            System.out.print(progressBar);
         }
         else
         {
            // this can occur, but has no impact on execution
            // logger.warn("The input progress value was invalid: " + progress);
            // logger.warn("The progress must be between 0.0 and 1.0
            // inclusive.");
         }
      }
      return done;
   }

   /**
    * This method prints the progress bar (as determined by a threshold) based
    * on the progress normalized between 0.0 and 1.0.
    *
    * The method can also be used to test whether the progress bar has
    * completed.
    *
    * @param message
    *           (String) - message to display
    * @param progress
    *           (double) - the current progress normalized between 0.0 and 1.0
    * @return true if the action succeeded and false if it failed
    */
   public boolean printProgressBarWithThreshold(String message, double progress)
   {
      // NOTE: I chose progress >= 1.0 to be more encompassing, as
      // printProgressBar is capable of handling the erroneous > 1.0 case
      if (((progress - previousProgress) * 100) >= percentThreshold || progress >= 1.0)
      {
         // update the progress tracking status
         previousProgress = progress;
         return printProgressBar(message, progress);
      }
      return false;
   }

   /**
    * This method prints the progress bar (as determined by a threshold) based
    * on the progress normalized between 0.0 and 1.0.
    *
    * @param progress
    *           (double) - the progress percentage
    * @return (boolean) - whether or not the progress visualizer is done and has
    *         terminated
    */
   public boolean printProgressBarWithThreshold(double progress)
   {
      return printProgressBarWithThreshold(actionMessage, progress);
   }

   /**
    * This method resets the progress visualizer such that it can be used again.
    */
   public void reset()
   {
      done = false;
   }

   /**
    * This method prints the progress bar for counting of a single baseline
    *
    * @param percentage
    *           (double) - the progress percentage
    *
    */
   public static void printProgressBar(double percentage)
   {
      try
      {
         Thread.sleep(1000);
         percentage = Math.round(percentage * 100000.0) / 100000.0;
         int percent = (int) Math.round(percentage * 100);

         System.out.print("Performing file counting......" + percent + "% complete.\r");
      }
      catch (InterruptedException e)
      {
         e.printStackTrace();
      }
   }

   /**
    * This method prints the progress bar when comparing files in the difference
    * operation
    *
    * @param percentage
    *           (double) - the progress percentage
    *
    */
   public static void printProgressBarDiff(double percentage)
   {
      try
      {
         Thread.sleep(1000);
         percentage = Math.round(percentage * 100000.0) / 100000.0;
         int percent = (int) Math.round(percentage * 100);
         System.out.print("Differencer operation......" + percent + "% complete.\r");
      }
      catch (InterruptedException e)
      {
         e.printStackTrace();
      }
   }

   /**
    * This method prints the progress bar for dup checking and counts of two
    * baselines
    *
    * @param dupPercentageA
    *           (double) - the progress percentage of dup checking for baseline
    *           A
    * @param dupPercentageB
    *           (double) - the progress percentage of dup checking for baseline
    *           B
    * @param countPercentageA
    *           (double) - the progress percentage of counting for baseline A
    * @param countPercentageB
    *           (double) - the progress percentage of counting for baseline B
    *
    */
   public static void printProgressBarDiff(double dupPercentageA, double dupPercentageB, double countPercentageA,
            double countPercentageB)
   {
      String functionCall = Thread.currentThread().currentThread().getStackTrace()[2].getFileName();
      dupPercentageA = Math.round(dupPercentageA * 100000.0) / 100000.0;
      int dupPercentA = (int) Math.round(dupPercentageA * 100);
      dupPercentageB = Math.round(dupPercentageB * 100000.0) / 100000.0;
      int dupPercentB = (int) Math.round(dupPercentageB * 100);

      countPercentageA = Math.round(countPercentageA * 100000.0) / 100000.0;
      int countPercentA = (int) Math.round(countPercentageA * 100);
      countPercentageB = Math.round(countPercentageB * 100000.0) / 100000.0;
      int countPercentB = (int) Math.round(countPercentageB * 100);

      // Slow down output when function call is not from
      // DuplicateFileFinder.java
      if (!functionCall.contains("DuplicateFileFinder"))
      {
         try
         {
            Thread.sleep(1000);
         }
         catch (InterruptedException e)
         {
            e.printStackTrace();
         }
      }

      // Print dup percentage A and dup percentage B
      if (dupPercentA != 100 && dupPercentB != 100)
      {
         System.out.print("Performing duplicate identification....Baseline A " + dupPercentA
                  + "% complete....Baseline B " + dupPercentB + "% complete.\r");
      }
      // Print dup percentage A and count percentage B
      else if (dupPercentA != 100 && dupPercentB == 100)
      {
         System.out.print("Performing duplicate identification....Baseline A " + dupPercentA
                  + "% complete and file counting....Baseline B " + countPercentB + "% complete.\r");
      }
      // Print dup percentage B and count percentage A
      else if (dupPercentA == 100 && dupPercentB != 100)
      {
         System.out.print("Performing duplicate identification....Baseline B " + dupPercentB
                  + "% complete and file counting....Baseline A " + countPercentA + "% complete.\r");
      }
      // Both dup A and B are finished so print only count percentages
      else
      {
         System.out.print("Performing file counting....Baseline A " + countPercentA + "% complete....Baseline B "
                  + countPercentB + "% complete.                               \r");
      }
   }

   /**
    * This method prints the progress bar for counts of two baselines
    *
    * @param countPercentageA
    *           (double) - the progress percentage of counting for baseline A
    * @param countPercentageB
    *           (double) - the progress percentage of counting for baseline B
    *
    */
   public static void printProgressBarDiff(double countPercentageA, double countPercentageB)
   {
      countPercentageA = Math.round(countPercentageA * 100000.0) / 100000.0;
      int countPercentA = (int) Math.round(countPercentageA * 100);
      countPercentageB = Math.round(countPercentageB * 100000.0) / 100000.0;
      int countPercentB = (int) Math.round(countPercentageB * 100);

      try
      {
         Thread.sleep(1000);
         System.out.print("Performing file counting....Baseline A " + countPercentA + "% complete....Baseline B "
                  + countPercentB + "% complete.\r");
      }
      catch (InterruptedException e)
      {
         e.printStackTrace();
      }
   }

   /**
    * This method prints the progress bar for dup checking and count of a single
    * baseline
    *
    * @param dupPercentageA
    *           (double) - the progress percentage of dup checking for baseline
    *           A
    * @param countPercentageA
    *           (double) - the progress percentage of counting for baseline A
    *
    */
   public static void printProgressBar(double dupPercentageA, double countPercentageA)
   {
      dupPercentageA = Math.round(dupPercentageA * 100000.0) / 100000.0;
      int dupPercentA = (int) Math.round(dupPercentageA * 100);

      countPercentageA = Math.round(countPercentageA * 100000.0) / 100000.0;
      int countPercentA = (int) Math.round(countPercentageA * 100);

      try
      {
         String functionCall = Thread.currentThread().currentThread().getStackTrace()[2].getFileName();
         if (!functionCall.contains("DuplicateFileFinder"))
         {
            Thread.sleep(1000);
         }
         System.out.print("Performing duplicate identification...." + dupPercentA + "% complete and file counting...."
                  + countPercentA + "% complete.\r");
      }
      catch (InterruptedException e)
      {
         e.printStackTrace();
      }
   }
}