package ucc.counters.handlers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import ucc.counters.*;

public class ColdFusionUtils
{
   // Pre-processing Variables
   CodeCounter counter = new CodeCounter();
   String line;
   int truncateLinesCount = 0;
   List<PositionItem> positionItemList = new ArrayList<PositionItem>();
   List<DeleteItem> deleteItemList = new ArrayList<DeleteItem>();
   List<Boolean> indexInUse;
   Boolean inMultiLineComment;
   Boolean inStringLiteral;
   String closingStringLiteral;

   String inMultiLineCommentCloseChar = "";
   String inStringLiteralCloseChar = "";

   final String TYPE_STRING_LITERAL = "SL";
   final String TYPE_SINGLE_LINE_COMMENT = "SLC";
   final String TYPE_MULTI_LINE_COMMENT_START = "MLCS";
   final String TYPE_MULTI_LINE_COMMENT_END = "MLCE";
   final String TYPE_COMPILER_DIRECTIVE = "CD";

   /**
    * Function for processing a line of code to extract the required information
    * and increment the appropriate metric counters.
    * 
    * @param incomingLine
    *           Line coming in from file
    *
    * @return line Line after modifications
    */
   public String ColdFusionUtils(String incomingLine)
   {
      line = incomingLine;
      line = line.trim();

      indexInUse = Arrays.asList(new Boolean[line.length()]);
      Collections.fill(indexInUse, Boolean.FALSE);

      // Identify and save all index positions of Multi Line Comment Start
      // Characters
      ArrayList<String> CommentStart = new ArrayList<String>();
      CommentStart.add("<!---");
      GetSpecialChars(CommentStart, TYPE_MULTI_LINE_COMMENT_START);

      // Identify and save all index positions of Multi Line Comment End
      // Characters
      ArrayList<String> CommentEnd = new ArrayList<String>();
      CommentEnd.add("--->");
      GetSpecialChars(CommentEnd, TYPE_MULTI_LINE_COMMENT_END);

      // Identify and save all index positions of String Literals
      ArrayList<String> QuoteChar = new ArrayList<String>();
      QuoteChar.add("\"");
      QuoteChar.add("'");
      GetSpecialChars(QuoteChar, TYPE_STRING_LITERAL);

      // Starting left to right, process each identified position.
      if (!positionItemList.isEmpty())
      {
         CleanUpLine();
      }

      return line;
   }

   /**
    * Function for processing a line of code to extract the required information
    * and increment the appropriate metric counters.
    * 
    * @param SpecialChars
    *           Array list containing characters that need to be found in the
    *           line
    * 
    * @param type
    *           The type for the special characters coming in
    */
   private void GetSpecialChars(ArrayList<String> SpecialChars, String type)
   {
      for (int x = 0; x < SpecialChars.size() && !line.isEmpty(); x++)
      {
         String specialCharacter = SpecialChars.get(x);
         int index = line.indexOf(specialCharacter);
         int endIndex = index + specialCharacter.length() - 1;
         boolean tagInUse = false;
         while (index >= 0)
         {
            tagInUse = false;
            for (int k = index; k <= endIndex; k++)
            {
               if (indexInUse.get(k))
               {
                  tagInUse = true;
                  break;
               }
            }
            if (!tagInUse)
            {
               PositionItem api = new PositionItem();
               api.position = index;
               api.type = type;
               api.actualChar = specialCharacter;
               if (type == TYPE_MULTI_LINE_COMMENT_START)
               {
                  // Assumption is the end characters
                  // are set in the same order as the
                  // beginning characters in the properties array
                  api.correspondingCloseChar = "--->";
               }
               else if (type == TYPE_MULTI_LINE_COMMENT_END || type == TYPE_SINGLE_LINE_COMMENT)
               {
                  api.correspondingCloseChar = null;
               }
               else
               {
                  // Assumption is the beginning quote character will
                  // always match the end
                  api.correspondingCloseChar = specialCharacter;
               }

               positionItemList.add(api);

               inStringLiteralCloseChar = specialCharacter;

               for (int j = index; j <= endIndex; j++)
               {
                  indexInUse.set(j, true);
               }
            }

            index = line.indexOf(specialCharacter, index + 1); // Re-establish
            // the
            // loop
            endIndex = index + specialCharacter.length() - 1;
         }
      }
   }

   /**
    * Function to arrange the special characters in order as they appear in the
    * line, call the CreateDeleteItemList function and call the
    * DeleteSpecialChars function
    *
    */
   private void CleanUpLine()
   {
      // Sort the array in order of 1st position occurrence, then
      // length of position item descending
      Comparator<PositionItem> sortByPosition = (p, o) -> p.position.compareTo(o.position);
      Comparator<PositionItem> sortByLength =
               (p, o) -> new Integer(p.actualChar.length()).compareTo(new Integer(o.actualChar.length()));
      positionItemList = positionItemList.stream().sorted(sortByPosition.thenComparing(sortByLength.reversed()))
               .collect(Collectors.toList());

      // Remove Duplicate Positions, (e.g. #, and #! will both have the
      // same start index, and in this example, we want to keep #! and
      // eliminate #)
      int currentPos = 0;
      int previousPos = 0;
      for (int x = 0; x < positionItemList.size() && !line.isEmpty(); x++)
      {
         currentPos = positionItemList.get(x).position;
         if (x == 0)
         {
            previousPos = currentPos;
            continue;
         }
         else if (previousPos == currentPos)
         {
            positionItemList.get(x).remove = true;
            previousPos = currentPos;
         }
      }

      // Re-assemble the list
      List<PositionItem> positionItemListCleaned = new ArrayList<PositionItem>();
      for (int x = 0; x < positionItemList.size() && !line.isEmpty(); x++)
      {
         if (!positionItemList.get(x).remove)
         {
            positionItemListCleaned.add(positionItemList.get(x));
         }
      }

      CreateDeleteItemList(positionItemListCleaned);

      if (line.length() > 0 && !deleteItemList.isEmpty())
      {
         DeleteSpecialChars();
      }
   }

   /**
    * Function for adding to the deleteItemList for positions that need to be
    * removed for the line
    * 
    * @param positionItemListCleaned
    *           list of special characters in order as they appear in the line
    * 
    */
   private void CreateDeleteItemList(List<PositionItem> positionItemListCleaned)
   {
      int nextPosition = 0;

      // Process the Position Array
      for (int x = 0; x < positionItemListCleaned.size() && !line.isEmpty(); x++)
      {

         if (x >= nextPosition)
         {
            if (positionItemListCleaned.get(x).type.equals(TYPE_STRING_LITERAL))
            {
               // Find the closing Literal, starting after the current
               // item
               for (int y = (x + 1); y < positionItemListCleaned.size() && !line.isEmpty(); y++)
               {
                  if (positionItemListCleaned.get(y).type.equals(TYPE_STRING_LITERAL)
                           && positionItemListCleaned.get(y).actualChar
                                    .equals(positionItemListCleaned.get(x).correspondingCloseChar))
                  {
                     deleteItemList.add(new DeleteItem(positionItemListCleaned.get(x).position,
                              positionItemListCleaned.get(y).position, positionItemListCleaned.get(x).type));
                     nextPosition = y + 1;
                     break;
                  }
               }

            }

            else if (positionItemListCleaned.get(x).type.equals(TYPE_MULTI_LINE_COMMENT_START))
            {
               // Find the closing multi-line comment character,
               // starting after the current item
               for (int y = (x + 1); y < positionItemListCleaned.size() && !line.isEmpty(); y++)
               {
                  if (positionItemListCleaned.get(y).type.equals(TYPE_MULTI_LINE_COMMENT_END)
                           && positionItemListCleaned.get(y).actualChar
                                    .equals(positionItemListCleaned.get(x).correspondingCloseChar))
                  {
                     deleteItemList.add(new DeleteItem(
                              positionItemListCleaned.get(x).position
                                       + positionItemListCleaned.get(y).actualChar.length(),
                              positionItemListCleaned.get(y).position, positionItemListCleaned.get(x).type));
                     nextPosition = y + 1;

                     break;
                  }
               }
            }

            else if (positionItemListCleaned.get(x).type.equals(TYPE_MULTI_LINE_COMMENT_END))
            {
               deleteItemList.add(new DeleteItem(positionItemListCleaned.get(x).position,
                        positionItemListCleaned.get(x).position + positionItemListCleaned.get(x).actualChar.length(),
                        positionItemListCleaned.get(x).type));

            }
         }
      }
   }

   /**
    * Function for removing the sections of the code which need to be deleted
    * according to the deleteItemList
    * 
    */
   private void DeleteSpecialChars()
   {
      String newLine = "";
      int previousEnd = 0;
      for (int m = 0; m < deleteItemList.size(); m++)
      {
         if (deleteItemList.get(m).type.equals(TYPE_STRING_LITERAL)
                  || deleteItemList.get(m).type.equals(TYPE_MULTI_LINE_COMMENT_START))
         {

            newLine = newLine + line.substring(previousEnd, deleteItemList.get(m).startPosition + 1);

         }
         else if (deleteItemList.get(m).type.equals(TYPE_MULTI_LINE_COMMENT_END))
         {
            newLine = newLine + line.substring(deleteItemList.get(m).startPosition, deleteItemList.get(m).endPosition);
         }

         // Clean up and grab rest of the line after last delete item
         if (m + 1 == deleteItemList.size() && previousEnd < line.length())
         {
            newLine = newLine + line.substring(deleteItemList.get(m).endPosition, line.length());
         }
         previousEnd = deleteItemList.get(m).endPosition;
      }
      line = newLine;
      line = line.trim();

   }

   /**
    * Class for storing variables needed for positions within the line as well
    * as a functions to set default values to these variables.
    */
   class PositionItem
   {
      public Integer position;
      public String type;
      public String actualChar;
      public String correspondingCloseChar;
      boolean remove;

      public PositionItem()
      {
         position = -1;
         type = "";
         actualChar = "";
         correspondingCloseChar = "";
         remove = false;
      }
   }

   /**
    * Class for storing variables which need to be deleted from the line as well
    * as a functions to set these variables.
    */
   class DeleteItem
   {
      Integer startPosition;
      Integer endPosition;
      String type;

      public DeleteItem(Integer startPosition, Integer endPosition, String type)
      {
         this.startPosition = startPosition;
         this.endPosition = endPosition;
         this.type = type;
      }
   }
}
