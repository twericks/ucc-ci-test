package ucc.counters.handlers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ucc.datatypes.Constants;
import ucc.datatypes.DataTypes;
import ucc.datatypes.DataTypes.LanguagePropertiesType;
import ucc.datatypes.UCCFile;
import ucc.langprops.LanguageProperties;
import ucc.main.RuntimeParameters;
import ucc.utils.FileUtils;

public class MultiLanguageHandler
{
   /** Instantiate the Log4j2 logger for this class */
   private static final Logger logger = LogManager.getLogger(MultiLanguageHandler.class);

   /** Class to hold global variables */
   public class globals
   {
      String ASPCode = "";
      String HTMLCode = "";
      String JSPCode = "";
      String CSSCode = "";
      String PHPCode = "";
      String CFCode = "";
      String tempLine = "";
      String CFSCode = "";
      String SQLCode = "";
      String XMLCode = "";
      String JSCode = "";
      String VBSCode = "";

      boolean openStartTag;
      boolean openCFTag;
      boolean openCommentTag;

      LanguageState languageState;
      String fileLanguage;

      public void setupLangState()
      {
         this.languageState = SetupState(this.fileLanguage);
         this.openStartTag = false;
         this.openCFTag = false;
         this.openCommentTag = false;
      }
   }

   // Variables
   private ArrayList<String> CSSStartTags = new ArrayList<String>();
   private ArrayList<String> CSSEndTags = new ArrayList<String>();
   private ArrayList<String> JSStartTags = new ArrayList<String>();
   private ArrayList<String> JSEndTags = new ArrayList<String>();
   private ArrayList<String> PHPStartTags = new ArrayList<String>();
   private ArrayList<String> PHPEndTags = new ArrayList<String>();
   private ArrayList<String> VBSStartTags = new ArrayList<String>();
   private ArrayList<String> VBSEndTags = new ArrayList<String>();
   private ArrayList<String> ERBStartTags = new ArrayList<String>();
   private ArrayList<String> ERBEndTags = new ArrayList<String>();
   private ArrayList<String> JSPStartTags = new ArrayList<String>();
   private ArrayList<String> JSPEndTags = new ArrayList<String>();
   private ArrayList<String> GSPStartTags = new ArrayList<String>();
   private ArrayList<String> GSPEndTags = new ArrayList<String>();
   private ArrayList<String> ASPStartTags = new ArrayList<String>();
   private ArrayList<String> ASPEndTags = new ArrayList<String>();
   private ArrayList<String> CFSStartTags = new ArrayList<String>();
   private ArrayList<String> CFSEndTags = new ArrayList<String>();
   private ArrayList<String> SQLStartTags = new ArrayList<String>();
   private ArrayList<String> SQLEndTags = new ArrayList<String>();

   // variables for cold fusion
   private String endTag = ">";
   private String cfTag = "<cf";
   private String cfCloseTag = "</cf";
   private String commentStart = "<!---";
   private String commentEnd = "--->";

   private String fileExt; // Use for *.erb and *.gsp implementation

   private RuntimeParameters RtParams;

   /** Enumerated type to note which language is the current state */
   protected enum LanguageState
   {
      IN_HTML, IN_CSS, IN_JS, IN_PHP, IN_VBS, IN_ERB, IN_JSP, IN_GSP, IN_ASP, IN_CF, IN_CFS, IN_SQL, IN_XML
   }

   /**
    * Default constructor for the multi-language handler
    * 
    * @param languageProperties
    *           An object containing language properties
    * @param aRuntimeParameters
    *           RuntimeParameters object to access through MultiLanguage
    *           processing
    */
   public MultiLanguageHandler(LanguageProperties languageProperties, RuntimeParameters aRuntimeParameters)
   {
      SetupVariables();

      RtParams = aRuntimeParameters;
   }

   /**
    * Function for getting the file extension of the file being operated on.
    * 
    * @param cntrResult
    *           A UCCFile object to store results of code counter
    */
   public void GetFileExt(UCCFile cntrResult)
   {
      fileExt = FileUtils.GetFileExt(cntrResult.FileName);
   }

   /**
    * Function for setting up the start and end tags for various languages.
    */
   private void SetupVariables()
   {
      // CSS tags
      CSSStartTags.add("<style>");
      CSSEndTags.add("</style>");

      // JS tags
      JSStartTags.add("<script>");
      JSStartTags.add("<script type=\"text/javascript\">");
      JSStartTags.add("<script type='text/javascript'>");
      JSStartTags.add("<script type=text/javascript>");
      JSStartTags.add("<script language=\"javascript\">");
      JSStartTags.add("<script language='javascript'>");
      JSStartTags.add("<script language=javascript>");
      JSStartTags.add("<script language=\"javascript\" type=\"text/javascript\">");
      JSStartTags.add("<script language='javascript' type='text/javascript'>");
      JSStartTags.add("<script language=javascript type=text/javascript>");
      JSEndTags.add("</script>");

      // PHP tags
      PHPStartTags.add("<?php");
      PHPEndTags.add("?>");

      // VBS tags
      VBSStartTags.add("<script type=\"text/vbscript\">");
      VBSStartTags.add("<script type='text/vbscript'>");
      VBSStartTags.add("<script type=text/vbscript>");
      VBSStartTags.add("<script language=\"vbscript\">");
      VBSStartTags.add("<script language='vbscript'>");
      VBSStartTags.add("<script language=vbscript>");
      VBSStartTags.add("<script language=\"vbscript\" type=\"text/vbscript\">");
      VBSStartTags.add("<script language='vbscript' type='text/vbscript'>");
      VBSStartTags.add("<script language=vbscript type=text/vbscript>");
      VBSEndTags.add("</script>");

      // ERB tags
      ERBStartTags.add("<%");
      ERBEndTags.add("%>");

      // JSP tags
      JSPStartTags.add("<%");
      JSPStartTags.add("<jsp:");
      JSPEndTags.add("%>");
      JSPEndTags.add("</jsp:");

      // GSP tags
      GSPStartTags.add("<g:");
      GSPEndTags.add("</g");

      // ASP tags
      // Modified by 2019 UCC-J Summer Team
      // ASPStartTags.add("<%");
      ASPStartTags.add("<script runat=");
      ASPStartTags.add("<asp:");
      // ASPEndTags.add("%>");
      ASPEndTags.add("</asp:");

      // ColdFusion Script tags
      CFSStartTags.add("<cfscript>");
      CFSEndTags.add("</cfscript>");

      // SQL tags
      SQLStartTags.add("<cfquery");
      SQLEndTags.add("</cfquery>");
   }

   /**
    * Function for setting up the initial state depending on which language the
    * file is.
    *
    * @param fileLanguage
    *           The language of the file being counted
    *
    */
   private LanguageState SetupState(String fileLanguage)
   {
      String languageState;
      if (fileLanguage.equals("HTML"))
      {
         return LanguageState.IN_HTML;
      }
      else if (fileLanguage.equals("ASP"))
      {
         return LanguageState.IN_HTML;
      }
      else if (fileLanguage.equals("JSP"))
      {
         return LanguageState.IN_HTML;
      }
      else if (fileLanguage.equals("CSS"))
      {
         return LanguageState.IN_CSS;
      }
      else if (fileLanguage.equals("PHP"))
      {
         return LanguageState.IN_HTML;
      }
      else if (fileLanguage.equals("COLDFUSION"))
      {
         return LanguageState.IN_HTML;
      }
      else if (fileLanguage.equals("JAVASCRIPT"))
      {
         return LanguageState.IN_JS;
      }
      else if (fileLanguage.equals("VBS"))
      {
         return LanguageState.IN_VBS;
      }
      else if (fileLanguage.equals("RUBY"))
      {
         return LanguageState.IN_HTML;
      }
      else if (fileLanguage.equals("GROOVY"))
      {
         return LanguageState.IN_HTML;
      }
      else if (fileLanguage.equals("XML"))
      {
         return LanguageState.IN_XML;
      }
      else
      {
         logger.error(fileLanguage + " is not a multi-language supported language.");
      }
      return LanguageState.valueOf(null);
   }

   /**
    * Function for cutting out the embedded languages and writing them to their
    * own files for later processing.
    * 
    * @param cntrResults
    *           A UCCFile object ArrayList to store results of code counters
    * @param i
    *           The index of the UCCFile we want to work on
    *
    */
   public void HandleEmbeddedCode(ArrayList<UCCFile> cntrResults, int i)
   {
      globals globalVars = new globals();
      UCCFile cntrResult = cntrResults.get(i);
      globalVars.fileLanguage = cntrResult.LangProperty.name();
      globalVars.setupLangState();

      LineNumberReader reader = null;

      try
      {
         reader = new LineNumberReader(
                  new InputStreamReader(new FileInputStream(cntrResult.FileName), Constants.CHARSET_NAME));

         // Read first line
         String line = reader.readLine();

         // While we have lines in the file...
         while (line != null)
         {

            // Split code up
            HandleEmbeddedLanguages(line, globalVars);
            // Read next line
            line = reader.readLine();
         }
      }
      catch (IOException e)
      {
         logger.error("The input reader failed to open.");
         logger.error(e);
      }
      finally
      {
         // If the original file was opened...
         if (reader != null)
         {
            // Close the original file
            try
            {
               cntrResult.IsCounted = true;
               reader.close();
            }
            catch (IOException e)
            {
               logger.error("The input reader failed to close.");
               logger.error(e);
            }
            reader = null;
         }

         PrintCode(cntrResults, i, globalVars);
      }
   }

   /**
    * Function for gathering all embedded languages together and saving them
    * into their respective strings.
    * 
    * @param line
    *           String coming in from file
    * @param globalVars
    *           Object that holds all the global variables
    */
   public void HandleEmbeddedLanguages(String line, globals globalVars)
   {
      boolean blankLine = false;

      if (line.trim().isEmpty() && (globalVars.languageState == LanguageState.IN_HTML
               || globalVars.languageState == LanguageState.IN_XML))
      {
         blankLine = true;
      }

      // Replace <%= and <%! with <% so we don't have lines starting with ! or =
      line = line.replaceAll("\\<\\%\\=", "\\<\\%").replaceAll("\\<\\%\\!", "\\<\\%");

      // If we're in an HTML file
      if (globalVars.fileLanguage.equals("HTML"))
      {
         // TODO put in support for *.erb and *.gsp here?

         // If we're in HTML code
         if (globalVars.languageState == LanguageState.IN_HTML)
         {
            // Look for CSS start tags
            line = SearchForEmbeddedStart(CSSStartTags, CSSEndTags, LanguageState.IN_CSS, globalVars.CSSCode, line,
                     globalVars);

            // Look for JS start tags
            line = SearchForEmbeddedStart(JSStartTags, JSEndTags, LanguageState.IN_JS, globalVars.JSCode, line,
                     globalVars);

            // Look for VBS start tags
            line = SearchForEmbeddedStart(VBSStartTags, VBSEndTags, LanguageState.IN_VBS, globalVars.VBSCode, line,
                     globalVars);

            // Look for JSP start tags
            line = SearchForEmbeddedStart(JSPStartTags, JSPEndTags, LanguageState.IN_JSP, globalVars.JSPCode, line,
                     globalVars);
         }
         else if (globalVars.languageState == LanguageState.IN_CSS) // If we're
                                                                    // in CSS
                                                                    // code
         {
            line = HandleEmbeddedCode(CSSEndTags, globalVars.languageState, globalVars.CSSCode, line, globalVars);
         }
         else if (globalVars.languageState == LanguageState.IN_JS) // If we're
                                                                   // in JS code
         {
            line = HandleEmbeddedCode(JSEndTags, globalVars.languageState, globalVars.JSCode, line, globalVars);
         }
         else if (globalVars.languageState == LanguageState.IN_VBS) // If we're
                                                                    // in VBS
                                                                    // code
         {
            line = HandleEmbeddedCode(VBSEndTags, globalVars.languageState, globalVars.VBSCode, line, globalVars);
         }
         else if (globalVars.languageState == LanguageState.IN_JSP) // If we're
                                                                    // in JSP
                                                                    // code
         {
            line = HandleEmbeddedCode(JSPEndTags, globalVars.languageState, globalVars.JSPCode, line, globalVars);
         }
      }

      // If we're in an ASP file
      if (globalVars.fileLanguage.equals("ASP"))
      {
         // If we're in HTML code
         if (globalVars.languageState == LanguageState.IN_HTML)
         {
            // Look for CSS start tags
            line = SearchForEmbeddedStart(CSSStartTags, CSSEndTags, LanguageState.IN_CSS, globalVars.CSSCode, line,
                     globalVars);

            // Look for JS start tags
            line = SearchForEmbeddedStart(JSStartTags, JSEndTags, LanguageState.IN_JS, globalVars.JSCode, line,
                     globalVars);

            // Look for VBS start tags
            line = SearchForEmbeddedStart(VBSStartTags, VBSEndTags, LanguageState.IN_VBS, globalVars.VBSCode, line,
                     globalVars);

            // Look for ASP start tags
            line = SearchForEmbeddedStart(ASPStartTags, ASPEndTags, LanguageState.IN_ASP, globalVars.ASPCode, line,
                     globalVars);
         }
         else if (globalVars.languageState == LanguageState.IN_CSS) // If we're
                                                                    // in CSS
                                                                    // code
         {
            line = HandleEmbeddedCode(CSSEndTags, globalVars.languageState, globalVars.CSSCode, line, globalVars);
         }
         else if (globalVars.languageState == LanguageState.IN_JS) // If we're
                                                                   // in JS code
         {
            line = HandleEmbeddedCode(JSEndTags, globalVars.languageState, globalVars.JSCode, line, globalVars);
         }
         else if (globalVars.languageState == LanguageState.IN_VBS) // If we're
                                                                    // in VBS
                                                                    // code
         {
            line = HandleEmbeddedCode(VBSEndTags, globalVars.languageState, globalVars.VBSCode, line, globalVars);
         }
         else if (globalVars.languageState == LanguageState.IN_ASP) // If we're
                                                                    // in ASP
                                                                    // code
         {
            line = HandleEmbeddedCode(ASPEndTags, globalVars.languageState, globalVars.ASPCode, line, globalVars);
         }
      }

      // If we're in an JSP file
      if (globalVars.fileLanguage.equals("JSP"))
      {
         // If we're in HTML code
         if (globalVars.languageState == LanguageState.IN_HTML)
         {
            // Look for CSS start tags
            line = SearchForEmbeddedStart(CSSStartTags, CSSEndTags, LanguageState.IN_CSS, globalVars.CSSCode, line,
                     globalVars);

            // Look for JS start tags
            line = SearchForEmbeddedStart(JSStartTags, JSEndTags, LanguageState.IN_JS, globalVars.JSCode, line,
                     globalVars);

            // Look for VBS start tags
            line = SearchForEmbeddedStart(VBSStartTags, VBSEndTags, LanguageState.IN_VBS, globalVars.VBSCode, line,
                     globalVars);

            // Look for JSP start tags
            line = SearchForEmbeddedStart(JSPStartTags, JSPEndTags, LanguageState.IN_JSP, globalVars.JSPCode, line,
                     globalVars);
         }
         else if (globalVars.languageState == LanguageState.IN_CSS) // If we're
                                                                    // in CSS
                                                                    // code
         {
            line = HandleEmbeddedCode(CSSEndTags, globalVars.languageState, globalVars.CSSCode, line, globalVars);
         }
         else if (globalVars.languageState == LanguageState.IN_JS) // If we're
                                                                   // in JS code
         {
            line = HandleEmbeddedCode(JSEndTags, globalVars.languageState, globalVars.JSCode, line, globalVars);
         }
         else if (globalVars.languageState == LanguageState.IN_VBS) // If we're
                                                                    // in VBS
                                                                    // code
         {
            line = HandleEmbeddedCode(VBSEndTags, globalVars.languageState, globalVars.VBSCode, line, globalVars);
         }
         else if (globalVars.languageState == LanguageState.IN_JSP) // If we're
                                                                    // in JSP
                                                                    // code
         {
            line = HandleEmbeddedCode(JSPEndTags, globalVars.languageState, globalVars.JSPCode, line, globalVars);
         }
      }

      // If we're in an PHP file
      if (globalVars.fileLanguage.equals("PHP"))
      {
         // If we're in HTML code
         if (globalVars.languageState == LanguageState.IN_HTML)
         {
            // Look for CSS start tags
            line = SearchForEmbeddedStart(CSSStartTags, CSSEndTags, LanguageState.IN_CSS, globalVars.CSSCode, line,
                     globalVars);

            // Look for JS start tags
            line = SearchForEmbeddedStart(JSStartTags, JSEndTags, LanguageState.IN_JS, globalVars.JSCode, line,
                     globalVars);

            // Look for VBS start tags
            line = SearchForEmbeddedStart(VBSStartTags, VBSEndTags, LanguageState.IN_VBS, globalVars.VBSCode, line,
                     globalVars);

            // Look for PHP start tags
            line = SearchForEmbeddedStart(PHPStartTags, PHPEndTags, LanguageState.IN_PHP, globalVars.PHPCode, line,
                     globalVars);
         }
         else if (globalVars.languageState == LanguageState.IN_CSS) // If we're
                                                                    // in CSS
                                                                    // code
         {
            line = HandleEmbeddedCode(CSSEndTags, globalVars.languageState, globalVars.CSSCode, line, globalVars);
         }
         else if (globalVars.languageState == LanguageState.IN_JS) // If we're
                                                                   // in JS code
         {
            line = HandleEmbeddedCode(JSEndTags, globalVars.languageState, globalVars.JSCode, line, globalVars);
         }
         else if (globalVars.languageState == LanguageState.IN_VBS) // If we're
                                                                    // in VBS
                                                                    // code
         {
            line = HandleEmbeddedCode(VBSEndTags, globalVars.languageState, globalVars.VBSCode, line, globalVars);
         }
         else if (globalVars.languageState == LanguageState.IN_PHP) // If we're
                                                                    // in PHP
                                                                    // code
         {
            line = HandleEmbeddedCode(PHPEndTags, globalVars.languageState, globalVars.PHPCode, line, globalVars);
         }
      }

      // If we're in an XML file
      if (globalVars.fileLanguage.equals("XML"))
      {
         // If we're in XML code
         if (globalVars.languageState == LanguageState.IN_XML)
         {
            // Look for CSS start tags
            line = SearchForEmbeddedStart(CSSStartTags, CSSEndTags, LanguageState.IN_CSS, globalVars.CSSCode, line,
                     globalVars);

            // Look for JS start tags
            line = SearchForEmbeddedStart(JSStartTags, JSEndTags, LanguageState.IN_JS, globalVars.JSCode, line,
                     globalVars);

            // Look for VBS start tags
            line = SearchForEmbeddedStart(VBSStartTags, VBSEndTags, LanguageState.IN_VBS, globalVars.VBSCode, line,
                     globalVars);
         }
         else if (globalVars.languageState == LanguageState.IN_CSS) // If we're
                                                                    // in CSS
                                                                    // code
         {
            line = HandleEmbeddedCode(CSSEndTags, globalVars.languageState, globalVars.CSSCode, line, globalVars);
         }
         else if (globalVars.languageState == LanguageState.IN_JS) // If we're
                                                                   // in JS code
         {
            line = HandleEmbeddedCode(JSEndTags, globalVars.languageState, globalVars.JSCode, line, globalVars);
         }
         else if (globalVars.languageState == LanguageState.IN_VBS) // If we're
                                                                    // in VBS
                                                                    // code
         {
            line = HandleEmbeddedCode(VBSEndTags, globalVars.languageState, globalVars.VBSCode, line, globalVars);
         }
      }

      // If we're not dealing with an open COLDFUSION tag
      if (globalVars.openCFTag == false)
      {
         // If we're in a COLDFUSION file
         if (globalVars.fileLanguage.equals("COLDFUSION"))
         {
            // If we're in COLDFUSION code
            if (globalVars.languageState == LanguageState.IN_HTML)
            {
               // Look for CSS start tags
               line = SearchForEmbeddedStart(CSSStartTags, CSSEndTags, LanguageState.IN_CSS, globalVars.CSSCode, line,
                        globalVars);

               // Look for JS start tags
               line = SearchForEmbeddedStart(JSStartTags, JSEndTags, LanguageState.IN_JS, globalVars.JSCode, line,
                        globalVars);

               // Look for VBS start tags
               line = SearchForEmbeddedStart(VBSStartTags, VBSEndTags, LanguageState.IN_VBS, globalVars.VBSCode, line,
                        globalVars);

               // Look for COLDFUSION SCRIPT start tags
               line = SearchForEmbeddedStart(CFSStartTags, CFSEndTags, LanguageState.IN_CFS, globalVars.CFSCode, line,
                        globalVars);

               // Look for SQL start tags
               line = SearchForMultiLineEmbeddedStart(SQLStartTags, SQLEndTags, LanguageState.IN_SQL,
                        globalVars.SQLCode, line, globalVars);
            }
            else if (globalVars.languageState == LanguageState.IN_CSS) // If
                                                                       // we're
                                                                       // in CSS
                                                                       // code
            {
               line = HandleEmbeddedCode(CSSEndTags, globalVars.languageState, globalVars.CSSCode, line, globalVars);
            }
            else if (globalVars.languageState == LanguageState.IN_JS) // If
                                                                      // we're
                                                                      // in JS
                                                                      // code
            {
               line = HandleEmbeddedCode(JSEndTags, globalVars.languageState, globalVars.JSCode, line, globalVars);
            }
            else if (globalVars.languageState == LanguageState.IN_VBS) // If
                                                                       // we're
                                                                       // in VBS
                                                                       // code
            {
               line = HandleEmbeddedCode(VBSEndTags, globalVars.languageState, globalVars.VBSCode, line, globalVars);
            }
            else if (globalVars.languageState == LanguageState.IN_CFS) // If
                                                                       // we're
                                                                       // in CFS
                                                                       // code
            {
               line = HandleEmbeddedCode(CFSEndTags, globalVars.languageState, globalVars.CFSCode, line, globalVars);
            }
            else if (globalVars.languageState == LanguageState.IN_SQL) // If
                                                                       // we're
                                                                       // in SQL
                                                                       // code
            {
               line = HandleEmbeddedCode(SQLEndTags, globalVars.languageState, globalVars.SQLCode, line, globalVars);
            }
         }
      }

      globalVars.tempLine = line;

      // If the line wasn't made blank from above or it was already a blank line
      if (!line.trim().isEmpty() || blankLine == true)
      {
         // If we're in a COLDFUSION file
         if (globalVars.fileLanguage.equals("COLDFUSION"))
         {
            // Removes everything between comments and string literals
            ColdFusionUtils eraseSpecialChars = new ColdFusionUtils();
            line = eraseSpecialChars.ColdFusionUtils(line);
            globalVars.tempLine = line;

            // Inside of a comment
            if (globalVars.openCommentTag)
            {
               // Search for comment end tag
               FindCommentClose(line, globalVars);
            }
            // Not in a comment
            else
            {
               String oldCFCode = globalVars.CFCode;
               // If we're not dealing with an open COLDFUSION tag
               if (globalVars.openCFTag == false)
               {
                  // Loop and find "<cf"
                  FindTags(cfTag, line, globalVars);

                  // Loop and find "</cf"
                  FindTags(cfCloseTag, line, globalVars);

                  // Loop and find comments
                  FindComments(commentStart, line, globalVars);
               }
               // In an openCFTag
               else
               {
                  FindCloseToOpenTag(line, globalVars);
               }
               // When there is an addition to CFCode we add a newline
               if (!oldCFCode.equals(globalVars.CFCode))
               {
                  globalVars.CFCode += "\n";
               }
            }
         }

         // Anything leftover is HTML
         if (!globalVars.tempLine.trim().isEmpty() || (globalVars.openCFTag == false && blankLine == true))
         {
            if (globalVars.fileLanguage.equals("XML"))
            {
               globalVars.XMLCode += (globalVars.tempLine + "\n");
            }
            else
            {
               globalVars.HTMLCode += (globalVars.tempLine + "\n");
            }
         }
      }
   }

   /**
    * Function for finding end tag for an open comment
    *
    * @param line
    *           String coming in from file
    * @param globalVars
    *           Object that holds all the global variables
    *
    */
   public void FindCommentClose(String line, globals globalVars)
   {
      String regEx;
      Matcher matcher;
      int endTagIndex = 0;
      String addToLine = "";

      regEx = "\\Q" + commentEnd + "\\E";
      matcher = Pattern.compile(regEx).matcher(line);

      // Found end of multi-line comment
      if (matcher.find())
      {
         endTagIndex = matcher.start();
         addToLine = line.substring(0, endTagIndex + commentEnd.length());
         globalVars.CFCode += addToLine;
         globalVars.tempLine = globalVars.tempLine.replace(addToLine, "");

         // Loop and find "<cf"
         FindTags(cfTag, line, globalVars);

         // Loop and find "</cf"
         FindTags(cfCloseTag, line, globalVars);

         // Loop and find comments
         FindComments(commentStart, line, globalVars);
      }
      // Still in multi-line comment
      else
      {
         globalVars.CFCode += line;
         globalVars.tempLine = globalVars.tempLine.replace(line, "");
      }
      globalVars.CFCode += "\n";
   }

   /**
    * Function for finding start tags in ColdFusion
    * 
    * @param tag
    *           Tag we are looking for in line
    * @param line
    *           String coming in from file
    * @param globalVars
    *           Object that holds all the global variables
    *
    */
   public void FindTags(String tag, String line, globals globalVars)
   {
      String regEx;
      Matcher matcher;
      int startTagIndex = 0;
      int endTagIndex = 0;
      String addToLine = "";

      regEx = "\\Q" + tag + "\\E";
      matcher = Pattern.compile(regEx).matcher(line);

      // Found a start tag
      while (matcher.find(startTagIndex))
      {
         startTagIndex = matcher.start();
         regEx = "\\Q" + endTag + "\\E";
         matcher = Pattern.compile(regEx).matcher(line);
         endTagIndex = startTagIndex;
         boolean endTagChecker = false;

         // Found an end tag
         while (matcher.find(endTagIndex))
         {
            endTagIndex = matcher.start();
            endTagChecker = true;
            String endCommentCheck = line.substring(endTagIndex - commentEnd.length() + 1, endTagIndex + 1);

            // Check that end tag is not an end comment tag
            if (!endCommentCheck.equals(commentEnd))
            {
               addToLine = line.substring(startTagIndex, endTagIndex + 1);
               globalVars.CFCode += addToLine;
               globalVars.tempLine = globalVars.tempLine.replace(addToLine, "");
               break;
            }
            endTagIndex++;
            regEx = "\\Q" + endTag + "\\E";
            matcher = Pattern.compile(regEx).matcher(line);
         }
         // Found the star tag but not the end tag so grab everything
         if (!endTagChecker)
         {
            addToLine = line.substring(startTagIndex, line.length());
            globalVars.CFCode += addToLine;
            globalVars.tempLine = globalVars.tempLine.replace(addToLine, "");
            globalVars.openCFTag = true;
            break;
         }
         startTagIndex++;
         regEx = "\\Q" + tag + "\\E";
         matcher = Pattern.compile(regEx).matcher(line);
      }
   }

   /**
    * Function for finding the closing tag to an open tag
    * 
    * @param line
    *           String coming in from file
    * @param globalVars
    *           Object that holds all the global variables
    *
    */
   public void FindCloseToOpenTag(String line, globals globalVars)
   {
      String regEx;
      Matcher matcher;
      int endTagIndex = 0;
      String addToLine = "";

      regEx = "\\Q" + endTag + "\\E";
      matcher = Pattern.compile(regEx).matcher(line);

      int previous = 0;

      // Found an end tag
      while (matcher.find(endTagIndex))
      {
         endTagIndex = matcher.start();
         String endCommentCheck = line.substring(endTagIndex - commentEnd.length() + 1, endTagIndex + 1);

         // Check that end tag is not an end comment tag
         if (!endCommentCheck.equals(commentEnd))
         {
            globalVars.openCFTag = false;
            addToLine = line.substring(previous, endTagIndex + 1);
            globalVars.CFCode += addToLine;
            globalVars.tempLine = globalVars.tempLine.replace(addToLine, "");

            // Loop and find "<cf"
            FindTags(cfTag, line, globalVars);

            // Loop and find "</cf"
            FindTags(cfCloseTag, line, globalVars);

            // Loop and find comments
            FindComments(commentStart, line, globalVars);
            break;
         }
         // Found end tag but it was end of comment
         else
         {
            addToLine = line.substring(0, endTagIndex + 1);
            globalVars.CFCode += addToLine;
            globalVars.tempLine = globalVars.tempLine.replace(addToLine, "");
            previous = endTagIndex + 1;
         }
         endTagIndex++;
      }
      // Did not find end tag and did not remove all of tempLine
      if (!globalVars.tempLine.trim().isEmpty())
      {
         globalVars.CFCode += globalVars.tempLine;
         globalVars.tempLine = "";
      }
   }

   /**
    * Function for finding comments in the line
    * 
    * @param commentTag
    *           The comment tag we are looking for in the line
    * @param line
    *           String coming in from file
    * @param globalVars
    *           Object that holds all the global variables
    *
    */
   public void FindComments(String commentTag, String line, globals globalVars)
   {
      String regEx;
      Matcher matcher;
      int startTagIndex = 0;
      int endTagIndex = 0;
      String addToLine = "";

      regEx = "\\Q" + commentTag + "\\E";
      matcher = Pattern.compile(regEx).matcher(line);

      // Found a comment start tag
      while (matcher.find(startTagIndex))
      {
         boolean commentChecker = false;
         startTagIndex = matcher.start();
         endTagIndex = startTagIndex;
         regEx = "\\Q" + commentEnd + "\\E";
         matcher = Pattern.compile(regEx).matcher(line);

         // Found comment end tag
         while (matcher.find(endTagIndex))
         {
            commentChecker = true;
            endTagIndex = matcher.start();
            addToLine = line.substring(startTagIndex, endTagIndex + commentEnd.length());

            // Make sure that this comment was not already added
            if (globalVars.tempLine.contains(addToLine))
            {
               globalVars.CFCode += addToLine;
               globalVars.tempLine = globalVars.tempLine.replace(addToLine, "");
               break;
            }
            endTagIndex++;
            regEx = "\\Q" + commentEnd + "\\E";
            matcher = Pattern.compile(regEx).matcher(line);
         }
         // Found a comment start tag but not an end tag
         if (!commentChecker)
         {
            globalVars.openCommentTag = true;
            addToLine = line.substring(startTagIndex, line.length());
            globalVars.CFCode += addToLine;
            globalVars.tempLine = globalVars.tempLine.replace(addToLine, "");
            endTagIndex = line.length();
            break;
         }
         startTagIndex++;
         regEx = "\\Q" + commentTag + "\\E";
         matcher = Pattern.compile(regEx).matcher(line);
      }
   }

   /**
    * Function for finding if a line contains a start tag for an embedded
    * language.
    * 
    * @param StartTags
    *           ArrayList of start tags for a particular language
    * @param EndTags
    *           ArrayList of end tags for a particular language
    * @param langState
    *           The language state we change to if start tags are found
    * @param savedCode
    *           String for saving embedded code
    * @param line
    *           String coming in from file
    * @param globalVars
    *           Object that holds all the global variables
    * @return line with embedded code removed
    */
   public String SearchForEmbeddedStart(ArrayList<String> StartTags, ArrayList<String> EndTags, LanguageState langState,
            String savedCode, String line, globals globalVars)
   {
      String regEx;
      Matcher matcher;
      String tempLine = "";

      // Look for start tags
      for (int st = 0; st < StartTags.size(); st++)
      {
         regEx = "\\Q" + StartTags.get(st) + "\\E";
         matcher = Pattern.compile(regEx).matcher(line.toLowerCase());

         // If we find one
         if (matcher.find())
         {
            // Set the state to the new language
            globalVars.languageState = langState;

            // Grab the rest of the line after the open tag
            tempLine = line.substring(matcher.start()).trim();

            // If the leftovers aren't empty, send the leftovers to the embedded
            // handler
            if (!tempLine.isEmpty())
            {
               line = line.substring(0, matcher.start())
                        + HandleEmbeddedCode(EndTags, langState, savedCode, tempLine, globalVars);
            }
            else
            {
               line = line.substring(0, matcher.end());
            }
         }
      }

      return line;
   }

   /**
    * Function for finding if a line contains a start tag for an embedded
    * language, except that this function handles start tags that can span
    * multiple lines.
    * 
    * @param StartTags
    *           ArrayList of start tags for a particular language
    * @param EndTags
    *           ArrayList of end tags for a particular language
    * @param langState
    *           The language state we change to if end tags are found
    * @param savedCode
    *           String for saving embedded code
    * @param line
    *           String coming in from file
    * @param globalVars
    *           Object that holds all the global variables
    * @return line with embedded code removed
    */
   public String SearchForMultiLineEmbeddedStart(ArrayList<String> StartTags, ArrayList<String> EndTags,
            LanguageState langState, String savedCode, String line, globals globalVars)
   {
      String regEx;
      Matcher matcher;
      String tempLine = "";

      // If we haven't already found a start tag
      if (globalVars.openStartTag == false)
      {
         // Look for start tags
         for (int st = 0; st < StartTags.size(); st++)
         {
            regEx = "\\Q" + StartTags.get(st) + "\\E";
            matcher = Pattern.compile(regEx).matcher(line.toLowerCase());

            // If we find one
            if (matcher.find())
            {
               globalVars.openStartTag = true;
               break;
            }
         }
      }

      // If we found an open start tag, but haven't found the close
      if (globalVars.openStartTag == true)
      {
         regEx = "\\>";
         matcher = Pattern.compile(regEx).matcher(line);

         // If we find one
         if (matcher.find())
         {
            globalVars.openStartTag = false;

            // Set the state to the new language
            globalVars.languageState = langState;

            // Grab the rest of the line after the open tag
            tempLine = line.substring(matcher.end()).trim();

            // If the leftovers aren't empty, send the leftovers to the embedded
            // handler
            if (!tempLine.isEmpty())
            {
               line = line.substring(0, matcher.end())
                        + HandleEmbeddedCode(EndTags, langState, savedCode, tempLine, globalVars);
            }
            else
            {
               line = line.substring(0, matcher.end());
            }
         }
      }

      return line;
   }

   /**
    * Function for saving embedded code into formatted strings until we find an
    * end tag.
    * 
    * @param EndTags
    *           ArrayList of end tags for a particular language
    * @param langState
    *           The language state we change to if end tags are found
    * @param savedCode
    *           String for saving embedded code
    * @param line
    *           String coming in from file
    * @param globalVars
    *           Object that holds all the global variables
    * @return line with embedded code removed
    */
   public String HandleEmbeddedCode(ArrayList<String> EndTags, LanguageState langState, String savedCode, String line,
            globals globalVars)
   {
      String regEx;
      Matcher matcher;
      String tempLine = line.trim();

      line = "";

      // If the line isn't empty
      if (!tempLine.isEmpty())
      {
         // Look for end tags
         for (int et = 0; et < EndTags.size(); et++)
         {
            regEx = "\\Q" + EndTags.get(et) + "\\E";
            matcher = Pattern.compile(regEx).matcher(tempLine.toLowerCase());

            // If we find one
            if (matcher.find())
            {
               // Reset the state back to the original
               globalVars.setupLangState();
               // Include the end tag in the original line
               line = tempLine.substring(matcher.end());

               // Get the leftover code on the code line
               tempLine = tempLine.substring(0, matcher.end());

               if (tempLine.contains("</cfquery"))
               {
                  line = tempLine.substring(0, matcher.end());
                  tempLine = tempLine.substring(matcher.end());
               }

               // Kick out of the loop
               break;
            }
         }

         // If the leftover code isn't empty
         if (!tempLine.trim().isEmpty())
         {
            tempLine += "\n";
            savedCode += tempLine;
         }
      }
      else // Handle empty lines
      {
         savedCode += "\n";
      }

      SaveCode(savedCode, langState, globalVars);

      return line;
   }

   /**
    * Function for saving the summed up formatted strings for each embedded
    * language into their respectively named strings.
    * 
    * @param savedCode
    *           String for saving embedded code
    * @param langState
    *           The language state we are currently in
    * @param globalVars
    *           Object that holds all the global variables
    *
    */
   public void SaveCode(String savedCode, LanguageState langState, globals globalVars)
   {
      if (langState == LanguageState.IN_HTML)
      {
         globalVars.HTMLCode = savedCode;
      }
      else if (langState == LanguageState.IN_CSS)
      {
         globalVars.CSSCode = savedCode;
      }
      else if (langState == LanguageState.IN_JS)
      {
         globalVars.JSCode = savedCode;
      }
      else if (langState == LanguageState.IN_PHP)
      {
         globalVars.PHPCode = savedCode;
      }
      else if (langState == LanguageState.IN_VBS)
      {
         globalVars.VBSCode = savedCode;
      }
      else if (langState == LanguageState.IN_JSP)
      {
         globalVars.JSPCode = savedCode;
      }
      else if (langState == LanguageState.IN_ASP)
      {
         globalVars.ASPCode = savedCode;
      }
      else if (langState == LanguageState.IN_CF)
      {
         globalVars.CFCode = savedCode;
      }
      else if (langState == LanguageState.IN_CFS)
      {
         globalVars.CFSCode = savedCode;
      }
      else if (langState == LanguageState.IN_SQL)
      {
         globalVars.SQLCode = savedCode;
      }
      else if (langState == LanguageState.IN_XML)
      {
         globalVars.XMLCode = savedCode;
      }
   }

   /**
    * Function for printing and/or writing the accumulated embedded code to
    * file.
    * 
    * @param cntrResults
    *           A UCCFile object ArrayList to store results of code counters
    * @param i
    *           The index of the UCCFile we want to work on
    * @param globalVars
    *           Object that holds all the global variables
    */
   public void PrintCode(ArrayList<UCCFile> cntrResults, int i, globals globalVars)
   {
      UCCFile cntrResult = cntrResults.get(i);

      // Print embedded CSS code
      if (!globalVars.CSSCode.trim().isEmpty())
      {
         globalVars.CSSCode = globalVars.CSSCode.substring(0, globalVars.CSSCode.length() - 1);

         WriteFile(cntrResult.FileName, ".EMB.css", globalVars.CSSCode, DataTypes.LanguagePropertiesType.CSS,
                  cntrResults, i);
      }

      // Print embedded JS code
      if (!globalVars.JSCode.trim().isEmpty())
      {
         globalVars.JSCode = globalVars.JSCode.substring(0, globalVars.JSCode.length() - 1);

         WriteFile(cntrResult.FileName, ".EMB.js", globalVars.JSCode, DataTypes.LanguagePropertiesType.JAVASCRIPT,
                  cntrResults, i);
      }

      // Print embedded PHP code
      if (!globalVars.PHPCode.trim().isEmpty())
      {
         globalVars.PHPCode = globalVars.PHPCode.substring(0, globalVars.PHPCode.length() - 1);

         WriteFile(cntrResult.FileName, ".EMB.php", globalVars.PHPCode, DataTypes.LanguagePropertiesType.PHP,
                  cntrResults, i);
      }

      // Print embedded VBS code
      if (!globalVars.VBSCode.trim().isEmpty())
      {
         globalVars.VBSCode = globalVars.VBSCode.substring(0, globalVars.VBSCode.length() - 1);

         WriteFile(cntrResult.FileName, ".EMB.vbs", globalVars.VBSCode, DataTypes.LanguagePropertiesType.VB_SCRIPT,
                  cntrResults, i);
      }

      // Print embedded JSP code
      if (!globalVars.JSPCode.trim().isEmpty())
      {
         globalVars.JSPCode = globalVars.JSPCode.substring(0, globalVars.JSPCode.length() - 1);

         WriteFile(cntrResult.FileName, ".EMB.jsp", globalVars.JSPCode, DataTypes.LanguagePropertiesType.JSP,
                  cntrResults, i);
      }

      // Print embedded ASP code
      if (!globalVars.ASPCode.trim().isEmpty())
      {
         globalVars.ASPCode = globalVars.ASPCode.substring(0, globalVars.ASPCode.length() - 1);

         WriteFile(cntrResult.FileName, ".EMB.asp", globalVars.ASPCode, DataTypes.LanguagePropertiesType.ASP,
                  cntrResults, i);
      }

      // Print embedded HTML code
      if (!globalVars.HTMLCode.trim().isEmpty())
      {
         globalVars.HTMLCode = globalVars.HTMLCode.substring(0, globalVars.HTMLCode.length() - 1);

         WriteFile(cntrResult.FileName, ".EMB.html", globalVars.HTMLCode, DataTypes.LanguagePropertiesType.HTML,
                  cntrResults, i);
      }

      // Print embedded COLDFUSION code
      if (!globalVars.CFCode.trim().isEmpty())
      {
         globalVars.CFCode = globalVars.CFCode.substring(0, globalVars.CFCode.length() - 1);

         WriteFile(cntrResult.FileName, ".EMB.cfm", globalVars.CFCode, DataTypes.LanguagePropertiesType.COLDFUSION,
                  cntrResults, i);
      }

      // Print embedded COLDFUSION SCRIPT code
      if (!globalVars.CFSCode.trim().isEmpty())
      {
         globalVars.CFSCode = globalVars.CFSCode.substring(0, globalVars.CFSCode.length() - 1);

         WriteFile(cntrResult.FileName, ".EMB.cfs", globalVars.CFSCode,
                  DataTypes.LanguagePropertiesType.COLDFUSION_SCRIPT, cntrResults, i);
      }

      // Print embedded SQL code
      if (!globalVars.SQLCode.trim().isEmpty())
      {
         globalVars.SQLCode = globalVars.SQLCode.substring(0, globalVars.SQLCode.length() - 1);

         WriteFile(cntrResult.FileName, ".EMB.sql", globalVars.SQLCode, DataTypes.LanguagePropertiesType.SQL,
                  cntrResults, i);
      }

      // Print embedded XML code
      if (!globalVars.XMLCode.trim().isEmpty())
      {
         globalVars.XMLCode = globalVars.XMLCode.substring(0, globalVars.XMLCode.length() - 1);

         WriteFile(cntrResult.FileName, ".EMB.xml", globalVars.XMLCode, DataTypes.LanguagePropertiesType.XML,
                  cntrResults, i);
      }
   }

   /**
    * Function for writing the embedded code to file and assigning it to the
    * file list.
    * 
    * @param fileName
    *           Name of file we want to save
    * @param fileExt
    *           Extension of file we want to save
    * @param savedCode
    *           String full of saved embedded code
    * @param lang
    *           The language property type we want associated with the file
    * @param cntrResults
    *           A UCCFile object ArrayList to store results of code counters
    * @param i
    *           The index of the UCCFile we want to work on
    */
   public void WriteFile(String fileName, String fileExt, String savedCode, LanguagePropertiesType lang,
            ArrayList<UCCFile> cntrResults, int i)
   {
      File file = new File(FileUtils.BuildTempOutFileName_embeddedCodeFile(RtParams, cntrResults.get(i), fileExt));
      BufferedWriter bw;
      try
      {
         bw = new BufferedWriter(new FileWriter(file));
         bw.write(savedCode);
         if (bw != null)
         {
            try
            {
               bw.close();

               cntrResults.get(i).EmbOfIdx = -2;
               cntrResults.add(new UCCFile());
               cntrResults.get(cntrResults.size() - 1).FileName =
                        FileUtils.BuildTempOutFileName_embeddedCodeFile(RtParams, cntrResults.get(i), fileExt);
               cntrResults.get(cntrResults.size() - 1).LangProperty = lang;
               cntrResults.get(cntrResults.size() - 1).EmbOfIdx = i;
               cntrResults.get(cntrResults.size() - 1).UCCFileParent = cntrResults.get(i);
               cntrResults.get(cntrResults.size() - 1).UniqueFileName = cntrResults.get(i).UniqueFileName;
            }
            catch (IOException e)
            {
               logger.error("The embedded " + fileExt + " writer failed to close.");
               logger.error(e);
            }
            bw = null;
         }
      }
      catch (IOException e)
      {
         logger.error("The embedded " + fileExt + " writer failed to open.");
         logger.error(e);
      }
   }
}