package ucc.counters;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ucc.datatypes.Constants;
import ucc.datatypes.UCCFile;
import ucc.langprops.LanguageProperties;
import ucc.utils.FileUtils;
import ucc.utils.TimeUtils;

/**
 * JavaScriptCounter class performs various code counting operations on
 * baseline(s) identified by the user. It contains algorithms/methods to count
 * JavaScript programming language files.
 *
 * @author Integrity Applications Incorporated
 *
 */
public class JavaScriptCounter extends CodeCounter
{
   /** Instantiate the Log4j2 logger for this class */
   private static final Logger logger = LogManager.getLogger(JavaScriptCounter.class);

   /**
    * Default constructor to instantiate a JavaCounter object
    * 
    * @param langProps
    *           Language properties for this counter
    */
   public JavaScriptCounter(LanguageProperties langProps)
   {
      // Call super class's constructor
      super(langProps);
   }

   /**
    * Computes Source Lines of Code metrics for given file. Metrics include:
    * Physical Source Line of Code (PSLOC) counts Logical Source Line of Code
    * (LSLOC) counts Complexity keyword counts Cyclomatic complexity counts
    */
   public void CountSLOC(UCCFile cntrResult)
   {
      // Count PSLOC and complexity
      CountFilePSLOC(cntrResult);

      // Count LSLOC and cyclomatic complexity
      CountFileLSLOC(cntrResult);
   }

   /**
    * Counts Physical SLOC. Current algorithm:
    * 
    * Read first line
    * 
    * Delete the UTF-8 BOM, if found
    * 
    * While we have lines in the file... { - Do some pre-processing - Check for
    * unclosed quotes - Count comments with handler (also counts blank lines) -
    * Count compiler directives with handler - Count non-blank lines - Do some
    * post-processing - Calculate PSLOC as non-blank lines + compiler directives
    * + number of continued compiler directives (these were erased in the
    * compiler handler) - Save post-processed line in new file for LSLOC
    * counting - Read next line }
    */
   protected void CountFilePSLOC(UCCFile cntrResult)
   {
      long startTime = 0;
      long endTime = 0;
      startTime = TimeUtils.GetTime();
      logger.debug(TimeUtils.PrintTimestamp() + " Counting PSLOC for " + cntrResult.FileName);

      LineNumberReader reader = null;

      // Pattern finder variables
      int openPos = 0;
      int closePos = 0;

      String regEx;
      Matcher matcher;

      // Buffered writer for _PSLOC file saving
      BufferedWriter bw = null;

      boolean cdNewLine = false;

      try
      {
         // Create file for PSLOC storage
         File file = new File(FileUtils.BuildTempOutFileName_PSLOC(RtParams, cntrResult));
         bw = new BufferedWriter(new FileWriter(file));

         reader = new LineNumberReader(
                  new InputStreamReader(new FileInputStream(cntrResult.FileName), Constants.CHARSET_NAME));

         // Read first line
         String line = reader.readLine();

         // Delete UTF-8 BOM instance
         line = DeleteUTF8BOM(line);

         // Reset truncated lines count
         truncateLinesCount = 0;

         // Pre-processing Variables
         boolean inMultiLineComment = false;
         boolean inStringLiteral = false;
         String closingStringLiteral = "";

         // While we have lines in the file...
         while (line != null)
         {
            // Ignore line data beyond the single line threshold value
            if (RtParams.TruncThreshold != 0 && line.length() > RtParams.TruncThreshold)
            {
               line = line.substring(0, RtParams.TruncThreshold);
               truncateLinesCount++;
            }

            logger.trace("line " + reader.getLineNumber() + " in:  " + line);

            /* PREPROCESSING START */

            line = line.trim();

            // //--> was intended to allow browsers that didn't understand the
            // <script> tag to degrade gracefully. This is not a comment.
            line = line.replaceAll("//-->", "-->");

            logger.trace("line " + reader.getLineNumber() + " in:  " + line);
            CounterUtils cu = new CounterUtils(this);
            CounterUtils.ResultObject ro = cu.PreformPreProcessing(line, cntrResult, inMultiLineComment,
                     inStringLiteral, closingStringLiteral);
            line = ro.line;
            inMultiLineComment = ro.inMultiLineComment;
            inStringLiteral = ro.inStringLiteral;
            truncateLinesCount += ro.truncateLinesCount;
            closingStringLiteral = ro.closingStringLiteral;

            /* PREPROCESSING FINISH */

            line = line.trim();

            /* POSTPROCESSING START */

            if (!line.isEmpty())
            {
               logger.trace("line " + reader.getLineNumber() + " out: " + line);
               // Delete the contents of for loops
               closePos = 0;
               regEx = "\\bfor\\b";
               matcher = Pattern.compile(regEx).matcher(line);
               while (closePos >= 0 && matcher.find(closePos))
               {
                  openPos = line.indexOf("(", matcher.end());
                  if (openPos != -1)
                  {
                     closePos = FindClose(line, openPos, '(', ')');
                     if (closePos != -1)
                     {
                        line = line.substring(0, openPos + "(".length()) + line.substring(closePos, line.length());
                     }
                  }
                  else
                  {
                     break;
                  }
               }

               regEx = "\\bvar\\b";
               matcher = Pattern.compile(regEx).matcher(line);
               if (matcher.find())
               {
                  line = line.replaceAll(regEx, "\nvar");
               }

               for (int lt = 0; lt < LineTerminator.size(); lt++)
               {
                  line = line.replaceAll(LineTerminator.get(lt), LineTerminator.get(lt) + "\n");
               }

               // Add a newline after all open curly braces
               // line = line.replaceAll("\\{", "\\{\n");

               // Add a new line after all compiler directives
               cdNewLine = false;
               for (int cd = 0; cd < CompilerDir.size(); cd++)
               {
                  if (line.contains(CompilerDir.get(cd)))
                  {
                     line += "\n";
                     cdNewLine = true;
                  }
               }
               if (!cdNewLine)
               {
                  for (int cc = 0; cc < CompilerChar.size(); cc++)
                  {
                     if (line.contains(CompilerChar.get(cc)))
                     {
                        line += "\n";
                     }
                  }
               }

               // Loop through the LSLOC keywords list
               for (int lk = 0; lk < LslocKeywords.size() && !line.isEmpty(); lk++)
               {
                  boolean isFound = line.contains(LslocKeywords.get(lk));

                  // Add a new line when we find a LSLOC keyword
                  if (isFound)
                  {
                     line += "\n";
                  }
               }

               // Replace leading whitespace for new lines
               while (line.contains("\n "))
               {
                  line = line.replaceAll("\n ", "\n");
               }
            }

            for (int fk = 0; fk < FunctionKeywords.size() && !line.isEmpty(); fk++)
            {
               regEx = "\\b" + FunctionKeywords.get(fk) + "\\b";
               matcher = Pattern.compile(regEx).matcher(line);

               if (matcher.find())
               {
                  line += "\n";
               }
            }

            if (!line.trim().isEmpty())
            {
               // line += " ";
               bw.write(line);
            }

            /* POSTPROCESSING FINISH */

            // Read next line
            line = reader.readLine();
         }
         endTime = TimeUtils.GetTime();
         String executedTime = TimeUtils.CalcElapsedTime(startTime, endTime);
         logger.debug("Finished counting PSLOC for " + cntrResult.FileName + " Exec. Time: " + executedTime);
      }
      catch (IOException e)
      {
         logger.error("The input reader failed to open.");
         logger.error(e);
      }
      finally
      {
         // If the original file was opened...
         if (reader != null)
         {
            // Save PSLOC metrics counted
            cntrResult.IsCounted = true;
            cntrResult.NumTotalLines = reader.getLineNumber();
            cntrResult.LangVersion = LangProps.GetLangVersion();

            // Close the original file
            try
            {
               reader.close();
            }
            catch (IOException e)
            {
               logger.error("The input reader failed to close.");
               logger.error(e);
            }
            reader = null;
         }

         // If the _PSLOC file was opened...
         if (bw != null)
         {
            try
            {
               bw.close();
            }
            catch (IOException e)
            {
               logger.error("The PSLOC writer failed to close.");
               logger.error(e);
            }
            bw = null;
         }

         // Log the number of lines in the file that were truncated
         if (truncateLinesCount > 0)
         {
            logger.warn("Truncated " + truncateLinesCount + " total lines in file " + cntrResult.FileName
                     + "  [-trunc set to " + Integer.toString(RtParams.TruncThreshold) + "]");
         }
      }
   }

   /**
    * Counts Logical SLOC. Current algorithm:
    * 
    * Read first line
    * 
    * Delete the UTF-8 BOM, if found
    * 
    * While we have lines in the file... { - Build true LSLOC and save to LSLOC
    * file - Search for duplicates based on threshold - Delete compiler
    * directives - Delete everything between quotes - Count data keywords -
    * Count executable keywords - Count complexity keywords - Increment
    * complexity loop level based on keyword - Increment cyclomatic complexity
    * loop level based on keyword - Count LSLOC keywords - Count data
    * declarations - Delete exclude keywords and characters
    * 
    * If the line is not empty, increment the LSLOC counter to catch leftover
    * LSLOC
    * 
    * Read next line }
    * 
    * @param cntrResult
    *           A UCCFile object to store results of code counter
    */
   protected void CountFileLSLOC(UCCFile cntrResult)
   {
      long startTime = 0;
      long endTime = 0;
      startTime = TimeUtils.GetTime();
      logger.debug(TimeUtils.PrintTimestamp() + " Counting LSLOC for " + cntrResult.FileName);

      globals globalVars = new globals();
      tempLoop tempLoopObj = new tempLoop();
      complexity complexityObj = new complexity();
      cyclomaticComplexity cyclomaticComplexityObj = new cyclomaticComplexity();

      LineNumberReader reader = null;

      int lsloc = 0; // Logical SLOC counter
      globalVars.lslocKeywordsCount = 0; // LSLOC keyword counter

      String tempLine = ""; // String for storing a temporary version of the
                            // line
      int lineIndex = 0; // Index of the line used for checksumming lines in
                         // sequence

      globalVars.lslocLineValue = 0;
      File file = null;

      String regEx;
      Matcher matcher;

      // Initialize complexity keywords/counts for this file
      InitAllCmplxKeywords(cntrResult);

      // Buffered writer for _LSLOC file saving
      BufferedWriter bw = null;

      try
      {
         // If we're differencing baselines...
         if (RtParams.DiffCode)
         {
            if (cntrResult.EmbOfIdx == -2)
            {
               // Create file for LSLOC storage
               file = new File(FileUtils.BuildTempOutFileName_LSLOC(RtParams, cntrResult));
               bw = new BufferedWriter(new FileWriter(file));
            }
            else
            {
               // Create file for LSLOC storage
               file = new File(FileUtils.BuildTempOutFileName_LSLOC_Embedded(RtParams, cntrResult.UCCFileParent));

               // If LSLOC file doesn't already exist, then create it
               if (!file.exists())
               {
                  file.createNewFile();
               }

               // true = append file
               FileWriter fileWritter = new FileWriter(file.getAbsolutePath(), true);
               bw = new BufferedWriter(fileWritter);
            }
         }

         reader = new LineNumberReader(
                  new InputStreamReader(new FileInputStream(FileUtils.BuildTempOutFileName_PSLOC(RtParams, cntrResult)),
                           Constants.CHARSET_NAME));

         // Read first line
         String line = reader.readLine();

         // Delete UTF-8 BOM instance
         line = DeleteUTF8BOM(line);

         // While we have lines in the file...
         while (line != null)
         {
            // Reset conditional
            globalVars.lslocLineValue = 0;

            logger.trace("line " + reader.getLineNumber() + " in:  " + line);

            // If we're baseline differencing or searching for duplicates...
            if (RtParams.DiffCode || RtParams.SearchForDups)
            {
               // Save line into a temporary line for LSLOC writing
               tempLine = line;

               // Delete all exclude keywords
               tempLine = DeleteExcludeKeywords(tempLine);

               // Delete exclude characters
               tempLine = DeleteExcludeCharacters(tempLine);

               // Delete all line terminators (e.g. for instances where the line
               // ends up being ;)
               for (int lt = 0; lt < LineTerminator.size(); lt++)
               {
                  tempLine = tempLine.replaceAll(LineTerminator.get(lt), "");
               }
            }

            // Count executable keyword occurrences
            CountKeywords(cntrResult.ExecKeywordCnts, line);

            // Count data keyword occurrences
            CountKeywords(cntrResult.DataKeywordCnts, line);

            line = line.trim();

            // Count complexity keywords
            if (RtParams.CountCmplxMetrics && !line.isEmpty())
            {
               CountComplexity(cntrResult, line);
            }

            // Delete everything inside of loops
            for (int lk = 0; lk < LslocKeywords.size() && !line.isEmpty(); lk++)
            {
               line = DeleteLoopContents(line, LslocKeywords.get(lk));
            }

            // Count data declarations
            line = CountDataDeclarations(cntrResult, line, globalVars);

            // Count LSLOC keywords
            line = CountLSLOCKeywords(line, globalVars);

            // Count function keywords
            CountFunctionKeywords(line, globalVars);

            // Count LSLOC
            if (!line.trim().isEmpty())
            {
               // Make sure we don't double count a while
               if (!globalVars.whileChecker)
               {
                  // Count number of line terminators on the line
                  for (int lt = 0; lt < LineTerminator.size(); lt++)
                  {
                     regEx = LineTerminator.get(lt);
                     matcher = Pattern.compile(regEx).matcher(line);
                     while (matcher.find())
                     {
                        logger.trace("line " + reader.getLineNumber() + " out: " + line);
                        lsloc++;
                        globalVars.lslocLineValue++;
                     }
                  }
               }
            }

            // Write the Raw LSLOC Line for Differencer
            if (globalVars.lslocLineValue > 0)
            {
               // If we're baseline differencing, write the LSLOC line
               if (RtParams.DiffCode)
               {
                  bw.write(lslocLineValueDelim + Integer.toString(globalVars.lslocLineValue) + lslocLineValueDelim
                           + tempLine + "\n");
               }

               // If we're searching for duplicates, checksum the LSLOC line
               if (RtParams.SearchForDups)
               {
                  // If we're in a pure JavaScript file
                  if (cntrResult.EmbOfIdx == -2)
                  {
                     if (!cntrResult.UniqueFileName)
                     {
                        cntrResult.FileLineChecksum.add(lineIndex, line.hashCode());
                     }
                  }
                  else // If we're in an embedded JavaScript file
                  {
                     if (!cntrResult.UCCFileParent.UniqueFileName)
                     {
                        cntrResult.UCCFileParent.FileLineChecksum.add(lineIndex, line.hashCode());
                     }
                  }
                  lineIndex++;
               }
            }

            // Read next line
            line = reader.readLine();
         }
         endTime = TimeUtils.GetTime();
         String executedTime = TimeUtils.CalcElapsedTime(startTime, endTime);
         logger.debug("Finished counting LSLOC for " + cntrResult.FileName + " Exec. Time: " + executedTime);
      }
      catch (IOException e)
      {
         logger.error("The PSLOC reader failed to open.");
         logger.error(e);
      }
      finally
      {
         // If the _PSLOC file was opened...
         if (reader != null)
         {
            // Save LSLOC metrics counted
            cntrResult.NumLSLOC = cntrResult.NumCompilerDirectives + cntrResult.NumDataDeclLog
                     + globalVars.lslocKeywordsCount + lsloc;
            cntrResult.NumExecInstrLog =
                     cntrResult.NumLSLOC - cntrResult.NumDataDeclLog - cntrResult.NumCompilerDirectives;
            cntrResult.NumDataDeclPhys = cntrResult.NumDataDeclLog;
            cntrResult.NumExecInstrPhys = cntrResult.NumExecInstrLog;
            cntrResult.LangVersion = LangProps.GetLangVersion();

            // Close the _PSLOC file
            try
            {
               reader.close();
            }
            catch (IOException e)
            {
               logger.error("The PSLOC reader failed to close.");
               logger.error(e);
            }
            reader = null;

            // Delete PSLOC file
            FileUtils.DeleteFile(FileUtils.BuildTempOutFileName_PSLOC(RtParams, cntrResult));
         }

         // If the _LSLOC file was opened...
         if (bw != null)
         {
            // Close the _LSLOC file
            try
            {
               bw.close();
            }
            catch (IOException e)
            {
               logger.error("The LSLOC writer failed to close.");
               logger.error(e);
            }
            bw = null;
         }
      }
   }

   /**
    * Function for counting and removing all LSLOC keywords and their (...)'s
    * 
    * @param line
    *           Line coming in from file
    * @param globalVars
    *           Class to hold global variables
    * @return Line with any LSLOC keywords removed
    */
   protected String CountLSLOCKeywords(String line, globals globalVars)
   {
      String regEx;
      Matcher matcher;
      StringBuffer sbLine = new StringBuffer();
      globalVars.whileChecker = false;

      line = line.trim();

      // Loop through the LSLOC keywords list
      for (int lk = 0; lk < LslocKeywords.size() && !line.isEmpty(); lk++)
      {
         regEx = "\\b" + LslocKeywords.get(lk) + "\\b";
         matcher = Pattern.compile(regEx).matcher(line);
         sbLine.setLength(0);

         // While we find LSLOC keywords on the line...
         while (matcher.find() && !line.isEmpty() && line.length() >= LslocKeywords.get(lk).length())
         {
            // Check if there is a ; after a while loop
            if (LslocKeywords.get(lk).equals("while") && String.valueOf(line.charAt(line.length() - 1)).equals(";"))
            {
               globalVars.whileChecker = true;
            }
            matcher.appendReplacement(sbLine, " ");

            // Tally them up
            globalVars.lslocKeywordsCount++;
            globalVars.lslocLineValue++;
         }
         matcher.appendTail(sbLine);
         line = sbLine.toString();
      }

      return line;
   }

   /**
    * Function for counting and removing all LSLOC keywords and their (...)'s
    * 
    * @param line
    *           Line coming in from file
    * @param globalVars
    *           Class to hold global variables
    * @return Line with any LSLOC keywords removed
    */
   protected String CountFunctionKeywords(String line, globals globalVars)
   {
      String regEx;
      Matcher matcher;
      StringBuffer sbLine = new StringBuffer();

      line = line.trim();

      // Loop through the LSLOC keywords list
      for (int fk = 0; fk < FunctionKeywords.size() && !line.isEmpty(); fk++)
      {
         regEx = "\\b" + FunctionKeywords.get(fk) + "\\b";
         matcher = Pattern.compile(regEx).matcher(line);
         sbLine.setLength(0);

         // While we find LSLOC keywords on the line...
         while (matcher.find() && !line.isEmpty() && line.length() >= FunctionKeywords.get(fk).length())
         {
            matcher.appendReplacement(sbLine, " ");

            // Tally them up
            globalVars.lslocKeywordsCount++;
            globalVars.lslocLineValue++;
         }
         matcher.appendTail(sbLine);
         line = sbLine.toString();
      }

      return line;
   }

   /**
    * Function for counting data declarations.
    *
    * @param cntrResult
    *           A UCCFile object to store results of code counter
    * @param line
    *           Line coming in from file
    * @return The modified line coming in
    */
   protected String CountDataDeclarations(UCCFile cntrResult, String line, globals globalVars)
   {
      String regEx;
      Pattern pattern;
      Matcher matcher;

      regEx = "\\bvar\\b";
      pattern = Pattern.compile(regEx);
      matcher = pattern.matcher(line.trim());
      if (matcher.find() && matcher.start() == 0)
      {
         line = "";
         cntrResult.NumDataDeclLog++;
         globalVars.lslocLineValue++;
      }

      return line;
   }
}