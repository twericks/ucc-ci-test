package ucc.counters;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ucc.datatypes.Constants;
import ucc.datatypes.UCCFile;
import ucc.langprops.LanguageProperties;
import ucc.utils.TimeUtils;

/**
 * DataFileCounter class generates the values of specified metrics for code
 * written in a text file.
 * 
 * @author Integrity Applications Incorporated
 * 
 */
public class DataFileCounter extends CodeCounter
{
   /** Instantiate the Log4j2 logger for this class */
   private static final Logger logger = LogManager.getLogger(DataFileCounter.class);

   /**
    * Default constructor to instantiate a DataFileCounter object
    * 
    * @param langProps
    *           Language properties for this counter
    */
   public DataFileCounter(LanguageProperties langProps)
   {
      // Call super class's constructor
      super(langProps);

   }

   /**
    * Computes Source Lines of Code metrics for given file. Metrics include:
    * Physical Source Line of Code (PSLOC) counts Logical Source Line of Code
    * (LSLOC) counts
    *
    * @param cntrResult
    *           A UCCFile object ArrayList to store results of code counters
    */
   public void CountSLOC(UCCFile cntrResult)
   {
      try
      {
         // Count PSLOC and complexity
         CountFileSLOC(cntrResult);
      }
      catch (Exception e)
      {
         logger.error("An error occurred while counting SLOC for " + cntrResult.FileName);
         logger.error(e.toString(), e);
      }
   }

   /**
    * Counts SLOC.
    * 
    * @param cntrResult
    *           A UCCFile object to store results of code counter
    */
   protected void CountFileSLOC(UCCFile cntrResult)
   {
      long startTime = 0;
      long endTime = 0;
      startTime = TimeUtils.GetTime();
      logger.debug(TimeUtils.PrintTimestamp() + " Counting SLOC for " + cntrResult.FileName);

      LineNumberReader reader = null;

      int lsloc = 0; // Logical SLOC
      int lineIndex = 0; // Index of the line used for checksumming lines in
                         // sequence

      // Initialize complexity keywords/counts for this file
      InitAllCmplxKeywords(cntrResult);

      String tempLine = "";

      try
      {

         reader = new LineNumberReader(
                  new InputStreamReader(new FileInputStream(cntrResult.FileName), Constants.CHARSET_NAME));

         // Read first line
         String line = reader.readLine();

         // Delete UTF-8 BOM instance
         line = DeleteUTF8BOM(line);

         // Pre-processing Variables
         boolean inMultiLineComment = false;
         boolean inStringLiteral = false;
         String closingStringLiteral = "";

         // While we have lines in the file...
         while (line != null)
         {
            tempLine = "";

            /* PREPROCESSING START */

            logger.trace("line " + reader.getLineNumber() + " in:  " + line);
            CounterUtils cu = new CounterUtils(this);
            CounterUtils.ResultObject ro = cu.PreformPreProcessing(line, cntrResult, inMultiLineComment,
                     inStringLiteral, closingStringLiteral);
            line = ro.line;

            /* PREPROCESSING FINISH */

            line = line.trim();

            // If we're baseline differencing or searching for duplicates...
            if (RtParams.DiffCode || RtParams.SearchForDups)
            {
               // Only write the LSLOC line if it is not empty
               if (!line.isEmpty())
               {
                  // If we're baseline differencing, write the LSLOC line
                  if (RtParams.DiffCode)
                  {
                     tempLine = line;
                  }

                  // If we're searching for duplicates, checksum the LSLOC line
                  if (RtParams.SearchForDups)
                  {

                     if (!cntrResult.UniqueFileName)
                     {
                        cntrResult.FileLineChecksum.add(lineIndex, line.hashCode());
                     }

                     lineIndex++;
                  }
               }
            }

            // Read next line
            line = reader.readLine();
         }
         endTime = TimeUtils.GetTime();
         String executedTime = TimeUtils.CalcElapsedTime(startTime, endTime);
         logger.debug("Finished counting SLOC for " + cntrResult.FileName + " Exec. Time: " + executedTime);
      }
      catch (IOException e)
      {
         logger.error("The input reader failed to open for " + cntrResult.FileName);
         logger.error(e.toString(), e);
      }
      catch (IndexOutOfBoundsException e)
      {
         logger.error("An error occurred while counting SLOC for " + cntrResult.FileName);
         logger.error(e.toString(), e);
      }
      finally
      {
         // If the original file was opened...
         if (reader != null)
         {
            // Save PSLOC metrics counted
            cntrResult.IsCounted = true;
            cntrResult.NumTotalLines = reader.getLineNumber();
            cntrResult.LangVersion = LangProps.GetLangVersion();
            cntrResult.NumLSLOC = lsloc;
            cntrResult.NumExecInstrLog = cntrResult.NumLSLOC;
            cntrResult.NumDataDeclPhys = cntrResult.NumDataDeclLog;
            cntrResult.NumExecInstrPhys = cntrResult.NumExecInstrLog;
            cntrResult.LangVersion = LangProps.GetLangVersion();

            // Close the original file
            try
            {
               reader.close();
            }
            catch (IOException e)
            {
               logger.error("The input reader failed to close.");
               logger.error(e);
            }
            reader = null;
         }
      }
   }
}