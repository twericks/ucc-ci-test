package ucc.counters;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Stack;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ucc.datatypes.CmplxDataType;
import ucc.datatypes.Constants;
import ucc.datatypes.UCCFile;
import ucc.langprops.LanguageProperties;
import ucc.utils.FileUtils;
import ucc.utils.TimeUtils;

/**
 * PythonCounter class performs various code counting operations on baseline(s)
 * identified by the user. It contains algorithms/methods to count Python
 * programming language files.
 *
 * @author Integrity Applications Incorporated
 *
 */
public class PythonCounter extends CodeCounter
{
   /** Instantiate the Log4j2 logger for this class */
   private static final Logger logger = LogManager.getLogger(PythonCounter.class);

   /**
    * Class for storing loop level variables needed for complexity metrics.
    */
   static class ComplexityObj
   {
      // Loop level counting variables for complexity metrics
      static ArrayList<Integer> loopLevelCount = new ArrayList<Integer>();
      static int loopLevel;
   }

   /**
    * An Arraylist to hold all operator count for each file
    */
   protected ArrayList<ArrayList<CmplxDataType>> UniqueOperatorList;

   /**
    * An Arraylist to hold all operand count for each file
    */
   protected ArrayList<TreeMap> UniqueOperandList;

   /**
    * Default constructor to instantiate a JavaCounter object
    * 
    * @param langProps
    *           Language properties for this counter
    */
   public PythonCounter(LanguageProperties langProps)
   {
      super(langProps); // Call super class's constructor
   }

   /**
    * Counts Physical SLOC. Current algorithm:
    * 
    * Read first line
    * 
    * Delete the UTF-8 BOM, if found
    * 
    * While we have lines in the file... { - Do some pre-processing - Check for
    * unclosed quotes - Count comments with handler (also counts blank lines) -
    * Count compiler directives with handler - Count non-blank lines - Do some
    * post-processing - Calculate PSLOC as non-blank lines + compiler directives
    * + number of continued compiler directives (these were erased in the
    * compiler handler) - Save post-processed line in new file for LSLOC
    * counting - Read next line }
    */
   protected void CountFilePSLOC(UCCFile cntrResult)
   {
      long startTime = 0;
      long endTime = 0;
      startTime = TimeUtils.GetTime();
      logger.debug(TimeUtils.PrintTimestamp() + " Counting PSLOC for " + cntrResult.FileName);

      LineNumberReader reader = null;

      // Pattern finder variables
      int openPos = 0;
      int closePos = 0;

      int parenPos = 0;
      int bracketPos = 0;
      int curlyBracketPos = 0;
      char openPattern = 0;
      char closePattern = 0;
      boolean multiLineFlag = false;
      String multiLine = "";
      String regEx = "";
      Matcher matcher;

      boolean continued = false;

      BufferedWriter bw = null;

      boolean cdNewLine = false;

      try
      {
         // Create file for PSLOC storage
         File file = new File(FileUtils.BuildTempOutFileName_PSLOC(RtParams, cntrResult));

         // Initialize BufferedWriter
         bw = new BufferedWriter(new FileWriter(file));

         // Initialize reader
         reader = new LineNumberReader(
                  new InputStreamReader(new FileInputStream(cntrResult.FileName), Constants.CHARSET_NAME));

         // Read first line
         String line = reader.readLine();

         // Delete UTF-8 BOM instance
         line = DeleteUTF8BOM(line);

         // Reset truncated lines count
         truncateLinesCount = 0;

         // Pre-processing Variables
         boolean inMultiLineComment = false;
         boolean inStringLiteral = false;
         String closingStringLiteral = "";

         // While we have lines in the file...
         while (line != null)
         {
            /* PREPROCESSING START */

            logger.trace("line " + reader.getLineNumber() + " in:  " + line);
            CounterUtils cu = new CounterUtils(this);
            CounterUtils.ResultObject ro = cu.PreformPreProcessing(line, cntrResult, inMultiLineComment,
                     inStringLiteral, closingStringLiteral);
            // Add a tempLine to save the indentation from pre-processing (Add
            // by 2019 Summer UCC-J Team)
            String tempLine = line;
            line = ro.line;
            inMultiLineComment = ro.inMultiLineComment;
            inStringLiteral = ro.inStringLiteral;
            truncateLinesCount += ro.truncateLinesCount;
            closingStringLiteral = ro.closingStringLiteral;

            /* PREPROCESSING FINISH */

            /* POSTPROCESSING START */

            if (!line.trim().isEmpty())
            {
               // Undo multi-line ()'s, []'s, {}'s
               if (multiLineFlag == true)
               {
                  line = multiLine + " " + line.trim();
               }

               // Find which open pattern is first
               parenPos = line.indexOf("(");
               bracketPos = line.indexOf("[");
               curlyBracketPos = line.indexOf("{");

               // If any of the patterns aren't found, set their index to a
               // large value
               if (parenPos == -1)
               {
                  parenPos = 99999;
               }

               if (bracketPos == -1)
               {
                  bracketPos = 99999;
               }

               if (curlyBracketPos == -1)
               {
                  curlyBracketPos = 99999;
               }

               // Find the first pattern found with Math.min
               openPos = -1;
               if (parenPos == Math.min(parenPos, Math.min(bracketPos, curlyBracketPos)) && parenPos != 99999)
               {
                  openPos = line.indexOf("(");
                  openPattern = '(';
                  closePattern = ')';
               }
               else if (bracketPos == Math.min(parenPos, Math.min(bracketPos, curlyBracketPos)) && bracketPos != 99999)
               {
                  openPos = line.indexOf("[");
                  openPattern = '[';
                  closePattern = ']';
               }
               else if (curlyBracketPos == Math.min(parenPos, Math.min(bracketPos, curlyBracketPos))
                        && curlyBracketPos != 99999)
               {
                  openPos = line.indexOf("{");
                  openPattern = '{';
                  closePattern = '}';
               }

               // Undo multi-line ()/[]/{}'s
               if (openPos != -1)
               {
                  closePos = FindClose(line, openPos, openPattern, closePattern);
                  if (closePos != -1)
                  {
                     multiLineFlag = false;
                     multiLine = "";
                  }
                  else
                  {
                     multiLineFlag = true;
                     multiLine = line;
                     line = "";
                  }
               }

               // Assume lines are not continued by default
               continued = false;

               // Undo official line continuations
               for (int lcc = 0; lcc < LineContChars.size(); lcc++)
               {
                  if (line.trim().endsWith(LineContChars.get(lcc)))
                  {
                     line += " ";
                     continued = true;
                     break;
                  }
               }

               // Undo unofficial line continuations

               // Case 1: lines ending in a calculation
               for (int ck = 0; ck < CalcKeywords.size(); ck++)
               {
                  if (line.trim().endsWith(CalcKeywords.get(ck)))
                  {
                     line += " ";
                     continued = true;
                     break;
                  }
               }

               // Case 2: lines ending in an assignment
               for (int ak = 0; ak < AssignKeywords.size(); ak++)
               {
                  if (line.trim().endsWith(AssignKeywords.get(ak)))
                  {
                     line += " ";
                     continued = true;
                     break;
                  }
               }

               // Case 3: lines ending in a '({[,'
               if (line.trim().endsWith("(") || line.trim().endsWith("{") || line.trim().endsWith("[")
                        || line.trim().endsWith(","))
               {
                  line += " ";
                  continued = true;
               }

               if (continued == false)
               {
                  line += "\n";
               }
            }

            // if ":" found in line w/ LSLOC keyword...
            for (int lk = 0; lk < LslocKeywords.size(); lk++)
            {
               regEx = "\\b" + LslocKeywords.get(lk) + "\\b";
               matcher = Pattern.compile(regEx).matcher(line.trim());

               if (matcher.find() && matcher.start() == 0)
               {
                  line = line.replaceFirst(":", ":\n");
               }

            }

            // replace double newlines with single newline
            while (line.contains("\n\n"))
            {
               line = line.replaceAll("\n\n", "\n");
            }

            // Add a new line after all compiler directives
            cdNewLine = false;
            for (int cd = 0; cd < CompilerDir.size(); cd++)
            {
               if (line.contains(CompilerDir.get(cd)))
               {
                  line += "\n";
                  cdNewLine = true;
               }
            }
            if (!cdNewLine)
            {
               for (int cc = 0; cc < CompilerChar.size(); cc++)
               {
                  if (line.contains(CompilerChar.get(cc)))
                  {
                     line += "\n";
                  }
               }
            }
            // Replace with newline for all line terminators
            for (int lt = 0; lt < LineTerminator.size(); lt++)
            {
               line = line.replaceAll(LineTerminator.get(lt), "\n");
            }

            // Replace leading whitespace for new lines
            while (line.contains("\n "))
            {
               line = line.replaceAll("\n ", "\n");
            }

            // write the line
            if (!line.trim().isEmpty())
            {
               // Add the indentation for countint the cyclomatic complexity
               // (Add by 2019 Summer UCC-J Team)
               line = tempLine.substring(0, tempLine.indexOf(tempLine.trim())) + line;
               bw.write(line);
            }

            /* POSTPROCESSING FINISH */

            // Read next line
            line = reader.readLine();
         }
         endTime = TimeUtils.GetTime();
         String executedTime = TimeUtils.CalcElapsedTime(startTime, endTime);
         logger.debug("Finished counting PSLOC for " + cntrResult.FileName + " Exec. Time: " + executedTime);
      }
      catch (IOException e)
      {
         logger.error("The input reader failed to open.");
         logger.error(e);
      }
      finally
      {
         // If the original file was opened...
         if (reader != null)
         {
            // Save PSLOC metrics counted
            cntrResult.IsCounted = true;
            cntrResult.NumTotalLines = reader.getLineNumber();
            cntrResult.LangVersion = LangProps.GetLangVersion();

            // Close the original file
            try
            {
               reader.close();
            }
            catch (IOException e)
            {
               logger.error("The input reader failed to close.");
               logger.error(e);
            }
            reader = null;
         }

         // If the _PSLOC file was opened...
         if (bw != null)
         {
            try
            {
               bw.close();
            }
            catch (IOException e)
            {
               logger.error("The PSLOC writer failed to close.");
               logger.error(e);
            }
            bw = null;
         }

         // Log the number of lines in the file that were truncated
         if (truncateLinesCount > 0)
         {
            logger.warn("Truncated " + truncateLinesCount + " total lines in file " + cntrResult.FileName
                     + "  [-trunc set to " + Integer.toString(RtParams.TruncThreshold) + "]");
         }
      }
   }

   /**
    * Counts Logical SLOC. Current algorithm:
    * 
    * Read first line
    * 
    * Delete the UTF-8 BOM, if found
    * 
    * While we have lines in the file... { - Build true LSLOC and save to LSLOC
    * file - Search for duplicates based on threshold - Delete compiler
    * directives - Delete everything between quotes - Delete everything in loops
    * (i.e. for(...) turns into for()) - Count data keywords - Count executable
    * keywords - Count complexity keywords - Increment complexity loop level
    * based on keyword - Increment cyclomatic complexity loop level based on
    * keyword - Count LSLOC keywords - Count data declarations - Delete exclude
    * keywords and characters
    * 
    * If the line is not empty, increment the LSLOC counter to catch leftover
    * LSLOC
    * 
    * Read next line }
    */
   protected void CountFileLSLOC(UCCFile cntrResult)
   {
      long startTime = 0;
      long endTime = 0;
      startTime = TimeUtils.GetTime();
      logger.debug(TimeUtils.PrintTimestamp() + " Counting LSLOC for " + cntrResult.FileName);

      globals globalVars = new globals();
      tempLoop tempLoopObj = new tempLoop();
      complexity complexityObj = new complexity();
      cyclomaticComplexity cyclomaticComplexityObj = new cyclomaticComplexity();

      LineNumberReader reader = null;
      int lsloc = 0;
      globalVars.lslocKeywordsCount = 0;
      int lineIndex = 0;
      String tempLine = "";

      globalVars.lslocLineValue = 0;
      File file = null;

      // Zero out loop level variables for complexity metrics
      complexityObj.loopLevelCount.clear();

      // Zero out loop level variables for complx metrics
      tempLoopObj.indentationSize.clear();
      tempLoopObj.loopKeyword.clear();

      // Zero out function variables for cyclomatic complexity metrics
      cyclomaticComplexityObj.functionLevel = 0;
      cyclomaticComplexityObj.indentation.clear();
      cyclomaticComplexityObj.keyword.clear();

      // Add base CC level for the cyclomatic complexity counts
      cntrResult.CyclCmplxCnts.add(new CmplxDataType());

      // Set the base CC level's function name to blank since Python can contain
      // scripts with no functions
      cntrResult.CyclCmplxCnts.get(cyclomaticComplexityObj.functionLevel).Keyword = " ";

      // Initialize complexity keywords/counts for this file
      InitAllCmplxKeywords(cntrResult);

      // Buffered writer for _LSLOC file saving
      BufferedWriter bw = null;

      try
      {
         // If we're differencing baselines...
         if (RtParams.DiffCode)
         {
            // Create file for LSLOC storage
            file = new File(FileUtils.BuildTempOutFileName_LSLOC(RtParams, cntrResult));
            bw = new BufferedWriter(new FileWriter(file));
         }

         reader = new LineNumberReader(
                  new InputStreamReader(new FileInputStream(FileUtils.BuildTempOutFileName_PSLOC(RtParams, cntrResult)),
                           Constants.CHARSET_NAME));

         // Read first line
         String line = reader.readLine();

         // Delete UTF-8 BOM instance
         line = DeleteUTF8BOM(line);

         // While we have lines in the file...
         while (line != null)
         {
            // Reset conditional
            globalVars.lslocLineValue = 0;

            logger.trace("line " + reader.getLineNumber() + " in:  " + line);

            // If we're baseline differencing or searching for duplicates...
            if (RtParams.DiffCode || RtParams.SearchForDups)
            {
               // Save line into a temporary line for LSLOC writing
               tempLine = line;

               // Delete all exclude keywords
               tempLine = DeleteExcludeKeywords(tempLine);

               // Delete all exclude characters
               tempLine = DeleteExcludeCharacters(tempLine);
            }

            // Count executable keyword occurences
            CountKeywords(cntrResult.ExecKeywordCnts, line);

            // Count data keyword occurrences
            CountKeywords(cntrResult.DataKeywordCnts, line);

            // Count complexity keywords
            if (RtParams.CountCmplxMetrics && !line.trim().isEmpty())
            {
               CountComplexity(cntrResult, line);
            }

            // Delete all compiler directive lines left over from PSLOC
            line = DeleteCompilerDirectives(cntrResult, line, globalVars);

            if (RtParams.CountCmplxMetrics)
            {
               // Get loop level
               GetLoopLevel(line, tempLoopObj);

               // Get function name
               GetFunctionName(cntrResult, line, cyclomaticComplexityObj);

               // Get cyclomatic complexity
               GetCyclomaticComplexity(cntrResult, line, cyclomaticComplexityObj);
            }

            // Count data declarations
            line = CountDataDeclarations(cntrResult, line, globalVars);

            // Delete exclude keywords
            line = DeleteExcludeKeywords(line);

            // Delete exclude characters
            line = DeleteExcludeCharacters(line);

            // Delete "case...:" statements
            line = DeleteCaseStatements(line);

            // Delete leftover :'s
            line = line.replaceAll(":", "");

            // Increment lsloc_counter
            if (!line.trim().isEmpty())
            {
               logger.trace("line " + reader.getLineNumber() + " out: " + line);
               lsloc++;
               globalVars.lslocLineValue++;
            }

            // Write the Raw LSLOC Line for Differencer
            if (globalVars.lslocLineValue > 0)
            {
               // If we're baseline differencing, write the LSLOC line
               if (RtParams.DiffCode)
               {
                  bw.write(lslocLineValueDelim + Integer.toString(globalVars.lslocLineValue) + lslocLineValueDelim
                           + tempLine + "\n");
               }

               // If we're searching for duplicates, checksum the LSLOC line
               if (RtParams.SearchForDups && !cntrResult.UniqueFileName)
               {
                  cntrResult.FileLineChecksum.add(lineIndex, tempLine.hashCode());
                  lineIndex++;
               }
            }

            // Read next line
            line = reader.readLine();
         }
         endTime = TimeUtils.GetTime();
         String executedTime = TimeUtils.CalcElapsedTime(startTime, endTime);
         logger.debug("Finished counting LSLOC for " + cntrResult.FileName + " Exec. Time: " + executedTime);
      }
      catch (IOException e)
      {
         logger.error("The PSLOC reader failed to open.");
         logger.error(e);
      }
      finally
      {
         // If we're counting complexity...
         if (RtParams.CountCmplxMetrics)
         {
            // Get true indentation size
            GetTrueIndentationSize(tempLoopObj, complexityObj);

            // Scale complexity loop levels
            ScaleComplexityLoopLevels(cntrResult, complexityObj);

            // Get true cyclomatic complexity
            GetTrueCyclomaticComplexity(cntrResult, cyclomaticComplexityObj);

            // Calculate cyclomatic complexity
            CalculateCyclomaticComplexity(cntrResult);
         }

         // If the _PSLOC file was opened...
         if (reader != null)
         {
            // Save LSLOC metrics counted
            cntrResult.NumLSLOC = cntrResult.NumCompilerDirectives + cntrResult.NumDataDeclLog
                     + globalVars.lslocKeywordsCount + lsloc;
            cntrResult.NumExecInstrLog =
                     cntrResult.NumLSLOC - cntrResult.NumDataDeclLog - cntrResult.NumCompilerDirectives;
            cntrResult.NumDataDeclPhys = cntrResult.NumDataDeclLog;
            cntrResult.NumExecInstrPhys = cntrResult.NumExecInstrLog;
            cntrResult.LangVersion = LangProps.GetLangVersion();

            // Close the _PSLOC file
            try
            {
               reader.close();
            }
            catch (IOException e)
            {
               logger.error("The PSLOC reader failed to close.");
               logger.error(e);
            }
            reader = null;

            // Delete PSLOC file
            FileUtils.DeleteFile(FileUtils.BuildTempOutFileName_PSLOC(RtParams, cntrResult));
         }

         // If the _LSLOC file was opened...
         if (bw != null)
         {
            // Close the _LSLOC file
            try
            {
               bw.close();
            }
            catch (IOException e)
            {
               logger.error("The LSLOC writer failed to close.");
               logger.error(e);
            }
            bw = null;
         }

         // If we're counting complexity...
         if (RtParams.CountCmplxMetrics)
         {
            // Decrement needed loop levels to get correct result
            GetTrueLoopLevels(tempLoopObj, complexityObj);

            // Scale complexity loop keyword counts to proper loop levels
            ScaleComplexityLoopLevels(cntrResult);

            // Calculate cyclomatic complexity
            CalculateCyclomaticComplexity(cntrResult);

            // Calculate halstead metrics
            CalculateHalsteadMetrics(cntrResult);

            // Calculate Maintainability Index(MI)
            CalculateMaintainabilityIndex(cntrResult);

         }
      }
   }

   /**
    * Function for getting the loop level for a particular line based on
    * occurrences of the loop keywords list.
    * 
    * @param line
    *           String input
    * @param tempLoopObj
    *           Class that holds all temp loop variables
    *
    */
   protected void GetLoopLevel(String line, tempLoop tempLoopObj)
   {
      String regEx;
      Matcher matcher;
      int ind = line.length() - line.replaceAll("^\\s+", "").length();

      // Add special case for 'class'
      regEx = "\\bclass\\b";
      matcher = Pattern.compile(regEx).matcher(line);

      // If we find any of the condition keywords on the given line...
      if (matcher.find())
      {
         regEx = ":";
         matcher = Pattern.compile(regEx).matcher(line);

         // If the line ends with a :...
         if (matcher.find() && matcher.end() == line.replaceFirst("\\s+$", "").length())
         {
            ind = line.length() - line.replaceAll("^\\s+", "").length();

            // Add the loop level and loop keyword to our arraylists
            tempLoopObj.indentationSize.add(ind);
            tempLoopObj.loopKeyword.add("class");
         }
      }

      // Add special case for 'def'
      regEx = "\\bdef\\b";
      matcher = Pattern.compile(regEx).matcher(line);

      // If we find any of the condition keywords on the given line...
      if (matcher.find())
      {
         regEx = ":";
         matcher = Pattern.compile(regEx).matcher(line);

         // If the line ends with a :...
         if (matcher.find() && matcher.end() == line.replaceFirst("\\s+$", "").length())
         {
            ind = line.length() - line.replaceAll("^\\s+", "").length();

            // Add the loop level and loop keyword to our arraylists
            tempLoopObj.indentationSize.add(ind);
            tempLoopObj.loopKeyword.add("def");
         }
      }

      // Loop through all the condition keywords
      for (int ck = 0; ck < CondKeywords.size(); ck++)
      {
         regEx = "\\b" + CondKeywords.get(ck) + "\\b";
         matcher = Pattern.compile(regEx).matcher(line);

         // If we find any of the condition keywords on the given line...
         if (matcher.find())
         {
            regEx = ":";
            matcher = Pattern.compile(regEx).matcher(line);

            // If the line ends with a :...
            if (matcher.find() && matcher.end() == line.replaceFirst("\\s+$", "").length())
            {
               ind = line.length() - line.replaceAll("^\\s+", "").length();

               // Add the loop level and loop keyword to our arraylists
               tempLoopObj.indentationSize.add(ind);
               tempLoopObj.loopKeyword.add(CondKeywords.get(ck));
            }
         }
      }
   }

   /**
    * Function for ruling out lines which are not functions and finally
    * extracting the function name if we determine that we are in a function.
    * 
    * @param cntrResult
    *           A UCCFile object to store results of code counter
    * @param line
    *           Line coming in from file
    * @param cyclomaticComplexityObj
    *           Class to hold cyclomatic complexity variables
    */
   protected void GetFunctionName(UCCFile cntrResult, String line, cyclomaticComplexity cyclomaticComplexityObj)
   {
      String regEx;
      Matcher matcher;

      line = line.trim();

      // Loop through all the function keywords...
      for (int fk = 0; fk < FunctionKeywords.size() && !line.isEmpty(); fk++)
      {
         regEx = ".*\\b" + FunctionKeywords.get(fk) + "\\b.*";
         matcher = Pattern.compile(regEx).matcher(line);
         if (matcher.find() && matcher.start() == 0)
         {
            // Increment the number of functions
            cyclomaticComplexityObj.functionLevel++;

            // Add a new element to the cyclomatic complexity count arrayList
            cntrResult.CyclCmplxCnts.add(new CmplxDataType());

            // Take substring to derive function name
            line = line.substring(line.indexOf(FunctionKeywords.get(fk)) + FunctionKeywords.get(fk).length(),
                     line.length()).trim();

            // Handles cases with arguments on function name line
            if (line.indexOf("(") >= 0)
            {
               line = line.substring(0, line.indexOf("(")).trim();
            }

            // Save function name
            cntrResult.CyclCmplxCnts.get(cyclomaticComplexityObj.functionLevel).Keyword = line;
         }
      }
   }

   /**
    * Function for counting cyclomatic complexity based on cyclomatic complexity
    * keywords
    * 
    * @param cntrResult
    *           A UCCFile object to store results of code counter
    * @param line
    *           Line coming in from file
    * @param cyclomaticComplexityObj
    *           Class to hold cyclomatic complexity variables
    */
   protected void GetCyclomaticComplexity(UCCFile cntrResult, String line, cyclomaticComplexity cyclomaticComplexityObj)
   {
      String regEx;
      Matcher matcher;
      int ind = line.length() - line.replaceAll("^\\s+", "").length();

      // Add special case for 'def'
      regEx = "\\bdef\\b";
      matcher = Pattern.compile(regEx).matcher(line);

      // If we find any of the condition keywords on the given line...
      if (matcher.find())
      {
         regEx = ":";
         matcher = Pattern.compile(regEx).matcher(line);

         // If the line ends with a :...
         if (matcher.find() && matcher.end() == line.replaceFirst("\\s+$", "").length())
         {
            ind = line.length() - line.replaceAll("^\\s+", "").length();

            // Add the loop level and loop keyword to our arraylists
            cyclomaticComplexityObj.indentation.add(ind);
            cyclomaticComplexityObj.keyword.add(line);
         }
      }

      // Loop through all the condition keywords
      for (int cck = 0; cck < CyclCmplexKeywords.size() && !line.trim().isEmpty(); cck++)
      {
         regEx = "\\b" + CyclCmplexKeywords.get(cck) + "\\b";
         matcher = Pattern.compile(regEx).matcher(line);

         // If we find any of the condition keywords on the given line...
         if (matcher.find())
         {
            regEx = ":";
            matcher = Pattern.compile(regEx).matcher(line);

            // If the line ends with a :...
            if (matcher.find() && matcher.end() == line.replaceFirst("\\s+$", "").length())
            {
               ind = line.length() - line.replaceAll("^\\s+", "").length();

               // Add the loop level and loop keyword to our arraylists
               cyclomaticComplexityObj.indentation.add(ind);
               cyclomaticComplexityObj.keyword.add(CyclCmplexKeywords.get(cck));
            }
         }
      }
   }

   /**
    * Function for adding all the loop level complexity counts up in a 0 to N-1
    * format and saving to the UCCFile's complexity loop level count metrics.
    * 
    * @param cntrResult
    *           A UCCFile object to store results of code counter
    * @param complexityObj
    *           Class to hold complexity variables
    */
   protected void ScaleComplexityLoopLevels(UCCFile cntrResult, complexity complexityObj)
   {
      int finalLoopCounter = 0;

      // Loop through all the loop level counts
      for (int i = 0; i < complexityObj.loopLevelCount.size(); i++)
      {
         // If the count is non-zero
         if (complexityObj.loopLevelCount.get(i) > 0)
         {
            // Add the level to the loop level arraylist
            cntrResult.CmplxLoopLvlCnts.add(new CmplxDataType());

            // Set the count to the tallied value
            cntrResult.CmplxLoopLvlCnts.get(finalLoopCounter).Count = complexityObj.loopLevelCount.get(i);

            // Increment the loop counter
            finalLoopCounter++;
         }
      }
   }

   /**
    * Function for calculating the total cyclomatic complexity and average
    * cyclomatic complexity of the file.
    * 
    * @param cntrResult
    *           A UCCFile object to store results of code counter
    */
   protected void CalculateCyclomaticComplexity(UCCFile cntrResult)
   {
      // (Modified by 2019 Summer UCC-J Team)
      // Remove this if statement to save the CC1 results by function of the
      // lines that's not in any function and contains no cc keyword
      // If the first level count is 0, but there are other levels, remove it
      // if (cntrResult.CyclCmplxCnts.size() > 1 &&
      // cntrResult.CyclCmplxCnts.get(0).Count == 0)
      // {
      // cntrResult.CyclCmplxCnts.remove(0);
      // }

      // Calculate cyclomatic complexity
      for (int i = 0; i < cntrResult.CyclCmplxCnts.size(); i++)
      {
         // Add 1 for the file/function itself
         cntrResult.CyclCmplxCnts.get(i).Count++;

         // Sum up total cyclomatic complexity
         cntrResult.CyclCmplxTotal += cntrResult.CyclCmplxCnts.get(i).Count;
      }

      // Get average of cyclomatic complexity
      cntrResult.CyclCmplxAvg = (double) cntrResult.CyclCmplxTotal / (double) cntrResult.CyclCmplxCnts.size();
   }

   /**
    * Function for looping through all of the conditional + { occurrences we
    * found in the file, decrementing where needed, and adding the corrected
    * loop levels and counts to the complexity object's loop level counter.
    *
    * @param tempLoopObj
    *           Class to hold temp loop variables
    * @param complexityObj
    *           Class to hold complexity variables
    */
   protected void GetTrueIndentationSize(tempLoop tempLoopObj, complexity complexityObj)
   {
      boolean isLoopKeyword;
      int index;
      int newIndentSize;
      int loopCounter = 0;
      int indentSizeDiff = 0;
      int size1 = 0;
      int size2 = 0;

      ArrayList<String> loopKeyword2 = new ArrayList<String>();
      ArrayList<Integer> indentationSize2 = new ArrayList<Integer>();
      ArrayList<Integer> uniqueIndSizes = new ArrayList<Integer>();

      // Loop through all the conditional keyword instances we found...
      for (int i = 0; i < tempLoopObj.loopKeyword.size(); i++)
      {
         // Assume the keyword we found was not a loop keyword
         isLoopKeyword = false;

         // Loop through all the loop keywords...
         for (int lk = 0; lk < LoopKeywords.size(); lk++)
         {
            // If the conditional keyword we found is a loop keyword...
            if (tempLoopObj.loopKeyword.get(i).equals(LoopKeywords.get(lk)))
            {
               // Turn this boolean to true
               isLoopKeyword = true;
            }
         }

         // If the conditional keyword we found was not a loop keyword...
         if (isLoopKeyword == false)
         {
            // Save the index in the list
            index = i;

            if (i < tempLoopObj.loopKeyword.size() - 1)
            {
               // Move to the next entry in the list
               i++;

               // Save what we may need to scale back by (difference in
               // indentation between two keywords)
               indentSizeDiff = tempLoopObj.indentationSize.get(i) - tempLoopObj.indentationSize.get(index);

               // Scale all the indentation sizes back until we hit the
               // indentation size of the conditional keyword
               // we're dealing with.
               // This will "un-nest" iterative loops from conditional loops
               while (tempLoopObj.indentationSize.get(i) > tempLoopObj.indentationSize.get(index)
                        && i < tempLoopObj.indentationSize.size() - 1)
               {
                  newIndentSize = tempLoopObj.indentationSize.get(i) - indentSizeDiff;
                  tempLoopObj.indentationSize.set(i, newIndentSize);
                  i++;
               }

               // Handle last entry if it meets the criteria
               if (tempLoopObj.indentationSize.get(i) > tempLoopObj.indentationSize.get(index))
               {
                  newIndentSize = tempLoopObj.indentationSize.get(i) - indentSizeDiff;
                  tempLoopObj.indentationSize.set(i, newIndentSize);
               }
            }

            // Go back to the conditional keyword's index and move on
            i = index;
         }
      }

      // Loop through all the conditional keyword instances we found... AGAIN
      for (int i = 0; i < tempLoopObj.loopKeyword.size(); i++)
      {
         // Assume the keyword we found was not a loop keyword AGAIN
         isLoopKeyword = false;

         // Loop through all the loop keywords... AGAIN
         for (int lk = 0; lk < LoopKeywords.size(); lk++)
         {
            // If the conditional keyword we found is a loop keyword... AGAIN
            if (tempLoopObj.loopKeyword.get(i).equals(LoopKeywords.get(lk)))
            {
               // Add to list of ONLY loop keywords
               loopKeyword2.add(tempLoopObj.loopKeyword.get(i));

               // Add to list of indentation sizes of ONLY loop keywords
               // System.out.println(tempLoopObj.loopKeyword.get(i) +
               // tempLoopObj.indentationSize.get(i));
               indentationSize2.add(tempLoopObj.indentationSize.get(i));
            }
         }
      }

      // Remove duplicates from indentationSize2 and save in a new unique list
      int value;
      for (int i = 0; i < indentationSize2.size(); i++)
      {
         value = indentationSize2.get(i);
         uniqueIndSizes.add(value);
      }
      uniqueIndSizes = RemoveDuplicates(uniqueIndSizes);

      // Sort elements of the unique list from least to greatest
      Collections.sort(uniqueIndSizes);

      // Allocate the loop level count to the size of the unique indentation
      // size list
      for (int i = 0; i < uniqueIndSizes.size(); i++)
      {
         // Add another level to the level counter arrayList
         complexityObj.loopLevelCount.add(0);
      }

      // Loop through all of the patterns we found for loop levels
      for (int i = 0; i < indentationSize2.size(); i++)
      {
         size1 = indentationSize2.get(i);
         // Tally up the number of loops found at each level
         for (int j = 0; j < uniqueIndSizes.size(); j++)
         {
            size2 = uniqueIndSizes.get(j);
            if (size1 == size2)
            {
               // Set the loop counter to the integer value of the current loop
               // level counter array list. This gets the
               // current loop count per the level we're on
               loopCounter = complexityObj.loopLevelCount.get(j).intValue();

               // Increment the loop counter
               loopCounter++;

               // Set the loop level counter arrayList level <loopLevel> to the
               // count <loopCounter>
               complexityObj.loopLevelCount.set(j, loopCounter);
            }
         }
      }
   }

   /**
    * Function for removing duplicates from an ArrayList.
    * 
    * @param uniqueIndSizes
    *           Input ArrayList containing indentation sizes
    * @return Input ArrayList with any duplicate entires removed
    */
   public static ArrayList<Integer> RemoveDuplicates(ArrayList<Integer> uniqueIndSizes)
   {
      Set<Integer> lhs = new LinkedHashSet<>(uniqueIndSizes);
      lhs.addAll(uniqueIndSizes);
      uniqueIndSizes.clear();
      uniqueIndSizes.addAll(lhs);

      return uniqueIndSizes;
   }

   /**
    * Function for getting the true cyclomatic complexity per function/file
    * based on keywords collected earlier.
    * 
    * @param cntrResult
    *           A UCCFile object to store results of code counter
    * @param cyclomaticComplexityObj
    *           Class to hold cyclomatic complexity variables
    */
   protected void GetTrueCyclomaticComplexity(UCCFile cntrResult, cyclomaticComplexity cyclomaticComplexityObj)
   {
      int kndx = 0;
      int tempCount = 0;
      // Using stack to save the outer function (Add by 2019 Summer UCC-J Team)
      Stack<Integer> kndxStack = new Stack<>();
      Stack<Integer> tempCountStack = new Stack<>();
      String defName = "";

      for (int k = 0; k < cyclomaticComplexityObj.keyword.size(); k++)
      {
         // If there is a function or return from inner function (Add by 2019
         // Summer UCC-J Team)
         if ((cyclomaticComplexityObj.keyword.get(k).trim().startsWith("def")
                  && cyclomaticComplexityObj.indentation.get(k) != -999) || tempCountStack.size() < kndxStack.size())
         {
            // Save the def index
            kndx = k;

            // Resume the kndx if there is any (Add by 2019 Summer UCC-J Team)
            if (tempCountStack.size() < kndxStack.size())
            {
               kndx = kndxStack.pop();
            }

            if (k < cyclomaticComplexityObj.keyword.size() - 1)
            {
               // Go to the next item in the list
               k++;

               // Add 1 for the def itself
               // tempCount++;

               // While the indentation is greater than that of the def
               while (cyclomaticComplexityObj.indentation.get(kndx) < cyclomaticComplexityObj.indentation.get(k)
                        && k < cyclomaticComplexityObj.indentation.size() - 1)
               {
                  // If there is function in function, end the loop (Add by 2019
                  // Summer UCC-J Team)
                  if (cyclomaticComplexityObj.keyword.get(k).trim().startsWith("def"))
                  {
                     break;
                  }

                  // Increment the count for the def
                  tempCount++;

                  // Set the indentation to -999 as we go so we know which ones
                  // we've counted
                  cyclomaticComplexityObj.indentation.set(k, -999);

                  // Go to the next item in the list
                  k++;
               }

               // Handle last entry if it meets the criteria
               if (cyclomaticComplexityObj.indentation.get(kndx) < cyclomaticComplexityObj.indentation.get(k))
               {
                  // If there is function in function, save current kndx and
                  // tempCount in stack (Add by 2019 Summer UCC-J Team)
                  if (cyclomaticComplexityObj.keyword.get(k).trim().startsWith("def"))
                  {
                     kndxStack.push(kndx);
                     tempCountStack.push(tempCount);
                  }
                  else
                  {
                     // Increment the count for the def
                     tempCount++;

                     // Set the indentation to -999 as we go so we know which
                     // ones we've counted
                     cyclomaticComplexityObj.indentation.set(k, -999);
                  }
               }
            }
            else if (cyclomaticComplexityObj.keyword.size() == 1)
            {
               // Increment the count for the def
               // Modified by 2019 Summer UCC-J Team:
               // No need to increase 1 cause for each function we increase
               // count by 1 in CalculateCyclomaticComplexity()
               // tempCount++;

               // Set the indentation to -999 as we go so we know which ones
               // we've counted
               cyclomaticComplexityObj.indentation.set(kndx, -999);
            }

            // Get the defName the same way we got the function name
            for (int fk = 0; fk < FunctionKeywords.size(); fk++)
            {
               defName = cyclomaticComplexityObj.keyword.get(kndx);
               defName = defName.substring(
                        defName.indexOf(FunctionKeywords.get(fk)) + FunctionKeywords.get(fk).length(),
                        defName.length());

               if (defName.indexOf("(") >= 0)
               {
                  defName = defName.substring(0, defName.indexOf("(")).trim();
               }
            }

            // Match up the cyclomatic complexity function name with the current
            // list defName
            for (int cc = 0; cc < cntrResult.CyclCmplxCnts.size(); cc++)
            {
               // if the names match, set the count to tempCount
               if (cntrResult.CyclCmplxCnts.get(cc).Keyword.equals(defName))
               {
                  cntrResult.CyclCmplxCnts.get(cc).Count = tempCount;
                  break;
               }
            }

            // Reset our count
            tempCount = 0;

            // Reset our index
            k = kndx;

            // Set the indentation to -999 as we go so we know which ones we've
            // counted
            cyclomaticComplexityObj.indentation.set(k, -999);

            // If there is an outer funtion, resume the tempCount (Add by 2019
            // Summer UCC-J Team)
            if (!kndxStack.isEmpty() && kndxStack.peek() != kndx)
            {
               tempCount = tempCountStack.pop();
               k = kndxStack.peek();
            }
         }
      }

      // Reset our count
      tempCount = 0;

      // Count all of our "level 0" instances (i.e. CC outside a function)
      for (int k = 0; k < cyclomaticComplexityObj.keyword.size(); k++)
      {
         if (cyclomaticComplexityObj.indentation.get(k) != -999)
         {
            tempCount++;
         }
      }

      // If we have any file level CC, save the tempCount
      if (cntrResult.CyclCmplxCnts.get(0).Keyword.trim().isEmpty())
      {
         if (cntrResult.CyclCmplxCnts.size() > 1)
         {
            if (tempCount >= 1)
            {
               cntrResult.CyclCmplxCnts.get(0).Count = tempCount;
            }
            else // Otherwise delete the file level item
            {
               // Modified by 2019 Summer UCC-J Team
               // Remove this line to save the CC1 results by function of the
               // lines that's not in any function and contains no cc keyword
               // cntrResult.CyclCmplxCnts.remove(0);
            }
         }
         else if (cntrResult.CyclCmplxCnts.size() == 1)
         {
            cntrResult.CyclCmplxCnts.get(0).Count = tempCount;
         }
      }
   }

   /**
    * Function for calculating the Halstead volume of the file.
    *
    * @param cntrResult
    *           A UCCFile object to store results of code counter
    */

   private void CalculateHalsteadMetrics(UCCFile cntrResults)
   {

      int uniqueOperatorCnt = 0;
      int totalOperatorCnt = 0;
      int uniqueOperandCnt = 0;
      int totalOperandCnt = 0;

      int N = 0;
      int n = 0;

      UniqueOperatorList = new ArrayList<>();
      UniqueOperatorList.add(cntrResults.CmplxTrigCnts);
      UniqueOperatorList.add(cntrResults.CmplxLogCnts);
      UniqueOperatorList.add(cntrResults.CmplxCalcCnts);
      UniqueOperatorList.add(cntrResults.CmplxMathCnts);
      UniqueOperatorList.add(cntrResults.CmplxCondCnts);
      UniqueOperatorList.add(cntrResults.CmplxLogicCnts);
      UniqueOperatorList.add(cntrResults.CmplxPreprocCnts);
      UniqueOperatorList.add(cntrResults.CmplxAssignCnts);
      UniqueOperatorList.add(cntrResults.CmplxPntrCnts);
      UniqueOperatorList.add(cntrResults.DataKeywordCnts);
      UniqueOperatorList.add(cntrResults.ExecKeywordCnts);

      UniqueOperandList = new ArrayList<>();
      UniqueOperandList.add(cntrResults.charMap);
      UniqueOperandList.add(cntrResults.digitMap);
      UniqueOperandList.add(cntrResults.wordMap);

      int[] operatorCnt = GetUniqueAndTotalOperatorCount(UniqueOperatorList);
      uniqueOperatorCnt = operatorCnt[0];
      totalOperatorCnt = operatorCnt[1];

      logger.debug("uniqueOperatorCnt=" + uniqueOperatorCnt);
      logger.debug("totalOperatorCnt=" + totalOperatorCnt);
      // System.out.println("uniqueOperatorCnt=" + uniqueOperatorCnt);
      // System.out.println("totalOperatorCnt=" + totalOperatorCnt);

      int[] operandCnt = GetUniqueAndTotalOperandCount(UniqueOperandList, cntrResults.BoolOperandCnts);
      uniqueOperandCnt = operandCnt[0];
      totalOperandCnt = operandCnt[1];

      logger.debug("uniqueOperandCnt=" + uniqueOperatorCnt);
      logger.debug("totalOperandCnt=" + totalOperatorCnt);
      // System.out.println("uniqueOperandCnt=" + uniqueOperandCnt);
      // System.out.println("totalOperandCnt=" + totalOperandCnt);

      // Program Length(N) is the sum of the total number of operators and
      // operands in the program
      N = totalOperatorCnt + totalOperandCnt;
      // System.out.println("N=");
      // System.out.println(N);
      // vocabulary size (n) is the sum of the number of unique operators and
      // operands
      n = uniqueOperatorCnt + uniqueOperandCnt;
      // System.out.println("n=");
      // System.out.println(n);
      // Halstead Volume or Program volume (V) = N * log2(n)
      if (n > 0)
      {
         cntrResults.V = N * Math.log(n) / Math.log(2);
      }

      logger.debug("Halstead Volume=" + cntrResults.V);
      // System.out.println("V=");
      // System.out.println(cntrResults.V);

   }

   /**
    * Function for calculating the 3-metric and 4-metric Maintainability Index
    * of the file.
    *
    * @param cntrResult
    *           A UCCFile object to store results of code counter
    */
   private void CalculateMaintainabilityIndex(UCCFile cntrResults)
   {

      cntrResults.maintainabilityIndexNC = 171 - 5.2 * Math.log(cntrResults.V) - 0.23 * cntrResults.CyclCmplxTotal
               - 16.2 * Math.log(cntrResults.NumLSLOC);

      logger.debug("maintainabilityIndex_3_Metric=" + cntrResults.maintainabilityIndexNC);

      cntrResults.maintainabilityIndex = cntrResults.maintainabilityIndexNC
               + 50 * Math.sin((Math.sqrt(2.4 * (cntrResults.NumWholeComments + cntrResults.NumEmbeddedComments))));

      logger.debug("maintainabilityIndex_4_Metric=" + cntrResults.maintainabilityIndex);
   }

   /**
    * Function for looping through all of the conditional + { occurrences we
    * found in the file, decrementing where needed, and adding the corrected
    * loop levels and counts to the complexity object's loop level counter.
    *
    * @param tempLoopObj
    *           Class to hold temp loop variables
    * @param complexityObj
    *           Class to hold complexity variables
    */
   protected void GetTrueLoopLevels(tempLoop tempLoopObj, complexity complexityObj)
   {
      boolean isLoopKeyword;
      int index;
      int newLevel;
      int maxLoopLevel = 0;
      int loopCounter = 0;

      // Loop through all the conditional keyword instances we found...
      for (int i = 0; i < tempLoopObj.loopLevel.size(); i++)
      {
         // Assume the keyword we found was not a loop keyword
         isLoopKeyword = false;

         // Loop through all the loop keywords...
         for (int lk = 0; lk < LoopKeywords.size(); lk++)
         {
            // If the conditional keyword we found is a loop keyword...
            if (tempLoopObj.loopKeyword.get(i).equals(LoopKeywords.get(lk)))
            {
               // Turn this boolean to true
               isLoopKeyword = true;
            }
         }

         // If the conditional keyword we found was not a loop keyword...
         if (isLoopKeyword == false)
         {
            // Save the index in the list
            index = i;

            if (i < tempLoopObj.loopLevel.size() - 1)
            {
               // Move to the next entry in the list
               i++;

               // Scale all the loop levels back 1 until we hit the level of the
               // conditional keyword we're dealing with.
               // This will "un-nest" iterative loops from conditional loops
               if (!tempLoopObj.loopKeyword.get(index).equals("do"))
               {
                  while (tempLoopObj.loopLevel.get(i) > tempLoopObj.loopLevel.get(index)
                           && i < tempLoopObj.loopLevel.size() - 1)
                  {
                     newLevel = tempLoopObj.loopLevel.get(i) - 1;
                     tempLoopObj.loopLevel.set(i, newLevel);
                     i++;
                  }

                  // Handle last entry if it meets the criteria
                  if (tempLoopObj.loopLevel.get(i) > tempLoopObj.loopLevel.get(index))
                  {
                     newLevel = tempLoopObj.loopLevel.get(i) - 1;
                     tempLoopObj.loopLevel.set(i, newLevel);
                  }
               }
            }

            // Go back to the conditional keyword's index and move on
            i = index;
         }
      }

      // Loop through all of the patterns we found for loop levels
      for (int i = 0; i < tempLoopObj.loopLevel.size(); i++)
      {
         // Get the max loop level of loop keywords
         for (int lk = 0; lk < LoopKeywords.size(); lk++)
         {
            // If the conditional keyword we found is a loop keyword...
            if (tempLoopObj.loopKeyword.get(i).equals(LoopKeywords.get(lk)))
            {
               // Get the maximum loop level we found for allocation of the loop
               // level count
               maxLoopLevel = Math.max(maxLoopLevel, tempLoopObj.loopLevel.get(i));
            }
         }
      }

      // Allocate the loop level count to the right size, discovered above
      for (int i = 0; i < maxLoopLevel; i++)
      {
         // Add another level to the level counter arrayList
         complexityObj.loopLevelCount.add(0);
      }

      // Loop through all of the patterns we found for loop levels
      for (int i = 0; i < tempLoopObj.loopLevel.size(); i++)
      {
         // Tally up the number of loops found at each level
         for (int lk = 0; lk < LoopKeywords.size(); lk++)
         {
            // If the conditional keyword we found is a loop keyword...
            if (tempLoopObj.loopKeyword.get(i).equals(LoopKeywords.get(lk)))
            {
               // Set the loop counter to the integer value of the current loop
               // level counter array list. This gets the
               // current loop count per the level we're on
               loopCounter = complexityObj.loopLevelCount.get(tempLoopObj.loopLevel.get(i)).intValue();

               // Increment the loop counter
               loopCounter++;

               // Set the loop level counter arrayList level <loopLevel> to the
               // count <loopCounter>
               complexityObj.loopLevelCount.set(tempLoopObj.loopLevel.get(i), loopCounter);
            }
         }
      }
   }

   /**
    * Function for adding all the loop level complexity counts up in a 0 to N-1
    * format and saving to the UCCFile's complexity loop level count metrics.
    *
    * @param cntrResult
    *           A UCCFile object to store results of code counter
    */
   protected void ScaleComplexityLoopLevels(UCCFile cntrResult)
   {
      int finalLoopCounter = 0;

      // Loop through all the loop level counts
      for (int i = 0; i < ComplexityObj.loopLevelCount.size(); i++)
      {
         // If the count is non-zero
         if (ComplexityObj.loopLevelCount.get(i) > 0)
         {
            // Add the level to the loop level arraylist
            cntrResult.CmplxLoopLvlCnts.add(new CmplxDataType());

            // Set the count to the tallied value
            cntrResult.CmplxLoopLvlCnts.get(finalLoopCounter).Count = ComplexityObj.loopLevelCount.get(i);

            // Increment the loop counter
            finalLoopCounter++;
         }
      }
   }

   /**
    * Computes total and unique number of operator counts
    *
    * @param cmplxData
    *           Complexity data object containing counts to sum
    * @return Integer array containing total number of operator counts and
    *         unique number of operator counts
    */

   private int[] GetUniqueAndTotalOperatorCount(ArrayList<ArrayList<CmplxDataType>> cmplxData)
   {
      int uniqueOperators = 0;
      int totalOperators = 0;

      if (cmplxData != null)
      {
         for (int i = 0; i < cmplxData.size(); i++)
         {
            for (int j = 0; j < cmplxData.get(i).size(); j++)
            {
               totalOperators += cmplxData.get(i).get(j).Count;
               if (cmplxData.get(i).get(j).Count > 0)
               {
                  uniqueOperators++;
               }

            }
         }
      }

      return new int[] { uniqueOperators, totalOperators };
   }

   private int[] GetUniqueAndTotalOperandCount(ArrayList<TreeMap> map, ArrayList<CmplxDataType> cmplxData)
   {
      int uniqueOperands = 0;
      int totalOperands = 0;

      if (cmplxData != null)
      {
         for (int i = 0; i < cmplxData.size(); i++)
         {
            totalOperands += cmplxData.get(i).Count;
            if (cmplxData.get(i).Count > 0)
            {
               uniqueOperands++;
            }
         }
      }

      for (int i = 0; i < map.size(); i++)
      {
         TreeMap<String, Integer> iterMap = new TreeMap<>(map.get(i));
         uniqueOperands += iterMap.size();
         for (Entry<String, Integer> entry : iterMap.entrySet())
         {

            totalOperands += entry.getValue();
         }

      }

      return new int[] { uniqueOperands, totalOperands };
   }
}
