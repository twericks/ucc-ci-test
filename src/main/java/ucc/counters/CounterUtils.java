package ucc.counters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import ucc.datatypes.UCCFile;

public class CounterUtils
{
   // Pre-processing Variables
   CodeCounter counter = new CodeCounter();
   String line;
   int truncateLinesCount = 0;
   List<PositionItem> positionItemList = new ArrayList<PositionItem>();
   List<DeleteItem> deleteItemList = new ArrayList<DeleteItem>();
   List<Boolean> indexInUse;
   Boolean inMultiLineComment;
   Boolean inStringLiteral;
   String closingStringLiteral;

   boolean turnedStringLiteralOff = false;
   boolean truncated = false;
   String inMultiLineCommentCloseChar = "";
   String inStringLiteralCloseChar = "";

   final String TYPE_STRING_LITERAL = "SL";
   final String TYPE_SINGLE_LINE_COMMENT = "SLC";
   final String TYPE_MULTI_LINE_COMMENT_START = "MLCS";
   final String TYPE_MULTI_LINE_COMMENT_END = "MLCE";
   final String TYPE_COMPILER_DIRECTIVE = "CD";

   // Sets the counter to be whatever counter type is being used
   // on the file (i.e.-Java)
   public CounterUtils(CodeCounter inCounter)
   {
      counter = inCounter;
   }

   /**
    * Function for processing a line of code to extract the required information
    * and increment the appropriate metric counters.
    * 
    * @param incomingLine
    *           Line coming in from file
    * @param cntrResult
    *           The result object which holds all of the counters
    * @param incomingInMultiLineComment
    *           States whether or not this line is part of a multi-line comment
    *           which started on a previous line
    * @param incomingInStringLiteral
    *           States whether or not this line is part of a multi-line string
    *           literal which started on a previous line
    * @param inComingClosingStringLiteral
    *           The object that holds the corresponding closing character for a
    *           multi-line string literal
    *
    * @return The ResultObject which contains the modified line and the
    *         inMultiLineComment variable
    */
   public ResultObject PreformPreProcessing(String incomingLine, UCCFile cntrResult, boolean incomingInMultiLineComment,
            boolean incomingInStringLiteral, String inComingClosingStringLiteral)
   {
      line = incomingLine;
      inMultiLineComment = incomingInMultiLineComment;
      inStringLiteral = incomingInStringLiteral;
      closingStringLiteral = inComingClosingStringLiteral;

      // Ignore line data beyond the single line threshold value
      if (counter.RtParams.TruncThreshold != 0 && line.length() > counter.RtParams.TruncThreshold)
      {
         line = line.substring(0, counter.RtParams.TruncThreshold);
         truncated = true;
         truncateLinesCount++;
      }

      line = line.trim();

      if (line.isEmpty())
      {
         cntrResult.NumBlankLines++;
      }
      else
      {
         // If the language is not case sensitive, lower-case line coming in
         if (!counter.LangProps.IsCaseSensitive())
         {
            line = line.toLowerCase();
         }
         indexInUse = Arrays.asList(new Boolean[line.length()]);
         Collections.fill(indexInUse, Boolean.FALSE);
         indexInUse = counter.FindEscapedChars(line, indexInUse);
         FindCompilerDirectives();

         // Identify and save all index positions of Multi Line Comment Start
         // Characters
         if (!counter.MultiLineCommentStart.isEmpty() && !inMultiLineComment)
         {
            GetSpecialChars(counter.MultiLineCommentStart, TYPE_MULTI_LINE_COMMENT_START);
         }

         // Identify and save all index positions of Multi Line Comment End
         // Characters
         if (!counter.MultiLineCommentEnd.isEmpty())
         {
            GetSpecialChars(counter.MultiLineCommentEnd, TYPE_MULTI_LINE_COMMENT_END);
         }

         // Identify and save all index positions of Single Line Comment
         // Characters
         if (!counter.SingleLineComment.isEmpty())
         {
            GetSpecialChars(counter.SingleLineComment, TYPE_SINGLE_LINE_COMMENT);
         }

         // Identify and save all index positions of String Literals
         if (!counter.QuoteChar.isEmpty())
         {
            GetSpecialChars(counter.QuoteChar, TYPE_STRING_LITERAL);
         }

         // Starting left to right, process each identified position.
         if (!positionItemList.isEmpty() || inMultiLineComment || inStringLiteral)
         {
            CleanUpLine(cntrResult);
         }
         else if (positionItemList.isEmpty() && !line.isEmpty())
         {
            cntrResult.NumPSLOC++;
         }
      }

      ResultObject ro = new ResultObject();
      ro.line = line;
      ro.inMultiLineComment = inMultiLineComment;
      ro.inStringLiteral = inStringLiteral;
      ro.truncateLinesCount = truncateLinesCount;
      ro.closingStringLiteral = closingStringLiteral;

      return ro;
   }

   /**
    * Function for processing a line of code to extract the required information
    * and increment the appropriate metric counters.
    * 
    * @param SpecialChars
    *           Array list containing characters that need to be found in the
    *           line
    * 
    * @param type
    *           The type for the special characters coming in
    */
   private void GetSpecialChars(ArrayList<String> SpecialChars, String type)
   {
      for (int x = 0; x < SpecialChars.size() && !line.isEmpty(); x++)
      {
         String specialCharacter = SpecialChars.get(x);
         int index = line.indexOf(specialCharacter);
         int endIndex = index + specialCharacter.length() - 1;
         boolean tagInUse = false;
         while (index >= 0)
         {
            tagInUse = false;
            for (int k = index; k <= endIndex; k++)
            {
               if (indexInUse.get(k))
               {
                  tagInUse = true;
                  break;
               }
            }
            if (!tagInUse)
            {
               PositionItem api = new PositionItem();
               api.position = index;
               api.type = type;
               api.actualChar = specialCharacter;
               if (type == TYPE_MULTI_LINE_COMMENT_START)
               {
                  // Assumption is the end characters
                  // are set in the same order as the
                  // beginning characters in the properties array
                  api.correspondingCloseChar = counter.MultiLineCommentEnd.get(x);
               }
               else if (type == TYPE_MULTI_LINE_COMMENT_END || type == TYPE_SINGLE_LINE_COMMENT)
               {
                  api.correspondingCloseChar = null;
               }
               else
               {
                  // Assumption is the beginning quote character will
                  // always match the end
                  api.correspondingCloseChar = specialCharacter;
               }

               positionItemList.add(api);

               inStringLiteralCloseChar = specialCharacter;

               for (int j = index; j <= endIndex; j++)
               {
                  indexInUse.set(j, true);
               }
            }

            index = line.indexOf(specialCharacter, index + 1); // Re-establish
            // the
            // loop
            endIndex = index + specialCharacter.length() - 1;
         }
      }
   }

   private void FindCompilerDirectives()
   {
      for (int i = 0; i < counter.CompilerDir.size(); i++)
      {
         int index = line.indexOf(counter.CompilerDir.get(i));
         while (index >= 0)
         {
            for (int j = index; j < index + counter.CompilerDir.get(i).length(); j++)
            {
               indexInUse.set(j, true);
            }
            index = line.indexOf(counter.CompilerDir.get(i), index + 1);
         }
      }
      for (int i = 0; i < counter.CompilerChar.size(); i++)
      {
         int index = line.indexOf(counter.CompilerChar.get(i));
         while (index >= 0)
         {
            for (int j = index; j < index + counter.CompilerChar.get(i).length(); j++)
            {
               indexInUse.set(j, true);
            }
            index = line.indexOf(counter.CompilerChar.get(i), index + 1);
         }
      }
   }

   /**
    * Function to arrange the special characters in order as they appear in the
    * line, call the CreateDeleteItemList function and call the
    * DeleteSpecialChars function
    * 
    * @param cntrResult
    *           The result object which holds all of the counters
    *
    */
   private void CleanUpLine(UCCFile cntrResult)
   {
      // Sort the array in order of 1st position occurrence, then
      // length of position item descending
      Comparator<PositionItem> sortByPosition = (p, o) -> p.position.compareTo(o.position);
      Comparator<PositionItem> sortByLength =
               (p, o) -> new Integer(p.actualChar.length()).compareTo(new Integer(o.actualChar.length()));
      positionItemList = positionItemList.stream().sorted(sortByPosition.thenComparing(sortByLength.reversed()))
               .collect(Collectors.toList());

      // Remove Duplicate Positions, (e.g. #, and #! will both have the
      // same start index, and in this example, we want to keep #! and
      // eliminate #)
      int currentPos = 0;
      int previousPos = 0;
      for (int x = 0; x < positionItemList.size() && !line.isEmpty(); x++)
      {
         currentPos = positionItemList.get(x).position;
         if (x == 0)
         {
            previousPos = currentPos;
            continue;
         }
         else if (previousPos == currentPos)
         {
            positionItemList.get(x).remove = true;
            previousPos = currentPos;
         }
      }

      // Re-assemble the list
      List<PositionItem> positionItemListCleaned = new ArrayList<PositionItem>();
      for (int x = 0; x < positionItemList.size() && !line.isEmpty(); x++)
      {
         if (!positionItemList.get(x).remove)
         {
            positionItemListCleaned.add(positionItemList.get(x));
         }
      }

      CreateDeleteItemList(positionItemListCleaned, cntrResult);

      // We assume the end was in the truncated half which is gone
      if (truncated)
      {
         inMultiLineComment = false;
         inStringLiteral = false;
      }
      // When in a multi-line comment and did not find end, remove line
      if (deleteItemList.isEmpty())
      {
         if (inMultiLineComment)
         {
            cntrResult.NumWholeComments++;
            line = "";
         }
         else if (inStringLiteral)
         {
            cntrResult.NumPSLOC++;
            line = "";
         }
      }

      if (line.length() > 0)
      {
         if (!deleteItemList.isEmpty())
         {
            DeleteSpecialChars(cntrResult);
         }
         else
         {
            cntrResult.NumPSLOC++;
         }
      }
   }

   /**
    * Function for adding to the deleteItemList for positions that need to be
    * removed for the line
    * 
    * @param positionItemListCleaned
    *           list of special characters in order as they appear in the line
    * 
    * @param cntrResult
    *           The result object which holds all of the counters
    * 
    */
   private void CreateDeleteItemList(List<PositionItem> positionItemListCleaned, UCCFile cntrResult)
   {
      int nextPosition = 0;

      // Process the Position Array
      for (int x = 0; x < positionItemListCleaned.size() && !line.isEmpty(); x++)
      {
         if (inMultiLineComment)
         {
            if (positionItemListCleaned.get(x).type.equals(TYPE_MULTI_LINE_COMMENT_END))
            {
               deleteItemList.add(new DeleteItem(0,
                        positionItemListCleaned.get(x).position + positionItemListCleaned.get(x).actualChar.length(),
                        positionItemListCleaned.get(x).type));
               inMultiLineComment = false;
            }
            continue;
         }
         else if (inStringLiteral)
         {
            if (positionItemListCleaned.get(x).actualChar.equals(closingStringLiteral))
            {
               deleteItemList.add(new DeleteItem(0, positionItemListCleaned.get(x).position,
                        positionItemListCleaned.get(x).type));
               inStringLiteral = false;
               turnedStringLiteralOff = true;
            }
            continue;
         }
         if (x >= nextPosition)
         {
            if (positionItemListCleaned.get(x).type.equals(TYPE_STRING_LITERAL))
            {
               // Find the closing Literal, starting after the current
               // item
               boolean foundClosingLiteral = false;
               for (int y = (x + 1); y < positionItemListCleaned.size() && !line.isEmpty(); y++)
               {
                  if (positionItemListCleaned.get(y).type.equals(TYPE_STRING_LITERAL)
                           && positionItemListCleaned.get(y).actualChar
                                    .equals(positionItemListCleaned.get(x).correspondingCloseChar))
                  {
                     deleteItemList.add(new DeleteItem(positionItemListCleaned.get(x).position,
                              positionItemListCleaned.get(y).position, positionItemListCleaned.get(x).type));
                     nextPosition = y + 1;
                     foundClosingLiteral = true;
                     break;
                  }
               }
               if (!foundClosingLiteral)
               {
                  inStringLiteral = true;
                  inStringLiteralCloseChar = positionItemListCleaned.get(x).correspondingCloseChar;
                  deleteItemList.add(new DeleteItem(positionItemListCleaned.get(x).position, line.length(),
                           positionItemListCleaned.get(x).type));
                  closingStringLiteral = inStringLiteralCloseChar;
                  // Ignore the remaining positions
                  break;
               }
            }
            else if (positionItemListCleaned.get(x).type.equals(TYPE_SINGLE_LINE_COMMENT))
            {
               if (positionItemListCleaned.get(x).position == 0)
               {
                  cntrResult.NumWholeComments++;
                  line = "";
                  break;
               }
               else
               {
                  deleteItemList.add(new DeleteItem(positionItemListCleaned.get(x).position, line.length(),
                           positionItemListCleaned.get(x).type));
                  break;
               }
            }
            else if (positionItemListCleaned.get(x).type.equals(TYPE_MULTI_LINE_COMMENT_START))
            {
               // Find the closing multi-line comment character,
               // starting after the current item
               boolean foundClosingMultiLine = false;
               for (int y = (x + 1); y < positionItemListCleaned.size() && !line.isEmpty(); y++)
               {
                  if (positionItemListCleaned.get(y).type.equals(TYPE_MULTI_LINE_COMMENT_END)
                           && positionItemListCleaned.get(y).actualChar
                                    .equals(positionItemListCleaned.get(x).correspondingCloseChar))
                  {
                     deleteItemList.add(new DeleteItem(positionItemListCleaned.get(x).position,
                              positionItemListCleaned.get(y).position
                                       + positionItemListCleaned.get(y).actualChar.length(),
                              positionItemListCleaned.get(x).type));
                     nextPosition = y + 1;
                     foundClosingMultiLine = true;
                     break;
                  }
               }
               if (!foundClosingMultiLine)
               {
                  inMultiLineComment = true;
                  inMultiLineCommentCloseChar = positionItemList.get(x).correspondingCloseChar;
                  deleteItemList.add(new DeleteItem(positionItemList.get(x).position, line.length(),
                           positionItemListCleaned.get(x).type));
                  // Ignore the remaining positions
                  break;
               }
            }
         }
      }
   }

   /**
    * Function for removing the sections of the code which need to be deleted
    * according to the deleteItemList
    * 
    * @param cntrResult
    *           The result object which holds all of the counters
    * 
    */
   private void DeleteSpecialChars(UCCFile cntrResult)
   {
      String newLine = "";
      int previousEnd = 0;
      boolean hasEmbeddedComment = false;
      for (int m = 0; m < deleteItemList.size(); m++)
      {
         if (deleteItemList.get(m).type.equals(TYPE_STRING_LITERAL))
         {
            if (turnedStringLiteralOff)
            {
               newLine = newLine + line.substring(previousEnd, deleteItemList.get(m).startPosition);
            }
            else
            {
               newLine = newLine + line.substring(previousEnd, deleteItemList.get(m).startPosition + 1);
            }
         }
         else if (deleteItemList.get(m).type.equals(TYPE_MULTI_LINE_COMMENT_START)
                  || deleteItemList.get(m).type.equals(TYPE_SINGLE_LINE_COMMENT))
         {
            hasEmbeddedComment = true;
            if (deleteItemList.get(m).startPosition != 0)
            {
               newLine = newLine + line.substring(previousEnd, deleteItemList.get(m).startPosition);
            }
         }
         else if (deleteItemList.get(m).type.equals(TYPE_MULTI_LINE_COMMENT_END))
         {
            inMultiLineComment = false;
            hasEmbeddedComment = true;
         }

         // Clean up and grab rest of the line after last delete item
         if (m + 1 == deleteItemList.size() && previousEnd < line.length())
         {
            newLine = newLine + line.substring(deleteItemList.get(m).endPosition, line.length());
         }
         previousEnd = deleteItemList.get(m).endPosition;
      }
      line = newLine;
      line = line.trim();

      if (line.isEmpty())
      {
         cntrResult.NumWholeComments++;
      }
      else if (!line.isEmpty())
      {
         if (hasEmbeddedComment)
         {
            cntrResult.NumEmbeddedComments++;
         }
         cntrResult.NumPSLOC++;
      }
   }

   /**
    * Class for storing variables needed for positions within the line as well
    * as a functions to set default values to these variables.
    */
   class PositionItem
   {
      public Integer position;
      public String type;
      public String actualChar;
      public String correspondingCloseChar;
      boolean remove;

      public PositionItem()
      {
         position = -1;
         type = "";
         actualChar = "";
         correspondingCloseChar = "";
         remove = false;
      }
   }

   /**
    * Class for storing variables which need to be deleted from the line as well
    * as a functions to set these variables.
    */
   class DeleteItem
   {
      Integer startPosition;
      Integer endPosition;
      String type;

      public DeleteItem(Integer startPosition, Integer endPosition, String type)
      {
         this.startPosition = startPosition;
         this.endPosition = endPosition;
         this.type = type;
      }
   }

   /**
    * Class for storing variables of the resulting line after modifications
    */
   class ResultObject
   {
      String line;
      String closingStringLiteral;
      boolean inMultiLineComment;
      boolean inStringLiteral;
      int truncateLinesCount;
   }
}
