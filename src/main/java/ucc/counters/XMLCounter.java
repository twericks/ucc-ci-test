package ucc.counters;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ucc.counters.handlers.MultiLanguageHandler;
import ucc.datatypes.Constants;
import ucc.datatypes.UCCFile;
import ucc.langprops.LanguageProperties;
import ucc.utils.FileUtils;
import ucc.utils.TimeUtils;

/**
 * XMLCounter class generates the values of specified metrics for code written
 * in XML.
 * 
 * @author Integrity Applications Incorporated
 * 
 */
public class XMLCounter extends CodeCounter
{
   /** Instantiate the Log4j2 logger for this class */
   private static final Logger logger = LogManager.getLogger(XMLCounter.class);

   /**
    * Default constructor to instantiate a XMLCounter object
    * 
    * @param langProps
    *           Language properties for this counter
    */
   public XMLCounter(LanguageProperties langProps)
   {
      // Call super class's constructor
      super(langProps);

      // Create the multi-language handler
      MultiLanguageHandler = new MultiLanguageHandler(LangProps, RtParams);
   }

   /**
    * Computes Source Lines of Code metrics for given file. Metrics include:
    * Physical Source Line of Code (PSLOC) counts Logical Source Line of Code
    * (LSLOC) counts
    *
    * @param cntrResults
    *           A UCCFile object ArrayList to store results of code counters
    * @param i
    *           The index of the UCCFile we want to work on
    */
   public void CountSLOC(ArrayList<UCCFile> cntrResults, int i)
   {
      long MAX_FILE_SIZE = 100;
      boolean skippedMLH = false;
      try
      {
         if (cntrResults.get(i).EmbOfIdx == -1)
         {
            File file = new File(cntrResults.get(i).FileName);
            long size = file.length() / 1000;

            // When xml file is less than 100kb, search for embedded code
            if (size < MAX_FILE_SIZE)
            {
               cntrResults.get(i).HasEmbCode = true;
               logger.debug("Checking " + cntrResults.get(i).FileName + " for embedded languages");
               MultiLanguageHandler.HandleEmbeddedCode(cntrResults, i);
            }
            // Skipping MultiLanguageHandler
            else
            {
               cntrResults.get(i).EmbOfIdx = -2;
               skippedMLH = true;
            }
         }
      }
      catch (Exception e)
      {
         logger.error("An error occurred while checking " + cntrResults.get(i).FileName + " for embedded languages");
         logger.error(e);
      }

      try
      {
         // When a file's EmbOfIdx is still -1, then we have failed to extract
         // out the embedded code
         if (cntrResults.get(i).EmbOfIdx == -1)
         {
            System.err.println("Failed to extract embedded code from " + cntrResults.get(i).FileName);
            logger.debug("Failed to extract embedded code from " + cntrResults.get(i).FileName);
         }
         // We have extracted out the embedded code or skipped MLH, count the
         // file
         else if (cntrResults.get(i).EmbOfIdx != -2 || skippedMLH)
         {
            cntrResults.get(i).HasEmbCode = false;
            // Count PSLOC and complexity
            CountFileSLOC(cntrResults, i);
         }
      }
      catch (Exception e)
      {
         logger.error("An error occurred while counting SLOC for " + cntrResults.get(i).FileName);
         logger.error(e);
      }
   }

   /**
    * Counts SLOC.
    * 
    * @param cntrResults
    *           A UCCFile object ArrayList to store results of code counters
    * @param i
    *           The index of the UCCFile we want to work on
    */
   protected void CountFileSLOC(ArrayList<UCCFile> cntrResults, int i)
   {
      globals globalVars = new globals();
      UCCFile cntrResult = cntrResults.get(i);
      long startTime = 0;
      long endTime = 0;
      startTime = TimeUtils.GetTime();
      logger.debug(TimeUtils.PrintTimestamp() + " Counting SLOC for " + cntrResult.FileName);

      LineNumberReader reader = null;

      int lsloc = 0; // Logical SLOC
      String regEx;
      Matcher matcher;
      int lineIndex = 0; // Index of the line used for checksumming lines in
                         // sequence

      // Initialize complexity keywords/counts for this file
      InitAllCmplxKeywords(cntrResult);

      // Buffered writer for _PSLOC file saving
      BufferedWriter bw = null;

      globalVars.lslocLineValue = 0;
      String tempLine = "";
      File file = null;

      try
      {
         // If we're differencing baselines...
         if (RtParams.DiffCode)
         {
            // Create file for LSLOC storage based on if file is embedded
            if (cntrResult.EmbOfIdx == -2)
            {
               file = new File(FileUtils.BuildTempOutFileName_LSLOC(RtParams, cntrResult));
            }
            else
            {
               file = new File(
                        FileUtils.BuildTempOutFileName_LSLOC_Embedded(RtParams, cntrResults.get(cntrResult.EmbOfIdx)));
            }

            // If LSLOC file doesn't already exist, then create it
            if (!file.exists())
            {
               file.createNewFile();
            }

            // true = append file
            FileWriter fileWritter = new FileWriter(file.getAbsolutePath(), true);
            bw = new BufferedWriter(fileWritter);
         }

         reader = new LineNumberReader(
                  new InputStreamReader(new FileInputStream(cntrResult.FileName), Constants.CHARSET_NAME));

         // Read first line
         String line = reader.readLine();

         // Delete UTF-8 BOM instance
         line = DeleteUTF8BOM(line);

         // Reset truncated lines count
         truncateLinesCount = 0;

         // Pre-processing Variables
         boolean inMultiLineComment = false;
         boolean inStringLiteral = false;
         String closingStringLiteral = "";

         // While we have lines in the file...
         while (line != null)
         {
            globalVars.lslocLineValue = 0;
            tempLine = "";

            /* PREPROCESSING START */

            logger.trace("line " + reader.getLineNumber() + " in:  " + line);

            // Need to remove quote chars
            line = line.replaceAll("\"", "");
            line = line.replaceAll("'", "");

            CounterUtils cu = new CounterUtils(this);
            CounterUtils.ResultObject ro = cu.PreformPreProcessing(line, cntrResult, inMultiLineComment,
                     inStringLiteral, closingStringLiteral);
            line = ro.line;
            inMultiLineComment = ro.inMultiLineComment;
            inStringLiteral = ro.inStringLiteral;
            truncateLinesCount += ro.truncateLinesCount;
            closingStringLiteral = ro.closingStringLiteral;

            /* PREPROCESSING FINISH */

            line = line.trim();

            // If we're baseline differencing or searching for duplicates...
            if (RtParams.DiffCode || RtParams.SearchForDups)
            {
               // Only write the LSLOC line if it is not empty
               if (!line.isEmpty())
               {
                  // If we're baseline differencing, write the LSLOC line
                  if (RtParams.DiffCode)
                  {
                     tempLine = line;
                  }

                  // If we're searching for duplicates, checksum the LSLOC line
                  if (RtParams.SearchForDups)
                  {
                     // If we're in a pure XML file
                     if (cntrResult.EmbOfIdx == -2)
                     {
                        if (!cntrResult.UniqueFileName)
                        {
                           cntrResult.FileLineChecksum.add(lineIndex, line.hashCode());
                        }
                     }
                     else // If we're in an embedded XML file
                     {
                        if (!cntrResults.get(cntrResult.EmbOfIdx).UniqueFileName)
                        {
                           cntrResults.get(cntrResult.EmbOfIdx).FileLineChecksum.add(lineIndex, line.hashCode());
                        }
                     }
                     lineIndex++;
                  }
               }
            }

            // Count PSLOC and LSLOC
            if (!line.isEmpty())
            {
               line = line.replaceAll("</", " ");
               logger.trace("line " + reader.getLineNumber() + " out: " + line);

               regEx = "<";
               matcher = Pattern.compile(regEx).matcher(line);

               // Count all open tags as LSLOC
               while (matcher.find())
               {
                  lsloc++;
                  globalVars.lslocLineValue++;
               }
            }

            if (globalVars.lslocLineValue > 0 && RtParams.DiffCode)
            {
               bw.write(lslocLineValueDelim + Integer.toString(globalVars.lslocLineValue) + lslocLineValueDelim
                        + tempLine + "\n");
            }

            // Read next line
            line = reader.readLine();
         }
         endTime = TimeUtils.GetTime();
         String executedTime = TimeUtils.CalcElapsedTime(startTime, endTime);
         logger.debug("Finished counting SLOC for " + cntrResult.FileName + " Exec. Time: " + executedTime);
      }
      catch (IOException e)
      {
         logger.error("The input reader failed to open for " + cntrResults.get(i).FileName);
         logger.error(e);
      }
      catch (IndexOutOfBoundsException e)
      {
         logger.error("An error occurred while counting SLOC for " + cntrResults.get(i).FileName);
         logger.error(e);
      }
      finally
      {
         // If the original file was opened...
         if (reader != null)
         {
            // Save PSLOC metrics counted

            cntrResult.IsCounted = true;
            cntrResult.NumTotalLines = reader.getLineNumber();
            cntrResult.LangVersion = LangProps.GetLangVersion();

            cntrResult.NumLSLOC = lsloc;
            cntrResult.NumExecInstrLog = cntrResult.NumLSLOC;
            cntrResult.NumDataDeclPhys = cntrResult.NumDataDeclLog;
            cntrResult.NumExecInstrPhys = cntrResult.NumExecInstrLog;

            // Close the original file
            try
            {
               reader.close();
            }
            catch (IOException e)
            {
               logger.error("The input reader failed to close.");
               logger.error(e);
            }
            reader = null;
         }

         // If the _LSLOC file was opened...
         if (bw != null)
         {
            // Close the original file
            try
            {
               bw.close();
            }
            catch (IOException e)
            {
               logger.error("The LSLOC writer failed to close.");
               logger.error(e);
            }
            bw = null;
         }

         // Log the number of lines in the file that were truncated
         if (truncateLinesCount > 0)
         {
            logger.warn("Truncated " + truncateLinesCount + " total lines in file " + cntrResult.FileName
                     + "  [-trunc set to " + Integer.toString(RtParams.TruncThreshold) + "]");
         }
      }
   }
}