package ucc.counters;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ucc.datatypes.CmplxDataType;
import ucc.datatypes.Constants;
import ucc.datatypes.UCCFile;
import ucc.langprops.LanguageProperties;
import ucc.utils.FileUtils;
import ucc.utils.TimeUtils;

/**
 * DOSBatchCounter class performs various code counting operations on
 * baseline(s) identified by the user. It contains algorithms/methods to count
 * DOS Batch programming language files.
 *
 * @author Integrity Applications Incorporated
 *
 */
public class DOSBatchCounter extends CodeCounter
{
   /** Instantiate the Log4j2 logger for this class */
   private static final Logger logger = LogManager.getLogger(DOSBatchCounter.class);

   /**
    * Default constructor to instantiate a CShellCounter object
    * 
    * @param langProps
    *           Language properties for this counter
    */
   public DOSBatchCounter(LanguageProperties langProps)
   {
      // Call super class's constructor
      super(langProps);
   }

   /**
    * Counts Physical SLOC. Current algorithm:
    * 
    * Read first line
    * 
    * Delete the UTF-8 BOM, if found
    * 
    * While we have lines in the file... { - Do some pre-processing - Count
    * comments with handler (also counts blank lines) - Count compiler
    * directives with handler - Count non-blank lines - Do some post-processing
    * - Calculate PSLOC as non-blank lines + compiler directives + number of
    * continued compiler directives (these were erased in the compiler handler)
    * - Save post-processed line in new file for LSLOC counting - Read next line
    * }
    */
   protected void CountFilePSLOC(UCCFile cntrResult)
   {
      long startTime = 0;
      long endTime = 0;
      startTime = TimeUtils.GetTime();
      logger.debug(TimeUtils.PrintTimestamp() + " Counting PSLOC for " + cntrResult.FileName);

      LineNumberReader reader = null;

      String regEx;
      Matcher matcher;

      // Buffered writer for _PSLOC file saving
      BufferedWriter bw = null;

      boolean cdNewLine = false;

      try
      {
         // Create file for PSLOC storage
         File file = new File(FileUtils.BuildTempOutFileName_PSLOC(RtParams, cntrResult));
         bw = new BufferedWriter(new FileWriter(file));

         reader = new LineNumberReader(
                  new InputStreamReader(new FileInputStream(cntrResult.FileName), Constants.CHARSET_NAME));

         // Read first line
         String line = reader.readLine();

         // Delete UTF-8 BOM instance
         line = DeleteUTF8BOM(line);

         // Reset truncated lines count
         truncateLinesCount = 0;

         // Pre-processing Variables
         boolean inMultiLineComment = false;
         boolean inStringLiteral = false;
         String closingStringLiteral = "";

         // While we have lines in the file...
         while (line != null)
         {
            /* PREPROCESSING START */

            logger.trace("line " + reader.getLineNumber() + " in:  " + line);
            CounterUtils cu = new CounterUtils(this);
            CounterUtils.ResultObject ro = cu.PreformPreProcessing(line, cntrResult, inMultiLineComment,
                     inStringLiteral, closingStringLiteral);
            line = ro.line;
            inMultiLineComment = ro.inMultiLineComment;
            inStringLiteral = ro.inStringLiteral;
            truncateLinesCount += ro.truncateLinesCount;
            closingStringLiteral = ro.closingStringLiteral;

            /* PREPROCESSING FINISH */

            /* POSTPROCESSING START */

            if (!line.isEmpty())
            {
               // Undo line continuations
               for (int lcc = 0; lcc < LineContChars.size(); lcc++)
               {
                  if (line.endsWith(LineContChars.get(lcc)))
                  {
                     line += " ";
                  }
                  else
                  {
                     line += "\n";
                  }
               }

               // If we're not in a echo line
               regEx = "\\becho\\b";
               matcher = Pattern.compile(regEx).matcher(line);
               if (!(matcher.find() && matcher.start() == 0))
               {
                  // Put a newline after each "do"
                  regEx = "\\bfor\\b(.*)\\bdo\\b\\s+\\(";
                  matcher = Pattern.compile(regEx).matcher(line);
                  if (!matcher.find())
                  {
                     regEx = "\\bfor\\b(.*)\\bdo";
                     matcher = Pattern.compile(regEx).matcher(line);
                     if (matcher.find())
                     {
                        line = line.substring(0, matcher.end()) + "\n" + line.substring(matcher.end());
                     }
                  }

                  // Put a newline before each "if"
                  regEx = "\\bif\\b";
                  matcher = Pattern.compile(regEx).matcher(line);
                  if (matcher.find())
                  {
                     line = line.replaceAll(regEx, "\nif");
                  }

                  // Put a newline before and after each "else"
                  regEx = "\\belse\\b";
                  matcher = Pattern.compile(regEx).matcher(line);
                  if (matcher.find())
                  {
                     line = line.replaceAll(regEx, "\nelse\n");
                  }
               }
            }

            // Replace all double newlines with single newlines
            while (line.contains("\n\n"))
            {
               line = line.replaceAll("\n\n", "\n");
            }

            // Delete all lines that are just a newline
            if (line.trim().equals("\n"))
            {
               line = "";
            }

            // Add a new line after all compiler directives
            cdNewLine = false;
            for (int cd = 0; cd < CompilerDir.size(); cd++)
            {
               if (line.contains(CompilerDir.get(cd)))
               {
                  line += "\n";
                  cdNewLine = true;
               }
            }
            if (!cdNewLine)
            {
               for (int cc = 0; cc < CompilerChar.size(); cc++)
               {
                  if (line.contains(CompilerChar.get(cc)))
                  {
                     line += "\n";
                  }
               }
            }

            // Replace leading whitespace for new lines
            while (line.contains("\n "))
            {
               line = line.replaceAll("\n ", "\n");
            }

            if (!line.trim().isEmpty())
            {
               bw.write(line);
            }

            /* POSTPROCESSING FINISH */

            // Read next line
            line = reader.readLine();
         }
         endTime = TimeUtils.GetTime();
         String executedTime = TimeUtils.CalcElapsedTime(startTime, endTime);
         logger.debug("Finished counting PSLOC for " + cntrResult.FileName + " Exec. Time: " + executedTime);
      }
      catch (IOException e)
      {
         logger.error("The input reader failed to open.");
         logger.error(e);
      }
      finally
      {
         // If the original file was opened...
         if (reader != null)
         {
            // Save PSLOC metrics counted
            cntrResult.IsCounted = true;
            cntrResult.NumTotalLines = reader.getLineNumber();
            cntrResult.LangVersion = LangProps.GetLangVersion();

            // Close the original file
            try
            {
               reader.close();
            }
            catch (IOException e)
            {
               logger.error("The input reader failed to close.");
               logger.error(e);
            }
            reader = null;
         }

         // If the _PSLOC file was opened...
         if (bw != null)
         {
            try
            {
               bw.close();
            }
            catch (IOException e)
            {
               logger.error("The PSLOC writer failed to close.");
               logger.error(e);
            }
            bw = null;
         }

         // Log the number of lines in the file that were truncated
         if (truncateLinesCount > 0)
         {
            logger.warn("Truncated " + truncateLinesCount + " total lines in file " + cntrResult.FileName
                     + "  [-trunc set to " + Integer.toString(RtParams.TruncThreshold) + "]");
         }
      }
   }

   /**
    * Counts Logical SLOC. Current algorithm:
    * 
    * Read first line
    * 
    * Delete the UTF-8 BOM, if found
    * 
    * While we have lines in the file... { - Build true LSLOC and save to LSLOC
    * file - Search for duplicates based on threshold - Delete everything
    * between quotes - Count data keywords - Count executable keywords - Count
    * complexity keywords - Increment complexity loop level based on keyword -
    * Count data declarations - Delete exclude keywords and characters
    * 
    * If the line is not empty, increment the LSLOC counter to catch leftover
    * LSLOC
    * 
    * Read next line }
    */
   protected void CountFileLSLOC(UCCFile cntrResult)
   {
      long startTime = 0;
      long endTime = 0;
      startTime = TimeUtils.GetTime();
      logger.debug(TimeUtils.PrintTimestamp() + " Counting LSLOC for " + cntrResult.FileName);

      globals globalVars = new globals();
      tempLoop tempLoopObj = new tempLoop();
      complexity complexityObj = new complexity();
      cyclomaticComplexity cyclomaticComplexityObj = new cyclomaticComplexity();

      LineNumberReader reader = null;

      int lsloc = 0; // Logical SLOC counter
      String tempLine = ""; // String for storing a temporary version of the
                            // line
      int lineIndex = 0; // Index of the line used for checksumming lines in
                         // sequence

      globalVars.lslocLineValue = 0;
      File file = null;

      // Zero out function variables for cyclomatic complexity metrics
      cyclomaticComplexityObj.functionLevel = 0;

      // Add base loop level to get started
      complexityObj.loopLevelCount.add(0);

      // Add base CC level for the cyclomatic complexity counts
      cntrResult.CyclCmplxCnts.add(new CmplxDataType());

      // Set the base CC level's function name to blank since Matlab can contain
      // scripts with no functions
      cntrResult.CyclCmplxCnts.get(cyclomaticComplexityObj.functionLevel).Keyword = " ";

      // Initialize complexity keywords/counts for this file
      InitAllCmplxKeywords(cntrResult);

      // Buffered writer for _LSLOC file saving
      BufferedWriter bw = null;

      try
      {
         // If we're differencing baselines...
         if (RtParams.DiffCode)
         {
            // Create file for LSLOC storage
            file = new File(FileUtils.BuildTempOutFileName_LSLOC(RtParams, cntrResult));
            bw = new BufferedWriter(new FileWriter(file));
         }

         reader = new LineNumberReader(
                  new InputStreamReader(new FileInputStream(FileUtils.BuildTempOutFileName_PSLOC(RtParams, cntrResult)),
                           Constants.CHARSET_NAME));

         // Read first line
         String line = reader.readLine();

         // Delete UTF-8 BOM instance
         line = DeleteUTF8BOM(line);

         // While we have lines in the file...
         while (line != null)
         {
            // Reset conditional
            globalVars.lslocLineValue = 0;

            logger.trace("line " + reader.getLineNumber() + " in:  " + line);

            // If we're baseline differencing or searching for duplicates...
            if (RtParams.DiffCode || RtParams.SearchForDups)
            {
               // Save line into a temporary line for LSLOC writing
               tempLine = line;

               // Delete all exclude keywords
               tempLine = DeleteExcludeKeywords(tempLine);

               // Delete all exclude characters
               tempLine = DeleteExcludeCharacters(tempLine);
            }

            // Count executable keyword occurrences
            CountKeywords(cntrResult.ExecKeywordCnts, line);

            // Count data keyword occurrences
            CountKeywords(cntrResult.DataKeywordCnts, line);

            line = line.trim();

            // Count complexity keywords and calculate loop level complexity
            if (RtParams.CountCmplxMetrics && !line.isEmpty())
            {
               CountComplexity(cntrResult, line);
            }

            // Delete all compiler directive lines left over from PSLOC
            line = DeleteCompilerDirectives(cntrResult, line, globalVars);

            // Count complexity keywords and calculate loop level complexity
            if (RtParams.CountCmplxMetrics && !line.isEmpty())
            {
               // Increment complexity loop level based on keyword
               getLoopLevel(line, complexityObj);

               // Determine function name
               getFunctionName(cntrResult, line, cyclomaticComplexityObj);

               // Increment cyclomatic complexity loop level based on keyword
               getCyclomaticComplexity(cntrResult, line, cyclomaticComplexityObj);
            }

            // Delete exclude keywords
            line = DeleteExcludeKeywords(line);

            // Count multiple if's on one line
            String regEx;
            Matcher matcher;

            line = line.trim();

            regEx = "\\(";
            matcher = Pattern.compile(regEx).matcher(line);
            if (!matcher.find())
            {
               regEx = "\\b" + "if" + "\\b";
               matcher = Pattern.compile(regEx).matcher(line);
               while (matcher.find())
               {
                  lsloc++;
                  globalVars.lslocLineValue++;
               }
            }

            // Delete exclude characters
            line = DeleteExcludeCharacters(line);

            // Delete lines that begin with :
            line = DeleteLinesStartingWithAColon(line);

            // Count LSLOC
            if (!line.trim().isEmpty())
            {
               logger.trace("line " + reader.getLineNumber() + " out: " + line);
               lsloc++;
               globalVars.lslocLineValue++;
            }

            // Write the Raw LSLOC Line for Differencer
            if (globalVars.lslocLineValue > 0)
            {
               // If we're baseline differencing, write the LSLOC line
               if (RtParams.DiffCode)
               {
                  bw.write(lslocLineValueDelim + Integer.toString(globalVars.lslocLineValue) + lslocLineValueDelim
                           + tempLine + "\n");
               }

               // If we're searching for duplicates, checksum the LSLOC line
               if (RtParams.SearchForDups && !cntrResult.UniqueFileName)
               {
                  cntrResult.FileLineChecksum.add(lineIndex, tempLine.hashCode());
                  lineIndex++;
               }
            }

            // Read next line
            line = reader.readLine();
         }
         endTime = TimeUtils.GetTime();
         String executedTime = TimeUtils.CalcElapsedTime(startTime, endTime);
         logger.debug("Finished counting LSLOC for " + cntrResult.FileName + " Exec. Time: " + executedTime);
      }
      catch (IOException e)
      {
         logger.error("The PSLOC reader failed to open.");
         logger.error(e);
      }
      finally
      {
         // If we're counting complexity...
         if (RtParams.CountCmplxMetrics)
         {
            // Scale complexity loop keyword counts to proper loop levels
            ScaleComplexityLoopLevels(cntrResult, complexityObj);

            // Calculate cyclomatic complexity
            CalculateCyclomaticComplexity(cntrResult);
         }

         // If the _PSLOC file was opened...
         if (reader != null)
         {
            // Save LSLOC metrics counted
            cntrResult.NumLSLOC = cntrResult.NumCompilerDirectives + lsloc + cntrResult.NumDataDeclLog;
            cntrResult.NumExecInstrLog =
                     cntrResult.NumLSLOC - cntrResult.NumDataDeclLog - cntrResult.NumCompilerDirectives;
            cntrResult.NumDataDeclPhys = cntrResult.NumDataDeclLog;
            cntrResult.NumExecInstrPhys = cntrResult.NumExecInstrLog;
            cntrResult.LangVersion = LangProps.GetLangVersion();

            // Close the _PSLOC file
            try
            {
               reader.close();
            }
            catch (IOException e)
            {
               logger.error("The PSLOC reader failed to close.");
               logger.error(e);
            }
            reader = null;

            // Delete PSLOC file
            FileUtils.DeleteFile(FileUtils.BuildTempOutFileName_PSLOC(RtParams, cntrResult));
         }

         // If the _LSLOC file was opened...
         if (bw != null)
         {
            // Close the _LSLOC file
            try
            {
               bw.close();
            }
            catch (IOException e)
            {
               logger.error("The LSLOC writer failed to close.");
               logger.error(e);
            }
            bw = null;
         }

         // Reset objects
         complexityObj.loopLevelCount.clear();
         complexityObj.loopLevel = 0;
         cyclomaticComplexityObj.functionLevel = 0;
      }
   }

   /**
    * Function for getting the loop level for a particular line based on
    * occurrences of the loop keywords list.
    * 
    * @param line
    *           Line coming in from file
    * @param complexityObj
    *           Class that holds all complexity variables
    *
    */
   protected void getLoopLevel(String line, complexity complexityObj)
   {
      String regEx, regEx1, regEx2;
      Matcher matcher, matcher1, matcher2;

      int loopCounter = 0;
      int currentLevel = 0;

      regEx = "\\becho\\b";
      matcher = Pattern.compile(regEx).matcher(line);
      if (!(matcher.find() && matcher.start() == 0))
      {
         regEx1 = "\\bfor\\b(.*)\\bdo\\b\\s+\\(";
         matcher1 = Pattern.compile(regEx1).matcher(line);

         regEx2 = "\\bfor\\b(.*)\\bdo\\b";
         matcher2 = Pattern.compile(regEx2).matcher(line);

         // If we find any of the loop keywords on the given line
         if (matcher1.find())
         {
            // Set the loop counter to the integer value of the current loop
            // level counter array list. This gets the
            // current loop count per the level we're on
            loopCounter = complexityObj.loopLevelCount.get(complexityObj.loopLevel).intValue();

            // Increment the loop counter
            loopCounter++;

            // Set the loop level counter arrayList level <loopLevel> to the
            // count <loopCounter>
            complexityObj.loopLevelCount.set(complexityObj.loopLevel, loopCounter);

            // Increment the loop level
            complexityObj.loopLevel++;

            // If the loop level counter arrayList size is less than or equal to
            // the loop level integer
            if (complexityObj.loopLevelCount.size() <= complexityObj.loopLevel)
            {
               // Add another level to the level counter arrayList
               complexityObj.loopLevelCount.add(0);
            }
         }
         else if (matcher2.find())
         {
            // Set the loop counter to the integer value of the current loop
            // level counter array list. This gets the
            // current loop count per the level we're on
            loopCounter = complexityObj.loopLevelCount.get(complexityObj.loopLevel).intValue();

            // Get current level (so we know where to jump back to if we go up
            // multiple levels on one line
            currentLevel = complexityObj.loopLevel;

            regEx = "\\bfor\\b";
            matcher = Pattern.compile(regEx).matcher(line);
            while (matcher.find())
            {
               loopCounter = complexityObj.loopLevelCount.get(complexityObj.loopLevel).intValue();

               // Increment the loop counter
               loopCounter++;

               // Set the loop level counter arrayList level <loopLevel> to the
               // count <loopCounter>
               complexityObj.loopLevelCount.set(complexityObj.loopLevel, loopCounter);

               // Increment the loop level
               complexityObj.loopLevel++;

               // If the loop level counter arrayList size is less than or equal
               // to the loop level integer
               if (complexityObj.loopLevelCount.size() <= complexityObj.loopLevel)
               {
                  // Add another level to the level counter arrayList
                  complexityObj.loopLevelCount.add(0);
               }
            }

            complexityObj.loopLevel = currentLevel;
         }
         else
         {
            if (line.contains(")"))
            {
               // If the loop level is greater than 0
               if (complexityObj.loopLevel > 0)
               {
                  // Decrement the loop level
                  complexityObj.loopLevel--;
               }
            }
         }
      }
   }

   /**
    * Function for ruling out lines which are not functions and finally
    * extracting the function name if we determine that we are in a function.
    * 
    * @param cntrResult
    *           A UCCFile object to store results of code counter
    * @param line
    *           Line coming in from file
    * @param cyclomaticComplexityObj
    *           Class to hold cyclomatic complexity variables
    */
   protected void getFunctionName(UCCFile cntrResult, String line, cyclomaticComplexity cyclomaticComplexityObj)
   {
      line = line.trim();

      if (line.startsWith(":"))
      {
         cntrResult.CyclCmplxCnts.get(cyclomaticComplexityObj.functionLevel).Keyword = line;

         // Increment the number of functions
         cyclomaticComplexityObj.functionLevel++;

         // Add a new element to the cyclomatic complexity count arrayList
         cntrResult.CyclCmplxCnts.add(new CmplxDataType());
      }
   }

   /**
    * Function for counting cyclomatic complexity based on cyclomatic complexity
    * keywords
    * 
    * @param cntrResult
    *           A UCCFile object to store results of code counter
    * @param line
    *           Line coming in from file
    * @param cyclomaticComplexityObj
    *           Class to hold cyclomatic complexity variables
    */
   protected void getCyclomaticComplexity(UCCFile cntrResult, String line, cyclomaticComplexity cyclomaticComplexityObj)
   {
      String regEx;
      Matcher matcher;

      // Loop through all the cyclomatic complexity keywords...
      for (int cck = 0; cck < CyclCmplexKeywords.size() && !line.isEmpty(); cck++)
      {
         regEx = "\\b" + CyclCmplexKeywords.get(cck) + "\\b";
         matcher = Pattern.compile(regEx).matcher(line);
         StringBuffer sbLine = new StringBuffer();

         // While the line isn't empty, the line is longer than keyword, and we
         // find the pattern...
         while (!line.isEmpty() && line.length() >= CyclCmplexKeywords.get(cck).length() && matcher.find())
         {
            matcher.appendReplacement(sbLine, " ");

            // Increment the function level's CC count
            cntrResult.CyclCmplxCnts.get(cyclomaticComplexityObj.functionLevel).Count++;
         }

         // Update the line
         matcher.appendTail(sbLine);
         line = sbLine.toString();
      }
   }

   /**
    * Function for adding all the loop level complexity counts up in a 0 to N-1
    * format and saving to the UCCFile's complexity loop level count metrics.
    * 
    * @param cntrResult
    *           A UCCFile object to store results of code counter
    * @param complexityObj
    *           Class to hold complexity variables
    */
   protected void ScaleComplexityLoopLevels(UCCFile cntrResult, complexity complexityObj)
   {
      int finalLoopCounter = 0;

      // Loop through all the loop level counts
      for (int i = 0; i < complexityObj.loopLevelCount.size(); i++)
      {
         // If the count is non-zero
         if (complexityObj.loopLevelCount.get(i) > 0)
         {
            // Add the level to the loop level arraylist
            cntrResult.CmplxLoopLvlCnts.add(new CmplxDataType());

            // Set the count to the tallied value
            cntrResult.CmplxLoopLvlCnts.get(finalLoopCounter).Count = complexityObj.loopLevelCount.get(i);

            // Increment the loop counter
            finalLoopCounter++;
         }
      }
   }

   /**
    * Function for calculating the total cyclomatic complexity and average
    * cyclomatic complexity of the file.
    * 
    * @param cntrResult
    *           A UCCFile object to store results of code counter
    */
   protected void CalculateCyclomaticComplexity(UCCFile cntrResult)
   {
      // Calculate cyclomatic complexity
      for (int i = 0; i < cntrResult.CyclCmplxCnts.size(); i++)
      {
         // Add 1 for the file/function itself
         cntrResult.CyclCmplxCnts.get(i).Count++;

         // Sum up total cyclomatic complexity
         cntrResult.CyclCmplxTotal += cntrResult.CyclCmplxCnts.get(i).Count;
      }

      // Get average of cyclomatic complexity
      cntrResult.CyclCmplxAvg = (double) cntrResult.CyclCmplxTotal / (double) cntrResult.CyclCmplxCnts.size();
   }

   /**
    * Function for deleting lines which starts in a colon.
    *
    * @param line
    *           Line coming in from file
    * @return Empty line if the line starts with :
    */
   protected String DeleteLinesStartingWithAColon(String line)
   {
      String regEx;
      Matcher matcher;

      line = line.trim();

      regEx = "\\:";
      matcher = Pattern.compile(regEx).matcher(line);
      if (matcher.find() && matcher.start() == 0)
      {
         line = "";
      }

      return line;
   }
}
