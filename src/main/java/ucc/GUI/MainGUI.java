package ucc.GUI;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import java.awt.Font;
import java.awt.Image;

import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.LineBorder;
import ucc.GUI.JTextFieldHintListener;
import ucc.main.MainUCC;
import ucc.utils.FileUtils;
import javax.swing.JLabel;
import javax.swing.JCheckBox;
import javax.swing.JTextField;
import javax.swing.UIManager;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollBar;
import javax.swing.JTextArea;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.CardLayout;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JSpinner;
import java.io.File;
import javax.swing.JFileChooser;
import java.awt.dnd.*;

public class MainGUI
{

   private JFrame frame;

   private JPanel leftpane;
   private JPanel rightpane;
   private JSplitPane splitpane_leftRight;
   private JPanel uppane;
   private JPanel downpane;
   private JSplitPane splitPane_upDown;

   private JPanel panel_basic_count;
   private JPanel panel_basic_differencing;
   private JPanel panel_advanced_language;
   private JPanel panel_advanced_processing;
   private JPanel inner_panel_count;
   private JPanel inner_panel_diff;
   private JPanel inner_panel_language;
   private JPanel inner_panel_processing;

   // ------------ left panel --------------------
   private JComboBox comboBox_CountingOrDifferencing;
   private JLabel lblBasic;
   private JLabel lblAdvanced;
   private JLabel lblLanguage;
   private JLabel lblProcessing;
   // ------------ language panel-----------------
   private JTextField textField_language_FileExtensionMapping;
   private JButton btnBrowse_language_FileExtensionMapping;
   private JRadioButton rdbtn_language_UseCustomLanguage;
   private JTextField textField_CustomLanguageProperties;
   private JButton btnBrowse_language_CustomLanguageProperties;
   private JLabel lblSelectLanguageForExport;
   private JComboBox comboBox_language_SelectLanguageForExport;
   private JButton button_language_Export;
   private JButton button_language_Reset;

   // ------------ processing panel-----------------
   private JCheckBox chckbx_processing_UnifiedOutput;
   private JCheckBox chckbx_processing_AsciiOutputFormat;
   private JLabel lblModificationThreshold;
   private JSpinner spinner_processing_Modification;
   private JLabel lblDuplicateThreshold;
   private JSpinner spinner_processing_Duplicate;
   private JLabel lblLslocTruncationThreshold;
   private JSpinner spinner_processing_LSLOC;
   private JLabel lblLogReportingLevel;
   private JComboBox comboBox_processing_Log;
   private JButton button_processing_Reset;

   // ------------ basic-counting panel-----------------
   private JCheckBox chckbx_counting_Complexity;
   private JCheckBox chckbx_counting_DuplicateFileChecking;
   private JCheckBox chckbx_counting_FollowSymbolicLinks;
   private JCheckBox chckbx_counting_TranslateClearcaseFilenames;
   // private JCheckBox chckbx_counting_FunctionLevel;
   private JButton button_counting_Reset;
   private JRadioButton rdbtn_counting_InputDirectory;
   private JRadioButton rdbtn_counting_FileList;
   private JTextField textField_counting_FileInput;
   private JButton btnBrowse_counting_FileInput;
   private JLabel lbl_counting_OutputDirectory;
   private JTextField textField_counting_FileOutput;
   private JButton btnBrowse_counting_FileOutput;

   // ------------ basic-differencing panel-----------------
   private JCheckBox chckbx_differencing_Complexity;
   private JCheckBox chckbx_differencing_DuplicateFileChecking;
   private JCheckBox chckbx_differencing_FollowSymbolicLinks;
   private JCheckBox chckbx_differencing_TranslateClearcaseFilenames;
   // private JCheckBox chckbx_differencing_FunctionLevel;
   private JButton button_differencing_Rest;
   private JRadioButton rdbtn_differencing_InputDirectory;
   private JRadioButton rdbtn_differencing_FileList;
   private JTextField textField_differencing_FileInputA;
   private JButton btnBrowse_differencing_FileInputA;
   private JLabel lbl_differencing_OutputDirectory;
   private JTextField textField_differencing_FileOutput;
   private JButton btnBrowse_differencing_FileOutput;
   private JTextField textField_differencing_FileInputB;
   private JButton btnBrowse_differencing_FileInputB;

   // ------------- down panel --------------------------
   private JTextArea textAreaCommandLine;
   private JButton btnStart;
   private JButton btnStop;
   private JScrollPane scrollPane;

   private long runningThreadID;
   private boolean isCounting = true;

   /**
    * Launch the application.
    */
   public static void main(String[] args)
   {
      EventQueue.invokeLater(new Runnable()
      {
         public void run()
         {
            try
            {
               MainGUI window = new MainGUI();
               window.frame.setVisible(true);
               window.splitpane_leftRight.setDividerLocation(0.2);
               window.splitPane_upDown.setDividerLocation(0.63);
            }
            catch (Exception e)
            {
               e.printStackTrace();
            }
         }
      });
   }

   /**
    * Create the application.
    */
   public MainGUI()
   {
      initialize();
   }

   /**
    * Initialize the contents of the frame.
    */
   private void initialize()
   {
      // initialize frame
      frame = new JFrame();
      frame.setBounds(100, 100, 911, 633);
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.setTitle("Unified Code Counter (UCC-JAVA)");

      // initialize panels
      initialize_Panel();
      initialize_Uppane_panel();
      initialize_CountingPanel();
      initialize_DifferencingPanel();
      initialize_LanguagePanel();
      initialize_ProcessingPanel();
      initialize_DownPanel();
      initialize_LeftPanel();

      frame.getContentPane().add(splitpane_leftRight);
   }

   /**
    * Set default in coutning panel
    */
   private void countingReset()
   {
      chckbx_counting_Complexity.setSelected(true);
      chckbx_counting_DuplicateFileChecking.setSelected(true);
      chckbx_counting_FollowSymbolicLinks.setSelected(true);
      chckbx_counting_TranslateClearcaseFilenames.setSelected(false);
      // chckbx_counting_FunctionLevel.setSelected(true);
      rdbtn_counting_InputDirectory.setSelected(true);
      rdbtn_counting_FileList.setSelected(false);

      textField_counting_FileInput.setText(null);
      textField_counting_FileOutput.setText(null);
   }

   /**
    * Set default in differencing panel
    */
   private void differencingReset()
   {
      chckbx_differencing_Complexity.setSelected(true);
      chckbx_differencing_DuplicateFileChecking.setSelected(true);
      chckbx_differencing_FollowSymbolicLinks.setSelected(true);
      chckbx_differencing_TranslateClearcaseFilenames.setSelected(false);
      // chckbx_differencing_FunctionLevel.setSelected(true);
      rdbtn_differencing_InputDirectory.setSelected(true);
      rdbtn_differencing_FileList.setSelected(false);

      textField_differencing_FileInputA.setText(null);
      textField_differencing_FileInputB.setText(null);
      textField_differencing_FileOutput.setText(null);

      textField_differencing_FileInputA
               .addFocusListener(new JTextFieldHintListener("BaseLine A", textField_differencing_FileInputA));
      textField_differencing_FileInputB
               .addFocusListener(new JTextFieldHintListener("BaseLine B", textField_differencing_FileInputB));
   }

   /**
    * Initialize the contents of the counting panel.
    */
   private void initialize_CountingPanel()
   {
      // basic count panel: inner_panel_count

      // 4 check boxes
      // Complexity check box
      chckbx_counting_Complexity = new JCheckBox("Complexity");
      chckbx_counting_Complexity.setToolTipText("");
      chckbx_counting_Complexity.setSelected(true);
      chckbx_counting_Complexity.setBounds(36, 47, 128, 23);

      GridBagConstraints s = new GridBagConstraints();
      s.fill = GridBagConstraints.BOTH;
      s.ipadx = 10;
      s.ipady = 10;
      s.insets = new Insets(10, 10, 10, 10);
      s.gridx = 0;
      s.gridy = 0;
      inner_panel_count.add(chckbx_counting_Complexity, s);

      // Duplicate check box
      chckbx_counting_DuplicateFileChecking = new JCheckBox("Duplicate File Checking");
      chckbx_counting_DuplicateFileChecking.setSelected(true);
      chckbx_counting_DuplicateFileChecking.setBounds(36, 87, 192, 23);
      s.gridx = 0;
      s.gridy = 1;
      inner_panel_count.add(chckbx_counting_DuplicateFileChecking, s);

      // Follow check box
      chckbx_counting_FollowSymbolicLinks = new JCheckBox("Follow Symbolic Links");
      chckbx_counting_FollowSymbolicLinks.setSelected(true);
      chckbx_counting_FollowSymbolicLinks.setBounds(36, 127, 192, 23);
      s.gridx = 0;
      s.gridy = 2;
      inner_panel_count.add(chckbx_counting_FollowSymbolicLinks, s);

      // Translate check box
      chckbx_counting_TranslateClearcaseFilenames = new JCheckBox("Translate ClearCase Filenames");
      chckbx_counting_TranslateClearcaseFilenames.setBounds(36, 166, 223, 23);
      s.gridx = 0;
      s.gridy = 3;
      inner_panel_count.add(chckbx_counting_TranslateClearcaseFilenames, s);

      // reset button
      button_counting_Reset = new JButton("Reset Default");
      button_counting_Reset.setBounds(36, 211, 138, 37);
      s.gridx = 0;
      s.gridy = 4;
      s.fill = GridBagConstraints.NONE;
      s.anchor = GridBagConstraints.WEST;
      inner_panel_count.add(button_counting_Reset, s);
      s.fill = GridBagConstraints.BOTH;
      button_counting_Reset.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            countingReset();

         }
      });

      // function level check box
      // chckbx_counting_FunctionLevel = new JCheckBox("Function Level");
      // chckbx_counting_FunctionLevel.setSelected(true);
      // chckbx_counting_FunctionLevel.setBounds(36, 87, 192, 23);
      // s.gridx = 2;
      // s.gridy = 2;
      // inner_panel_count.add(chckbx_counting_FunctionLevel, s);

      // Input Directory radio button
      rdbtn_counting_InputDirectory = new JRadioButton("Input Directory");
      rdbtn_counting_InputDirectory.setSelected(true);
      rdbtn_counting_InputDirectory.setBounds(287, 47, 141, 23);
      s.gridx = 1;
      s.gridy = 0;
      inner_panel_count.add(rdbtn_counting_InputDirectory, s);

      // File list radio button
      rdbtn_counting_FileList = new JRadioButton("File List");
      rdbtn_counting_FileList.setBounds(441, 47, 141, 23);
      s.gridx = 2;
      s.gridy = 0;
      inner_panel_count.add(rdbtn_counting_FileList, s);

      // Button group
      ButtonGroup group_counting_Input = new ButtonGroup();
      group_counting_Input.add(rdbtn_counting_InputDirectory);
      group_counting_Input.add(rdbtn_counting_FileList);

      // File input texfiled
      textField_counting_FileInput = new JTextField();
      textField_counting_FileInput.setEditable(false);
      textField_counting_FileInput.setBounds(297, 86, 318, 37);
      s.gridx = 1;
      s.gridy = 1;
      s.weightx = 1;
      s.gridwidth = 2;
      textField_counting_FileInput.setColumns(10);

      inner_panel_count.add(textField_counting_FileInput, s);
      s.weightx = 0;
      s.gridwidth = 1;

      // drag&drop listener create 2019/06
      DropTargetListener listener_counting_FileInput = new DropTargetListenerImpl(textField_counting_FileInput);
      DropTarget dropTarget_counting_FileInput = new DropTarget(textField_counting_FileInput,
               DnDConstants.ACTION_COPY_OR_MOVE, listener_counting_FileInput, true);

      // File input button
      btnBrowse_counting_FileInput = new JButton("...");
      btnBrowse_counting_FileInput.setBounds(627, 87, 30, 37);
      s.gridx = 3;
      s.gridy = 1;
      inner_panel_count.add(btnBrowse_counting_FileInput, s);
      btnBrowse_counting_FileInput.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            showFileOpenDialog(frame, textField_counting_FileInput, rdbtn_counting_InputDirectory.isSelected());
         }
      });

      // Output directory label
      lbl_counting_OutputDirectory = new JLabel("Output Directory");
      lbl_counting_OutputDirectory.setBounds(303, 147, 125, 16);
      s.gridx = 1;
      s.gridy = 2;
      inner_panel_count.add(lbl_counting_OutputDirectory, s);

      // output directory input textfield
      textField_counting_FileOutput = new JTextField();
      textField_counting_FileOutput.setEditable(false);
      textField_counting_FileOutput.setColumns(10);
      textField_counting_FileOutput.setBounds(297, 177, 318, 37);
      s.gridx = 1;
      s.gridy = 3;
      s.gridwidth = 2;
      s.weightx = 1;
      inner_panel_count.add(textField_counting_FileOutput, s);
      s.weightx = 0;
      s.gridwidth = 1;

      // drag&drop listener create 2019/06
      DropTargetListener listener_counting_FileOutput = new DropTargetListenerImpl(textField_counting_FileOutput);
      DropTarget dropTarget_counting_FileOutput = new DropTarget(textField_counting_FileOutput,
               DnDConstants.ACTION_COPY_OR_MOVE, listener_counting_FileOutput, true);

      // File output button
      btnBrowse_counting_FileOutput = new JButton("...");
      btnBrowse_counting_FileOutput.setBounds(627, 178, 30, 37);
      s.gridx = 3;
      s.gridy = 3;
      inner_panel_count.add(btnBrowse_counting_FileOutput, s);
      btnBrowse_counting_FileOutput.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            showFileOpenDialog(frame, textField_counting_FileOutput, true);
         }
      });

      // blank block make the layout reasonable
      s.weightx = 0;
      s.weighty = 1;
      s.gridwidth = 0;
      s.gridy = 5;
      inner_panel_count.add(new JLabel(""), s);
   }

   /**
    * Initialize the contents of the differencing panel.
    */
   private void initialize_DifferencingPanel()
   {
      // Basic differencing panel: inner_panel_diff

      // 4 check boxes
      // Complexity check box
      chckbx_differencing_Complexity = new JCheckBox("Complexity");
      chckbx_differencing_Complexity.setSelected(true);
      chckbx_differencing_Complexity.setBounds(36, 47, 128, 23);
      GridBagConstraints s = new GridBagConstraints();
      s.fill = GridBagConstraints.BOTH;
      s.ipadx = 10;
      s.ipady = 10;
      s.insets = new Insets(10, 10, 10, 10);
      s.gridx = 0;
      s.gridy = 0;
      inner_panel_diff.add(chckbx_differencing_Complexity, s);

      // Duplicate check box
      chckbx_differencing_DuplicateFileChecking = new JCheckBox("Duplicate File Checking");
      chckbx_differencing_DuplicateFileChecking.setSelected(true);
      chckbx_differencing_DuplicateFileChecking.setBounds(36, 87, 192, 23);
      s.gridx = 0;
      s.gridy = 1;
      inner_panel_diff.add(chckbx_differencing_DuplicateFileChecking, s);

      // Follow check box
      chckbx_differencing_FollowSymbolicLinks = new JCheckBox("Follow Symbolic Links");
      chckbx_differencing_FollowSymbolicLinks.setSelected(true);
      chckbx_differencing_FollowSymbolicLinks.setBounds(36, 127, 192, 23);
      s.gridx = 0;
      s.gridy = 2;
      inner_panel_diff.add(chckbx_differencing_FollowSymbolicLinks, s);

      // Translate check box
      chckbx_differencing_TranslateClearcaseFilenames = new JCheckBox("Translate ClearCase Filenames");
      chckbx_differencing_TranslateClearcaseFilenames.setBounds(36, 166, 223, 23);
      s.gridx = 0;
      s.gridy = 3;
      inner_panel_diff.add(chckbx_differencing_TranslateClearcaseFilenames, s);

      // function level check box
      // chckbx_differencing_FunctionLevel = new JCheckBox("Function Level");
      // chckbx_differencing_FunctionLevel.setSelected(true);
      // chckbx_differencing_FunctionLevel.setBounds(36, 87, 192, 23);
      // s.gridx = 2;
      // s.gridy = 3;
      // inner_panel_diff.add(chckbx_differencing_FunctionLevel, s);

      // reset button
      button_differencing_Rest = new JButton("Reset Default");
      button_differencing_Rest.setBounds(36, 211, 138, 37);
      s.gridx = 0;
      s.gridy = 4;
      s.fill = GridBagConstraints.NONE;
      s.anchor = GridBagConstraints.WEST;
      inner_panel_diff.add(button_differencing_Rest, s);
      s.fill = GridBagConstraints.BOTH;
      button_differencing_Rest.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            differencingReset();

         }
      });

      // Input Directory radio button
      rdbtn_differencing_InputDirectory = new JRadioButton("Input Directory");
      rdbtn_differencing_InputDirectory.setSelected(true);
      rdbtn_differencing_InputDirectory.setBounds(287, 47, 141, 23);
      s.gridx = 1;
      s.gridy = 0;
      inner_panel_diff.add(rdbtn_differencing_InputDirectory, s);

      // File list radio button
      rdbtn_differencing_FileList = new JRadioButton("File List");
      rdbtn_differencing_FileList.setBounds(441, 47, 141, 23);
      s.gridx = 2;
      s.gridy = 0;
      inner_panel_diff.add(rdbtn_differencing_FileList, s);

      // Button froup
      ButtonGroup group_differencing_InputA = new ButtonGroup();
      group_differencing_InputA.add(rdbtn_differencing_InputDirectory);
      group_differencing_InputA.add(rdbtn_differencing_FileList);

      // base A file input textfield
      textField_differencing_FileInputA = new JTextField();
      textField_differencing_FileInputA.setEditable(false);
      textField_differencing_FileInputA
               .addFocusListener(new JTextFieldHintListener("BaseLine A", textField_differencing_FileInputA));
      textField_differencing_FileInputA.setBounds(297, 86, 318, 37);
      textField_differencing_FileInputA.setColumns(10);
      s.gridx = 1;
      s.gridy = 1;
      s.weightx = 1;
      s.gridwidth = 2;
      inner_panel_diff.add(textField_differencing_FileInputA, s);
      s.gridwidth = 1;
      s.weightx = 0;

      // drag&drop listener create 2019/06
      DropTargetListener listener_differencing_FileInputA =
               new DropTargetListenerImpl(textField_differencing_FileInputA);
      DropTarget dropTarget_differencing_FileInputA = new DropTarget(textField_differencing_FileInputA,
               DnDConstants.ACTION_COPY_OR_MOVE, listener_differencing_FileInputA, true);

      // base A file input button
      btnBrowse_differencing_FileInputA = new JButton("...");
      btnBrowse_differencing_FileInputA.setBounds(627, 87, 30, 37);
      s.gridx = 3;
      s.gridy = 1;
      inner_panel_diff.add(btnBrowse_differencing_FileInputA, s);
      btnBrowse_differencing_FileInputA.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            showFileOpenDialog(frame, textField_differencing_FileInputA,
                     rdbtn_differencing_InputDirectory.isSelected());
         }
      });

      // output directory input textfield
      lbl_differencing_OutputDirectory = new JLabel("Output Directory");
      lbl_differencing_OutputDirectory.setBounds(303, 211, 125, 16);
      s.gridx = 1;
      s.gridy = 3;
      inner_panel_diff.add(lbl_differencing_OutputDirectory, s);
      textField_differencing_FileOutput = new JTextField();
      textField_differencing_FileOutput.setEditable(false);
      textField_differencing_FileOutput.setColumns(10);
      textField_differencing_FileOutput.setBounds(297, 239, 318, 37);
      s.gridx = 1;
      s.gridy = 4;
      s.weightx = 1;
      s.gridwidth = 2;
      inner_panel_diff.add(textField_differencing_FileOutput, s);
      s.weightx = 0;
      s.gridwidth = 1;

      // drag&drop listener create 2019/06
      DropTargetListener listener_differencing_FileOutput =
               new DropTargetListenerImpl(textField_differencing_FileOutput);
      DropTarget dropTarget_differencing_FileOutput = new DropTarget(textField_differencing_FileOutput,
               DnDConstants.ACTION_COPY_OR_MOVE, listener_differencing_FileOutput, true);

      // output button
      btnBrowse_differencing_FileOutput = new JButton("...");
      btnBrowse_differencing_FileOutput.setBounds(627, 239, 30, 37);
      s.gridx = 3;
      s.gridy = 4;
      inner_panel_diff.add(btnBrowse_differencing_FileOutput, s);
      btnBrowse_differencing_FileOutput.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            showFileOpenDialog(frame, textField_differencing_FileOutput, true);
         }
      });

      ButtonGroup group_differencing_InputB = new ButtonGroup();

      // base line B file input textfield
      textField_differencing_FileInputB = new JTextField();
      textField_differencing_FileInputB.setEditable(false);
      textField_differencing_FileInputB
               .addFocusListener(new JTextFieldHintListener("BaseLine B", textField_differencing_FileInputB));
      textField_differencing_FileInputB.setColumns(10);
      textField_differencing_FileInputB.setBounds(297, 149, 318, 37);
      s.gridx = 1;
      s.gridy = 2;
      s.gridwidth = 2;
      s.weightx = 1;
      inner_panel_diff.add(textField_differencing_FileInputB, s);
      s.gridwidth = 1;
      s.weightx = 0;

      // drag&drop listener create 2019/06
      DropTargetListener listener_differencing_FileInputB =
               new DropTargetListenerImpl(textField_differencing_FileInputB);
      DropTarget dropTarget_differencing_FileInputB = new DropTarget(textField_differencing_FileInputB,
               DnDConstants.ACTION_COPY_OR_MOVE, listener_differencing_FileInputB, true);

      // base line B button
      btnBrowse_differencing_FileInputB = new JButton("...");
      btnBrowse_differencing_FileInputB.setBounds(627, 149, 30, 37);
      s.gridx = 3;
      s.gridy = 2;
      inner_panel_diff.add(btnBrowse_differencing_FileInputB, s);
      btnBrowse_differencing_FileInputB.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            showFileOpenDialog(frame, textField_differencing_FileInputB,
                     rdbtn_differencing_InputDirectory.isSelected());
         }
      });

      s.weightx = 0;
      s.weighty = 1;
      s.gridwidth = 0;
      s.gridy = 5;
      inner_panel_diff.add(new JLabel(""), s);
   }

   /**
    * Initialize the contents of the processing panel.
    */
   private void initialize_ProcessingPanel()
   {
      // processing panel: inner_panel_processing
      // Unified check box
      chckbx_processing_UnifiedOutput = new JCheckBox("Unified Output");
      chckbx_processing_UnifiedOutput.setToolTipText("");
      chckbx_processing_UnifiedOutput.setBounds(32, 57, 128, 23);
      GridBagConstraints s = new GridBagConstraints();
      s.fill = GridBagConstraints.BOTH;
      s.ipadx = 10;
      s.ipady = 10;
      s.insets = new Insets(10, 10, 10, 10);
      s.gridx = 0;
      s.gridy = 0;
      inner_panel_processing.add(chckbx_processing_UnifiedOutput, s);

      // ASCII check box
      chckbx_processing_AsciiOutputFormat = new JCheckBox("ASCII Output Format");
      chckbx_processing_AsciiOutputFormat.setBounds(32, 104, 161, 23);
      s.gridx = 0;
      s.gridy = 1;
      inner_panel_processing.add(chckbx_processing_AsciiOutputFormat, s);

      // Modification threshold
      SpinnerModel processing_modification_model = new SpinnerNumberModel(60.0, 0.0, 100.0, 0.1);
      spinner_processing_Modification = new JSpinner(processing_modification_model);
      spinner_processing_Modification.setBounds(505, 56, 95, 26);
      s.gridx = 2;
      s.gridy = 0;
      inner_panel_processing.add(spinner_processing_Modification, s);

      lblModificationThreshold = new JLabel("Modification Threshold %");
      lblModificationThreshold.setBounds(291, 61, 169, 16);
      s.gridx = 1;
      s.gridy = 0;
      inner_panel_processing.add(lblModificationThreshold, s);

      // Duplicate threshold
      lblDuplicateThreshold = new JLabel("Duplicate Threshold %");
      lblDuplicateThreshold.setBounds(291, 104, 169, 16);
      s.gridx = 1;
      s.gridy = 1;
      inner_panel_processing.add(lblDuplicateThreshold, s);

      SpinnerModel processing_duplicate_model = new SpinnerNumberModel(100.0, 0.0, 100.0, 0.1);
      spinner_processing_Duplicate = new JSpinner(processing_duplicate_model);
      spinner_processing_Duplicate.setBounds(505, 99, 95, 26);
      s.gridx = 2;
      s.gridy = 1;
      inner_panel_processing.add(spinner_processing_Duplicate, s);

      // LSLOC Truncation Threshold
      lblLslocTruncationThreshold = new JLabel("LSLOC Truncation Threshold %");
      lblLslocTruncationThreshold.setBounds(291, 146, 202, 16);
      s.gridx = 1;
      s.gridy = 2;
      inner_panel_processing.add(lblLslocTruncationThreshold, s);

      SpinnerModel processing_lsloc_model = new SpinnerNumberModel(10000, 0, 10000, 1);
      spinner_processing_LSLOC = new JSpinner(processing_lsloc_model);
      spinner_processing_LSLOC.setValue(10000);
      spinner_processing_LSLOC.setBounds(505, 141, 95, 26);
      s.gridx = 2;
      s.gridy = 2;
      inner_panel_processing.add(spinner_processing_LSLOC, s);

      // Log Reporting Level
      lblLogReportingLevel = new JLabel("Log Reporting Level %");
      lblLogReportingLevel.setBounds(291, 187, 202, 16);
      s.gridx = 1;
      s.gridy = 3;
      inner_panel_processing.add(lblLogReportingLevel, s);

      String[] logStrings = { "ERROR", "FATAL", "WARN", "INFO", "DEBUG", "TRACE" };
      comboBox_processing_Log = new JComboBox(logStrings);
      comboBox_processing_Log.setBounds(505, 182, 105, 26);
      s.gridx = 2;
      s.gridy = 3;
      inner_panel_processing.add(comboBox_processing_Log, s);

      // Reset button
      button_processing_Reset = new JButton("Reset Default");
      button_processing_Reset.setBounds(20, 274, 138, 37);
      s.gridx = 0;
      s.gridy = 3;
      s.weightx = 1;
      s.fill = GridBagConstraints.NONE;
      s.anchor = GridBagConstraints.WEST;
      inner_panel_processing.add(button_processing_Reset, s);
      button_processing_Reset.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            processingReset();

         }
      });

      // blank block make the layout reasonable
      s.weightx = 0;
      s.weighty = 1;
      s.gridwidth = 0;
      s.gridy = 4;
      inner_panel_processing.add(new JLabel(""), s);
   }

   /**
    * Set default in processing panel
    */
   private void processingReset()
   {
      chckbx_processing_UnifiedOutput.setSelected(false);
      chckbx_processing_AsciiOutputFormat.setSelected(false);
      spinner_processing_Modification.setValue(60);
      spinner_processing_Duplicate.setValue(100);
      spinner_processing_LSLOC.setValue(10000);
      comboBox_processing_Log.setSelectedIndex(0);
   }

   /**
    * Set default in language panel
    */
   private void languageReset()
   {
      textField_language_FileExtensionMapping.setText(null);
      textField_CustomLanguageProperties.setText(null);
      comboBox_language_SelectLanguageForExport.setSelectedIndex(0);
   }

   /**
    * Initialize the contents of the Language panel.
    */
   private void initialize_LanguagePanel()
   {

      // language panel: inner_panel_language
      // language's all the components here
      JLabel lblNewLabel_1 = new JLabel("File Extension Mapping File");
      lblNewLabel_1.setBounds(230, 43, 198, 16);
      GridBagConstraints s = new GridBagConstraints();
      s.fill = GridBagConstraints.BOTH;
      s.ipadx = 10;
      s.ipady = 10;
      s.insets = new Insets(10, 10, 10, 10);
      s.gridx = 1;
      s.gridy = 0;
      inner_panel_language.add(lblNewLabel_1, s);

      textField_language_FileExtensionMapping = new JTextField();
      textField_language_FileExtensionMapping.setBounds(224, 71, 350, 38);
      s.gridx = 1;
      s.gridy = 1;
      s.weightx = 1;
      s.gridwidth = 2;
      inner_panel_language.add(textField_language_FileExtensionMapping, s);
      s.weightx = 0;
      s.gridwidth = 1;
      textField_language_FileExtensionMapping.setColumns(10);

      DropTargetListener listener_language_FileExtensionMapping =
               new DropTargetListenerImpl(textField_language_FileExtensionMapping);
      DropTarget dropTarget_language_FileExtensionMapping = new DropTarget(textField_language_FileExtensionMapping,
               DnDConstants.ACTION_COPY_OR_MOVE, listener_language_FileExtensionMapping, true);

      btnBrowse_language_FileExtensionMapping = new JButton("...");
      btnBrowse_language_FileExtensionMapping.setBounds(586, 72, 30, 38);
      s.gridx = 3;
      s.gridy = 1;
      inner_panel_language.add(btnBrowse_language_FileExtensionMapping, s);

      // (dyw)file extension browse
      btnBrowse_language_FileExtensionMapping.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            showFileOpenDialog(frame, textField_language_FileExtensionMapping, false);
         }
      });

      JLabel lblNewLabel_2 = new JLabel("Use Custom Language Properties");
      lblNewLabel_2.setBounds(224, 128, 247, 23);
      s.gridx = 1;
      s.gridy = 2;
      inner_panel_language.add(lblNewLabel_2, s);

      textField_CustomLanguageProperties = new JTextField();
      textField_CustomLanguageProperties.setColumns(10);
      textField_CustomLanguageProperties.setBounds(224, 163, 350, 38);
      s.gridx = 1;
      s.gridy = 3;
      s.gridwidth = 2;
      s.weightx = 1;
      inner_panel_language.add(textField_CustomLanguageProperties, s);
      s.gridwidth = 1;
      s.weightx = 0;

      DropTargetListener listener_CustomLanguageProperties =
               new DropTargetListenerImpl(textField_CustomLanguageProperties);
      DropTarget dropTarget_CustomLanguageProperties = new DropTarget(textField_CustomLanguageProperties,
               DnDConstants.ACTION_COPY_OR_MOVE, listener_CustomLanguageProperties, true);

      btnBrowse_language_CustomLanguageProperties = new JButton("...");
      btnBrowse_language_CustomLanguageProperties.setBounds(586, 164, 30, 38);
      s.gridx = 3;
      s.gridy = 3;
      inner_panel_language.add(btnBrowse_language_CustomLanguageProperties, s);

      // (dyw)language property browse
      btnBrowse_language_CustomLanguageProperties.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            showFileOpenDialog(frame, textField_CustomLanguageProperties, false);
         }
      });

      lblSelectLanguageForExport = new JLabel("Select Language For Export");
      lblSelectLanguageForExport.setBounds(230, 220, 198, 16);
      s.gridx = 1;
      s.gridy = 4;
      inner_panel_language.add(lblSelectLanguageForExport, s);
      String[] petStrings = { "--", "ADA", "ASP", "ASSEMBLY", "BASH", "HTML", "IDL", "JAVA", "JAVASCRIPT", "RUBY",
               "SCALA", "SQL", "VB" };
      comboBox_language_SelectLanguageForExport = new JComboBox(petStrings);
      comboBox_language_SelectLanguageForExport.setBounds(224, 248, 263, 27);
      comboBox_language_SelectLanguageForExport.setSelectedItem("--");
      s.gridx = 1;
      s.gridy = 5;
      inner_panel_language.add(comboBox_language_SelectLanguageForExport, s);

      button_language_Export = new JButton("Export");
      button_language_Export.setBounds(499, 242, 117, 38);
      buttonListener exportListerner = new buttonListener();
      button_language_Export.addActionListener(exportListerner);
      s.gridx = 2;
      s.gridy = 5;
      s.fill = GridBagConstraints.NONE;
      s.anchor = GridBagConstraints.WEST;
      inner_panel_language.add(button_language_Export, s);

      button_language_Reset = new JButton("Reset Default");
      button_language_Reset.setBounds(20, 274, 138, 37);
      s.gridx = 0;
      s.gridy = 5;
      s.fill = GridBagConstraints.NONE;
      s.anchor = GridBagConstraints.WEST;
      s.weightx = 1;
      inner_panel_language.add(button_language_Reset, s);
      // reset the language panel
      button_language_Reset.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            languageReset();

         }
      });

      // blank block make the layout reasonable
      s.weightx = 0;
      s.weighty = 1;
      s.gridwidth = 0;
      s.gridy = 6;
      inner_panel_language.add(new JLabel(""), s);
   }

   /**
    * Initialize the contents of the command line panel.
    */
   private void initialize_DownPanel()
   {
      // right down panel: downpane
      // command line output
      textAreaCommandLine = new JTextArea();
      textAreaCommandLine.setEditable(false);
      textAreaCommandLine.setWrapStyleWord(true);
      textAreaCommandLine.setLineWrap(true);
      textAreaCommandLine.setBorder(BorderFactory.createCompoundBorder(textAreaCommandLine.getBorder(),
               BorderFactory.createEmptyBorder(2, 2, 2, 2)));

      textAreaCommandLine.setText("");

      scrollPane = new JScrollPane(textAreaCommandLine, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
               ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
      scrollPane.setBounds(6, 0, 702, 160);
      scrollPane.setBorder(new LineBorder(Color.DARK_GRAY, 1));
      GridBagConstraints s = new GridBagConstraints();
      s.fill = GridBagConstraints.BOTH;
      s.gridx = 0;
      s.gridy = 0;
      s.weightx = 0;
      s.weighty = 1;
      s.gridwidth = 3;
      s.insets = new Insets(10, 10, 10, 10);
      downpane.add(scrollPane, s);
      s.weightx = 0;
      s.weighty = 0;
      s.gridwidth = 1;

      // start button
      btnStart = new JButton("Start");
      // try {
      // Image img = ImageIO.read(getClass().getResource("/image/start.png"));
      // Image dimg = img.getScaledInstance(12, 12,Image.SCALE_SMOOTH);
      // btnStart.setIcon(new ImageIcon(dimg));
      // } catch (Exception ex) {
      // System.out.println(ex);
      // }

      btnStart.setBounds(571, 175, 117, 34);
      buttonListener startListerner = new buttonListener();
      btnStart.addActionListener(startListerner);
      s.gridx = 2;
      s.gridy = 1;
      downpane.add(btnStart, s);
      btnStart.setFont(new Font("Lucida Grande", Font.BOLD, 15));

      // stop button
      btnStop = new JButton("Stop");
      // try {
      // Image img2 = ImageIO.read(getClass().getResource("/image/stop.png"));
      // Image dimg2 = img2.getScaledInstance(12, 12,Image.SCALE_SMOOTH);
      // btnStop.setIcon(new ImageIcon(dimg2));
      // } catch (Exception ex) {
      // System.out.println(ex);
      // }
      btnStop.setBounds(435, 175, 117, 34);
      s.gridx = 1;
      s.gridy = 1;
      downpane.add(btnStop, s);
      btnStop.setEnabled(false);
      btnStop.setFont(new Font("Lucida Grande", Font.BOLD, 15));
      buttonListener stopListerner = new buttonListener();
      btnStop.addActionListener(stopListerner);

      s.gridx = 0;
      s.gridy = 1;
      s.weightx = 1;
      downpane.add(new JLabel(""), s);
   }

   /**
    * Initialize the contents of the left menu panel.
    */
   private void initialize_LeftPanel()
   {
      // left panel:leftpane
      // drop-down list, counting, differencing
      comboBox_CountingOrDifferencing = new JComboBox();
      comboBox_CountingOrDifferencing.setFont(new Font("Lucida Grande", Font.PLAIN, 17));
      comboBox_CountingOrDifferencing.setModel(new DefaultComboBoxModel(new String[] { "Counting", "Differencing" }));
      comboBox_CountingOrDifferencing.setBounds(14, 30, 158, 34);
      leftpane.add(comboBox_CountingOrDifferencing);
      // add actionlistner to listen for change
      comboBox_CountingOrDifferencing.addActionListener(new ActionListener()
      {
         @Override
         public void actionPerformed(ActionEvent e)
         {
            // lblBasic.setBackground(UIManager.getColor("List.selectionInactiveBackground"));
            lblBasic.setBackground(Color.lightGray);
            lblProcessing.setBackground(null);
            lblLanguage.setBackground(null);
            // get the selected item
            String s = (String) comboBox_CountingOrDifferencing.getSelectedItem();
            // check for a match
            switch (s)
            {
               case "Counting":
                  isCounting = true;
                  CardLayout c = (CardLayout) (uppane.getLayout());
                  c.show(uppane, "panel_basic_count");
                  differencingReset();
                  break;
               case "Differencing":
                  isCounting = false;
                  CardLayout c2 = (CardLayout) (uppane.getLayout());
                  c2.show(uppane, "panel_basic_differencing");
                  countingReset();
                  break;

            }
         }
      });

      // basic setting panel
      lblBasic = new JLabel("  Basic");
      // lblBasic.setOpaque(true);
      // lblBasic.setBackground(UIManager.getColor("List.selectionInactiveBackground"));
      lblBasic.setBackground(Color.lightGray);
      lblBasic.setOpaque(true);
      lblBasic.setFont(new Font("Arial", Font.PLAIN, 15));
      lblBasic.setBounds(22, 76, 142, 25);
      leftpane.add(lblBasic);
      // event listener for checkbox
      lblBasic.addMouseListener(new MouseAdapter()
      {
         @Override
         // changed from mouseClicked 2019/07
         public void mouseReleased(MouseEvent e)
         {
            String to_show_panel = "";
            String s = (String) comboBox_CountingOrDifferencing.getSelectedItem();// get
                                                                                  // the
                                                                                  // selected
                                                                                  // item
            switch (s)
            {// check for a match
               case "Counting":
                  to_show_panel = "panel_basic_count";
                  break;
               case "Differencing":
                  to_show_panel = "panel_basic_differencing";
                  break;

            }
            // lblBasic.setBackground(UIManager.getColor("List.selectionInactiveBackground"));
            lblBasic.setBackground(Color.lightGray);
            lblProcessing.setBackground(null);
            lblLanguage.setBackground(null);
            CardLayout c = (CardLayout) (uppane.getLayout());
            c.show(uppane, to_show_panel);
         }
      });

      try
      {
         Image imgdown = ImageIO.read(getClass().getResource("image/down.png"));
         Image dimgdown = imgdown.getScaledInstance(12, 12, Image.SCALE_SMOOTH);
         ImageIcon icondown = new ImageIcon(dimgdown);

         Image imgright = ImageIO.read(getClass().getResource("image/right.png"));
         Image dimgright = imgright.getScaledInstance(12, 12, Image.SCALE_SMOOTH);
         ImageIcon iconright = new ImageIcon(dimgright);

         // advanced setting panel
         lblAdvanced = new JLabel(" Advanced");
         lblAdvanced.setIcon(iconright);

         lblAdvanced.setFont(new Font("Arial", Font.PLAIN, 15));
         lblAdvanced.setBounds(10, 110, 142, 25);
         leftpane.add(lblAdvanced);
         // listener for the advanced button
         lblAdvanced.addMouseListener(new MouseAdapter()
         {
            @Override
            // changed from mouseClicked 2019/07
            public void mouseReleased(MouseEvent e)
            {
               String previousStr = lblAdvanced.getText();
               // System.out.println(lblLanguage.isVisible());
               // if(previousStr.equals(" 鈻� Advanced")){
               if (lblLanguage.isVisible())
               {
                  lblAdvanced.setIcon(iconright);
                  lblLanguage.setVisible(false);
                  lblProcessing.setVisible(false);
               }
               else
               {
                  lblAdvanced.setIcon(icondown);
                  lblLanguage.setVisible(true);
                  lblProcessing.setVisible(true);
               }
            }
         });

      }
      catch (Exception ex)
      {
         System.out.println(ex);
      }
      // language setting panel
      lblLanguage = new JLabel("       Language");
      lblLanguage.setFont(new Font("Arial", Font.PLAIN, 15));
      lblLanguage.setBounds(19, 138, 142, 25);
      lblLanguage.setVisible(false);
      lblLanguage.setOpaque(true);
      leftpane.add(lblLanguage);
      // listener for language button
      lblLanguage.addMouseListener(new MouseAdapter()
      {
         @Override
         // changed from mouseClicked 2019/07
         public void mouseReleased(MouseEvent e)
         {
            lblBasic.setBackground(null);
            lblProcessing.setBackground(null);
            // lblLanguage.setBackground(UIManager.getColor("List.selectionInactiveBackground"));
            lblLanguage.setBackground(Color.lightGray);
            CardLayout c = (CardLayout) (uppane.getLayout());
            c.show(uppane, "panel_advanced_language");
         }
      });

      // processing setting panel
      lblProcessing = new JLabel("       Processing");
      lblProcessing.setFont(new Font("Arial", Font.PLAIN, 15));
      lblProcessing.setBounds(19, 167, 142, 25);
      lblProcessing.setVisible(false);
      lblProcessing.setOpaque(true);
      leftpane.add(lblProcessing);
      // listener for processing button
      lblProcessing.addMouseListener(new MouseAdapter()
      {
         @Override
         // changed from mouseClicked 2019/07
         public void mouseReleased(MouseEvent e)
         {
            lblBasic.setBackground(null);
            lblLanguage.setBackground(null);
            // lblProcessing.setBackground(UIManager.getColor("List.selectionInactiveBackground"));
            lblProcessing.setBackground(Color.lightGray);
            CardLayout c = (CardLayout) (uppane.getLayout());
            c.show(uppane, "panel_advanced_processing");
         }
      });

   }

   /**
    * Initialize the contents of the base panel. SplitPane left & right, up &
    * down.
    */
   private void initialize_Panel()
   {
      // create a splitPane锛宒ivide into leftpane and rightpane
      // ----------------------------------
      leftpane = new JPanel();
      rightpane = new JPanel();
      splitpane_leftRight = new JSplitPane(SwingConstants.VERTICAL, leftpane, rightpane);
      splitpane_leftRight.setEnabled(false);
      splitpane_leftRight.setBackground(new Color(238, 238, 238));
      splitpane_leftRight.setResizeWeight(0);
      splitpane_leftRight.setOrientation(SwingConstants.VERTICAL);
      splitpane_leftRight.setDividerSize(0);
      rightpane.setLayout(new GridLayout());
      leftpane.setLayout(null);
      // ----------------------------------

      // divide rightpane into uppane and downpane
      // ----------------------------------
      uppane = new JPanel();
      downpane = new JPanel();
      splitPane_upDown = new JSplitPane(SwingConstants.HORIZONTAL, uppane, downpane);
      splitPane_upDown.setBackground(new Color(238, 238, 238));
      splitPane_upDown.setEnabled(false);
      splitPane_upDown.setBorder(null);
      splitPane_upDown.setDividerSize(0);
      splitPane_upDown.setResizeWeight(0);
      splitPane_upDown.setBounds(0, 0, 718, 597);
      rightpane.add(splitPane_upDown);
      uppane.setLayout(new CardLayout(0, 0));
      downpane.setLayout(new GridBagLayout());
      // ----------------------------------

      frame.addComponentListener(new ComponentAdapter()
      {
         public void componentResized(ComponentEvent e)
         {
            splitpane_leftRight.setDividerLocation(0.2);
            splitPane_upDown.setDividerLocation(0.63);
         }
      });

   }

   /**
    * Initialize the contents of the base right & up panel, 4 cards and their
    * inner panels.
    */
   private void initialize_Uppane_panel()
   {
      // uppane has cardLayout, 4 cards: panel_basic_count,
      // panel_basic_differencing,
      // panel_advanced_language,panel_advanced_processing
      // ----------------------------------
      panel_basic_count = new JPanel();
      panel_basic_count.setBackground(new Color(238, 238, 238));
      uppane.add(panel_basic_count, "panel_basic_count");

      panel_basic_differencing = new JPanel();
      panel_basic_differencing.setBackground(new Color(238, 238, 238));
      uppane.add(panel_basic_differencing, "panel_basic_differencing");

      panel_advanced_language = new JPanel();
      panel_advanced_language.setBackground(new Color(238, 238, 238));
      uppane.add(panel_advanced_language, "panel_advanced_language");

      panel_advanced_processing = new JPanel();
      panel_advanced_processing.setBackground(new Color(238, 238, 238));
      uppane.add(panel_advanced_processing, "panel_advanced_processing");

      panel_basic_count.setLayout(new GridBagLayout());
      panel_basic_differencing.setLayout(new GridBagLayout());
      panel_advanced_language.setLayout(new GridBagLayout());
      panel_advanced_processing.setLayout(new GridBagLayout());
      // ----------------------------------

      // ----------------------------------
      // 4 cards: panel_basic_count, panel_basic_differencing,
      // panel_advanced_language,panel_advanced_processing
      // add one new panel for each card: inner_panel_count, inner_panel_diff,
      // inner_panel_language, inner_panel_processing
      inner_panel_count = new JPanel();
      inner_panel_count.setBorder(new LineBorder(Color.darkGray, 1));
      // inner_panel_count.setPreferredSize(new Dimension(700, 330));
      // inner_panel_count.setBounds(6, 22, 700, 330);
      GridBagConstraints s = new GridBagConstraints();
      s.fill = GridBagConstraints.BOTH;
      s.weightx = 1;
      s.weighty = 1;
      s.insets = new Insets(10, 10, 10, 10);
      panel_basic_count.add(inner_panel_count, s);

      inner_panel_diff = new JPanel();
      inner_panel_diff.setBorder(new LineBorder(new Color(0, 0, 0)));
      inner_panel_diff.setBounds(6, 22, 700, 330);
      s = new GridBagConstraints();
      s.fill = GridBagConstraints.BOTH;
      s.weightx = 1;
      s.weighty = 1;
      s.insets = new Insets(10, 10, 10, 10);
      panel_basic_differencing.add(inner_panel_diff, s);

      inner_panel_language = new JPanel();
      inner_panel_language.setBorder(new LineBorder(new Color(0, 0, 0)));
      inner_panel_language.setBounds(6, 22, 700, 330);
      s = new GridBagConstraints();
      s.fill = GridBagConstraints.BOTH;
      s.weightx = 1;
      s.weighty = 1;
      s.insets = new Insets(10, 10, 10, 10);
      panel_advanced_language.add(inner_panel_language, s);

      inner_panel_processing = new JPanel();
      inner_panel_processing.setBorder(new LineBorder(new Color(0, 0, 0)));
      inner_panel_processing.setBounds(6, 22, 700, 330);
      s = new GridBagConstraints();
      s.fill = GridBagConstraints.BOTH;
      s.weightx = 1;
      s.weighty = 1;
      s.insets = new Insets(10, 10, 10, 10);
      panel_advanced_processing.add(inner_panel_processing, s);

      inner_panel_count.setLayout(new GridBagLayout());
      inner_panel_diff.setLayout(new GridBagLayout());
      inner_panel_language.setLayout(new GridBagLayout());
      inner_panel_processing.setLayout(new GridBagLayout());
      // ----------------------------------
   }

   /**
    * choose files or directory (lsy)
    */
   private static void showFileOpenDialog(Component parent, JTextField msgArea, boolean isInputDirectory)
   {
      JFileChooser fileChooser = new JFileChooser();
      fileChooser.setCurrentDirectory(new File("."));
      // can choose directories or files
      if (isInputDirectory)
      {// can choose a directory
         fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
      }
      else
      {// can choose a file
         fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
      }
      // can choose multiple
      fileChooser.setMultiSelectionEnabled(true);
      // process will be blocked until file has been chosen
      int result = fileChooser.showOpenDialog(parent);
      if (result == JFileChooser.APPROVE_OPTION)
      {
         // Only choose one file
         File file = fileChooser.getSelectedFile();
         msgArea.setText(file.getAbsolutePath());
      }
   }

   /**
    * Main Thread to invoke the UCC.Main method
    */
   private class ValidateThread implements Runnable
   {
      @Override
      public void run()
      {
         // redirect the output to GUI
         PrintStream out = new PrintStream(new TextAreaOutputStream(textAreaCommandLine));
         System.setOut(out);
         System.setErr(out);
         // store the current thread id
         runningThreadID = Thread.currentThread().getId();
         // get input from panel setting
         String input = textField_counting_FileInput.getText();
         String inputA = textField_differencing_FileInputA.getText();
         String inputB = textField_differencing_FileInputB.getText();
         String countOutputDirectory = textField_counting_FileOutput.getText();
         String diffOutputDirectory = textField_differencing_FileOutput.getText();
         String fileExtension = textField_language_FileExtensionMapping.getText();
         String commonProperty = textField_CustomLanguageProperties.getText();
         String languageName = (String) comboBox_language_SelectLanguageForExport.getSelectedItem();

         File fileInput = new File(input);
         File fileA = new File(inputA);
         File fileB = new File(inputB);
         File fileE = new File(fileExtension);
         File fileP = new File(commonProperty);
         File countFileOutput = new File(countOutputDirectory);
         File diffFileOutput = new File(diffOutputDirectory);
         // check for counting or differencing
         if (isCounting)
         {
            // check file/directory exist or not (for counting)
            if (!fileInput.exists() && !fileInput.isDirectory())
            {
               SwingUtilities.invokeLater(new Runnable()
               {
                  public void run()
                  {
                     textAreaCommandLine.setText("WARNING: Please choose valid files!\n\n");
                  }
               });
               btnStop.setEnabled(false);
               return;
            }
            if (!countFileOutput.isDirectory())
            {
               SwingUtilities.invokeLater(new Runnable()
               {
                  public void run()
                  {
                     textAreaCommandLine.setText("WARNING: Please choose valid output directory!\n\n");
                  }
               });
               btnStop.setEnabled(false);
               return;
            }
            // verify file list valid or not
            if (rdbtn_counting_FileList.isSelected())
            {
               ArrayList<String> verifiedFileList = new ArrayList<String>();
               if (fileInput.isDirectory() || FileUtils.VerifyFileList(input, verifiedFileList) == false)
               {
                  SwingUtilities.invokeLater(new Runnable()
                  {
                     public void run()
                     {
                        textAreaCommandLine.setText("WARNING: Please choose valid FileList file!\n\n");
                     }
                  });
                  btnStop.setEnabled(false);
                  return;
               }
            }
            else
            {
               if (!fileInput.isDirectory())
               {
                  SwingUtilities.invokeLater(new Runnable()
                  {
                     public void run()
                     {
                        textAreaCommandLine.setText("WARNING: Please choose valid input directory!\n\n");
                     }
                  });
                  btnStop.setEnabled(false);
                  return;
               }
            }
         }
         else
         {
            if (!fileA.exists() && !fileA.isDirectory())
            {
               SwingUtilities.invokeLater(new Runnable()
               {
                  public void run()
                  {
                     textAreaCommandLine.setText("WARNING: Please choose baseLine A files!\n\n");
                  }
               });
               btnStop.setEnabled(false);
               return;
            }
            if (!fileB.exists() && !fileB.isDirectory())
            {
               SwingUtilities.invokeLater(new Runnable()
               {
                  public void run()
                  {
                     textAreaCommandLine.setText("WARNING: Please choose baseLine B files!\n\n");
                  }
               });
               btnStop.setEnabled(false);
               return;
            }
            if (!diffFileOutput.isDirectory())
            {
               SwingUtilities.invokeLater(new Runnable()
               {
                  public void run()
                  {
                     textAreaCommandLine.setText("WARNING: Please choose valid output directory!\n\n");
                  }
               });
               btnStop.setEnabled(false);
               return;
            }
            if (rdbtn_differencing_FileList.isSelected())
            {

               ArrayList<String> verifiedFileList = new ArrayList<String>();
               if (fileA.isDirectory() || FileUtils.VerifyFileList(inputA, verifiedFileList) == false)
               {
                  SwingUtilities.invokeLater(new Runnable()
                  {
                     public void run()
                     {
                        textAreaCommandLine.setText("WARNING: Please choose valid FileList file for BaseLine A!\n\n");
                     }
                  });
                  btnStop.setEnabled(false);
                  return;
               }
               ArrayList<String> verifiedFileList2 = new ArrayList<String>();
               if (fileB.isDirectory() || FileUtils.VerifyFileList(inputB, verifiedFileList) == false)
               {
                  SwingUtilities.invokeLater(new Runnable()
                  {
                     public void run()
                     {
                        textAreaCommandLine.setText("WARNING: Please choose valid FileList file for BaseLine B!\n\n");
                     }
                  });
                  btnStop.setEnabled(false);
                  return;
               }
            }
            else
            {
               if (!fileA.isDirectory() && !fileB.isDirectory())
               {
                  SwingUtilities.invokeLater(new Runnable()
                  {
                     public void run()
                     {
                        textAreaCommandLine.setText("WARNING: Please choose valid input directory!\n\n");
                     }
                  });
                  btnStop.setEnabled(false);
                  return;
               }
            }
         }
         // build the command line list
         List<String> cmdList = new ArrayList<>();
         if (isCounting)
         {
            if (!rdbtn_counting_FileList.isSelected())
            {
               cmdList.add("-dir");
            }
            else
            {
               cmdList.add("-i1");
            }
            cmdList.add(input);
            cmdList.add("-outdir");
            cmdList.add(countOutputDirectory);
            // counting checkbox
            if (!chckbx_counting_Complexity.isSelected())
            {
               cmdList.add("-nocomplex");
            }
            if (!chckbx_counting_DuplicateFileChecking.isSelected())
            {
               cmdList.add("-nodup");
            }
            if (!chckbx_counting_FollowSymbolicLinks.isSelected())
            {
               cmdList.add("-nolinks");
            }
            if (chckbx_counting_TranslateClearcaseFilenames.isSelected())
            {
               cmdList.add("-cf");
            }
            // if(chckbx_counting_FunctionLevel.isSelected()) {
            // cmdList.add("-func");
            // }
         }
         else
         {
            cmdList.add("-d");
            if (!rdbtn_differencing_FileList.isSelected())
            {
               cmdList.add("-dir");
               cmdList.add(inputA);
               cmdList.add(inputB);
            }
            else
            {
               cmdList.add("-i1");
               cmdList.add(inputA);
               cmdList.add("-i2");
               cmdList.add(inputB);
            }
            cmdList.add("-outdir");
            cmdList.add(diffOutputDirectory);
            // differencing checkbox
            if (!chckbx_differencing_Complexity.isSelected())
            {
               cmdList.add("-nocomplex");
            }
            if (!chckbx_differencing_DuplicateFileChecking.isSelected())
            {
               cmdList.add("-nodup");
            }
            if (!chckbx_differencing_FollowSymbolicLinks.isSelected())
            {
               cmdList.add("-nolinks");
            }
            if (chckbx_differencing_TranslateClearcaseFilenames.isSelected())
            {
               cmdList.add("-cf");
            }
            // if(chckbx_differencing_FunctionLevel.isSelected()) {
            // cmdList.add("-func");
            // }
         }

         // language setting
         if (fileE.exists() && fileE.isFile())
         {
            cmdList.add("-extfile");
            cmdList.add(fileExtension);
         }

         if (fileP.exists() && fileP.isFile())
         {
            cmdList.add("-import");
            cmdList.add(commonProperty);
         }

         if (!languageName.equals("--"))
         {
            cmdList.add("-export");
            cmdList.add(languageName);
         }
         // processing setting
         if (chckbx_processing_UnifiedOutput.isSelected())
         {
            cmdList.add("-unified");
         }
         if (chckbx_processing_AsciiOutputFormat.isSelected())
         {
            cmdList.add("-ascii");
         }
         try
         {
            spinner_processing_Modification.commitEdit();
         }
         catch (java.text.ParseException e)
         {
         }
         String modification_thre = spinner_processing_Modification.getValue().toString();
         double modifi_thre = Double.valueOf(modification_thre);
         if (modifi_thre >= 0)
         {
            cmdList.add("-t");
            cmdList.add(modification_thre);
         }
         try
         {
            spinner_processing_Duplicate.commitEdit();
         }
         catch (java.text.ParseException e)
         {
         }
         String duplication_thre = spinner_processing_Duplicate.getValue().toString();
         double duplica_thre = Double.valueOf(duplication_thre);
         if (duplica_thre >= 0)
         {
            cmdList.add("-tdup");
            cmdList.add(duplication_thre);
         }
         try
         {
            spinner_processing_LSLOC.commitEdit();
         }
         catch (java.text.ParseException e)
         {
         }
         String truncation_thre = spinner_processing_LSLOC.getValue().toString();
         int truca_thre = Integer.parseInt(truncation_thre);
         if (truca_thre >= 0)
         {
            cmdList.add("-trunc");
            cmdList.add(truncation_thre);
         }
         String log_level = comboBox_processing_Log.getSelectedItem().toString();
         cmdList.add("-debug");
         cmdList.add(log_level);

         // invoke the command line list
         MainUCC.main(cmdList.toArray(new String[0]));
         // disable the stop button
         btnStop.setEnabled(false);
      }

   }

   // export language thread
   private class exportThread implements Runnable
   {
      @Override
      public void run()
      {
         // redirect output to GUI
         PrintStream out = new PrintStream(new TextAreaOutputStream(textAreaCommandLine));
         System.setOut(out);
         System.setErr(out);
         List<String> cmdList = new ArrayList<>();
         String countOutputDirectory = textField_counting_FileOutput.getText();
         String diffOutputDirectory = textField_differencing_FileOutput.getText();
         File countFileOutput = new File(countOutputDirectory);
         File diffFileOutput = new File(diffOutputDirectory);
         String languageName = (String) comboBox_language_SelectLanguageForExport.getSelectedItem();
         if (languageName.equals("--"))
         {
            SwingUtilities.invokeLater(new Runnable()
            {
               public void run()
               {
                  textAreaCommandLine.setText("WARNING: Please choose valid language name!\n\n");
               }
            });

            return;
         }
         else
         {
            cmdList.add("-export");
            cmdList.add(languageName);
         }
         if (!countFileOutput.isDirectory() && !diffFileOutput.isDirectory())
         {
            SwingUtilities.invokeLater(new Runnable()
            {
               public void run()
               {
                  textAreaCommandLine.setText("WARNING: Please choose valid output directory!\n\n");
               }
            });

            return;
         }
         else
         {
            cmdList.add("-outdir");
            if (countFileOutput.isDirectory())
            {
               cmdList.add(countOutputDirectory);
            }
            else
            {
               cmdList.add(diffOutputDirectory);
            }
         }
         // invoke command line list
         MainUCC.main(cmdList.toArray(new String[0]));
      }
   }

   // button listeners for start, stop and export button
   private class buttonListener implements ActionListener
   {
      @Override
      public void actionPerformed(ActionEvent e)
      {
         // build new thread to run UCC.Main
         if (e.getSource() == btnStart)
         {
            btnStop.setEnabled(true);
            SwingUtilities.invokeLater(new Runnable()
            {
               public void run()
               {
                  textAreaCommandLine.setText("");
               }
            });

            ValidateThread runnable = new ValidateThread();
            Thread thread = new Thread(runnable);
            thread.start();

         }
         // stop the current thread
         if (e.getSource() == btnStop)
         {
            Set<Thread> setOfThread = Thread.getAllStackTraces().keySet();
            // get the running thread for UCC.Main
            for (Thread thread : setOfThread)
            {
               if (thread.getId() == runningThreadID)
               {
                  thread.stop();
               }
            }
            btnStop.setEnabled(false);
            SwingUtilities.invokeLater(new Runnable()
            {
               public void run()
               {
                  textAreaCommandLine.append("\nThe process has been stoped!\n");
               }
            });

         }
         // build thread for export button
         if (e.getSource() == button_language_Export)
         {
            SwingUtilities.invokeLater(new Runnable()
            {
               public void run()
               {
                  textAreaCommandLine.setText("");
               }
            });
            exportThread export = new exportThread();
            Thread thread2 = new Thread(export);
            thread2.start();
         }
      }
   }
}
