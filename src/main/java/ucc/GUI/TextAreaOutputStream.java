package ucc.GUI;

import java.io.IOException;
import java.io.OutputStream;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class TextAreaOutputStream extends OutputStream
{
   private JTextArea textControl;

   public TextAreaOutputStream(JTextArea control)
   {
      textControl = control;
   }

   @Override
   public void flush()
   {
   }

   @Override
   public void close()
   {
   }

   @Override
   public void write(byte[] buffer, int offset, int length) throws IOException
   {
      final String text = new String(buffer, offset, length);
      SwingUtilities.invokeLater(new Runnable()
      {
         @Override
         public void run()
         {
            if (text.length() > 2 && text.charAt(text.length() - 2) == '%')
            {
               textControl.append(text.substring(0, text.length() - 1) + "\n");
            }
            else
            {
               textControl.append(text);
            }
            // textControl.append (text);
            textControl.setCaretPosition(textControl.getText().length()); // 让滚动条滚动
            textControl.paintImmediately(textControl.getBounds());
         }
      });
   }

   @Override
   public void write(int b) throws IOException
   {
      write(new byte[] { (byte) b }, 0, 1);
   }
   /*
    * @Override public void write(int b) throws IOException { // append the data
    * as characters to the JTextArea control SwingUtilities.invokeLater(new
    * Runnable() { public void run() {
    * textControl.append(String.valueOf((char)b)); } });
    * 
    * textControl.setCaretPosition(textControl.getText().length()); //让滚动条滚动
    * textControl.paintImmediately(textControl.getBounds()); }
    * 
    * @Override public void write(byte b[]) throws IOException {
    * SwingUtilities.invokeLater(new Runnable() { public void run() {
    * textControl.append(new String(b)); } });
    * textControl.setCaretPosition(textControl.getText().length()); //让滚动条滚动
    * textControl.paintImmediately(textControl.getBounds()); }
    * 
    * @Override public void write(byte b[], int off, int len) throws IOException
    * { SwingUtilities.invokeLater(new Runnable() { public void run() {
    * textControl.append(new String(b, off, len)); } });
    * 
    * textControl.setCaretPosition(textControl.getText().length()); //让滚动条滚动
    * textControl.paintImmediately(textControl.getBounds()); }
    */
}
