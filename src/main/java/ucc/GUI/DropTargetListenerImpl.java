package ucc.GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.*;
import java.io.File;
import java.util.List;

public class DropTargetListenerImpl implements DropTargetListener
{
   private JTextField textField;

   public DropTargetListenerImpl(JTextField textField)
   {
      this.textField = textField;
   }

   @Override
   public void dragEnter(DropTargetDragEvent dtde)
   {

   }

   @Override
   public void dragOver(DropTargetDragEvent dtde)
   {

   }

   @Override
   public void dragExit(DropTargetEvent dte)
   {

   }

   @Override
   public void dropActionChanged(DropTargetDragEvent dtde)
   {

   }

   public void drop(DropTargetDropEvent dtde)
   {
      boolean isAccept = false;
      try
      {
         if (dtde.isDataFlavorSupported(DataFlavor.javaFileListFlavor))
         {
            dtde.acceptDrop(DnDConstants.ACTION_COPY_OR_MOVE);
            isAccept = true;
            List<File> files = (List<File>) dtde.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
            if (files != null && files.size() > 0)
            {
               String filePaths = "";
               for (File file : files)
               {
                  filePaths = file.getAbsolutePath();
               }
               textField.setText(filePaths);
            }
         }
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
      if (isAccept)
      {
         dtde.dropComplete(true);
      }
   }

}
